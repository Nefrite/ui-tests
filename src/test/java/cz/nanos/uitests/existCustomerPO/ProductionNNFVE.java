package cz.nanos.uitests.existCustomerPO;

import cz.nanos.uitests.pages.HomePage;
import cz.nanos.uitests.pages.NewContract.*;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;




@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductionNNFVE extends AuthorizedScenario {
    public String scenarioDescription = "Nová smlouva pro nového zákazníka. Výroba NN.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    public static String contract_type = "";
    public static String voltage = "";

    @Test
    public void ProductionNN(){
        actions.clickOnElement(HomePage.new_3_consumer);

        WebActions.actual("NEW CONTRACT", "page");
        WebActions.actual("KROK 1 - ZÁKAZNÍK", "step");
        //String retezec = actions.uniqueX(8);

        actions.radioSetId(NewContractStepOne.customerType, "typeFOP");
        WebActions.actual("Sekce Firemní výdaje", "section");
        actions.writeText(NewContractStepOne.personRegCode, "25575767");

        // založení nového zákazníka, ošetření na tlačítka
        System.err.println();
        actions.clickOnElement(NewContractStepOne.buttonSearchCustomerFOPPO);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));

        System.out.println("Uživatel nalezen.");

        actions.waitDropdownLoadValue(NewContractStepOne.primary_contact_person_id);
        WebElement table1 = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/form[1]/div[2]/div[3]/div[1]/div[1]/div[2]/table[1]/tbody[1]"));
        if (table1.findElements(By.tagName("tr")).size() == 0) {
            WebActions.actual("Sekce Obecné kontakty", "section");
            actions.clickOnElement(NewContractStepOne.addBasicContact);
            actions.ModalGeneralFillForm(driver);
            // kontrola zadaných hodnot
            actions.tableGetRowValuesGeneral(driver.findElement(By.xpath("//div[contains(@class,'block block-newContract--general-contacts')]//tbody")), "Nákupčí energie");
        } else {
            System.err.println("Zákazník má záznamy v tabulce obecné kontakty");
        }

        /* Nastaveni bez změny
        WebActions.actual("Sekce Nastavení", "section");
        actions.selectSetByValue(NewContractStepOne.sales_representative_id, "11");
        actions.clickOnElement(NewContractStepOne.primary_contact_person_id);
        actions.selectFirst(NewContractStepOne.primary_contact_person_id);
        actions.writeText(NewContractStepOne.person_memo, "Poznámka u nastavení");
        actions.checkboxChecked(NewContractStepOne.person_vip);
        actions.checkboxChecked(NewContractStepOne.autosend_demands);
        actions.checkboxChecked(NewContractStepOne.autosend_advance_invoice);
        actions.checkboxChecked(NewContractStepOne.autosend_advance_reminders);
        actions.checkboxChecked(NewContractStepOne.autosend_payment_receipts);
        */

        WebElement table2 = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/form[1]/div[2]/div[4]/div[1]/div[1]/div[3]/table[1]/tbody[1]"));
        if (table2.findElements(By.tagName("tr")).size() == 0) {
            WebActions.actual("Sekce Použití kontaktů", "section");
            actions.clickOnElement(NewContractStepOne.addContact);
            actions.selectFirst(NewContractStepOne.contact_select);
        actions.checkboxChecked(NewContractStepOne.contact_general);
        actions.checkboxChecked(NewContractStepOne.contact_advances);
        actions.checkboxChecked(NewContractStepOne.contact_bill);
        actions.checkboxChecked(NewContractStepOne.contact_invoice);
        actions.checkboxChecked(NewContractStepOne.contact_marketing);
        actions.checkboxChecked(NewContractStepOne.contact_demand);
        actions.checkboxChecked(NewContractStepOne.contact_production);
            actions.clickOnElement(NewContractStepOne.contact_save);
            actions.waitHideLoader();
            actions.tableGetRowValuesGeneral(driver.findElement(By.xpath("//div[contains(@class,'block block-newContract--general-contacts')]//tbody")), "Nákupčí energie");
        } else {
            System.err.println("Zákazník má záznamy v tabulce použití kontaktů");
        }

        WebElement table3 = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/form[1]/div[2]/div[5]/div[1]/div[1]/div[1]/div[3]/table[1]/tbody[1]"));
        if (table3.findElements(By.tagName("tr")).size() == 0) {
            // přidání platebních kontaktů
            String payment_type = "bank";
            String unique = actions.uniqueX(10);
            String nazev = "Platba BÚ";
            String kod_banky = "2010";
            actions.clickOnElement(NewContractStepOne.addPaymentContact);
            actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);

            payment_type = "sipo";
            unique = actions.uniqueX(10);
            nazev = "Platba SIPO";
            kod_banky = "2010";
            actions.clickOnElement(NewContractStepOne.addPaymentContact);
            actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);
            actions.clickOnElement(By.name("buttonNextStep"));
        } else {
            System.err.println("Zákazník má záznamy v tabulce platební kontakty");
        }
        actions.clickOnElement(By.name("buttonNextStep"));

        // 2. krok
        WebActions.actual("KROK 2 - TYP SMLOUVY", "step");
        actions.selectSetByText(NewContractStepTwo.contract_type, "Výroba");
        actions.clickOnElement(By.name("buttonNextStep"));

        // 3.krok
        WebActions.actual("KROK 3 - NASTAVENI SMLOUVY", "step");
        NewContractStepThree.controlAllElement();
        actions.selectSetByText(NewContractStepThree.production_tariff_id, "Výkup");
        actions.selectFirst(NewContractStepThree.contact_person_id);
        actions.writeText(NewContractStepThree.client_code, "Značka");
        actions.writeText(NewContractStepThree.memo, "Poznámka krok 3");
        actions.clickOnElement(NewContractStepThree.buttonNextStep);

        // 4.krok

        // získání EAN z API
        String EAN = WebActions.getEAN("");
        actions.writeText(NewContractStepFour.addSearchEan, EAN);
        //actions.clickOnElement(NewContractStepFour.buttonSearch);
        actions.clickAndControlToast(NewContractStepFour.buttonSearch, "EAN nenalezen", "Zadané EAN je již použité, " +
                "proto nelze pokračovat dále v zakládání OPM.");

        actions.clickOnElement(NewContractStepFour.buttonCancelChange);

        // kontrola statických elementů
        NewContractStepFour.getStaticElements();
        /**
         *
         */
        //NewContractStepFour.checkAllTypeFactory();

        actions.actual("Obecné", "section");
        actions.writeDistNetwork(NewContractStepFour.dist_network, "0011");
        actions.clickOnElement(By.xpath("//button[@title='Editovat adresu']"));

        String town = "Břeclav";
        String psc = "69002";
        String mistni_cast = "";
        String street = "Fintajsova";
        String number_cp = "1347";
        String number_op = "";
        String address = "";
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
        actions.radioSet(NewContractStepFour.measurement_type, "A");
        actions.writeText(NewContractStepFour.memo, "Poznámka krok 4");
        actions.writeText(NewContractStepFour.custom_name, "Značka odběr");

        actions.actual("Odběr", "section");
        actions.writeText(NewContractStepFour.custom_name, "Značka");
        actions.writeText(NewContractStepFour.reserved_power, "20");
        actions.writeText(NewContractStepFour.reserved_capacity_year, "20");
        actions.writeText(NewContractStepFour.control_stage_3, "10");
        actions.writeText(NewContractStepFour.control_stage_4, "10");
        actions.writeText(NewContractStepFour.control_stage_5, "10");
        actions.writeText(NewContractStepFour.control_stage_6, "10");
        actions.writeText(NewContractStepFour.control_stage_7_kwh, "15");
        actions.writeText(NewContractStepFour.control_stage_7_hr, "15");

        /*
        actions.clickOnElement(NewContractStepFour.btnIcon_AddMonth);
        actions.writeText(NewContractStepFour.month_1, "10");
        actions.writeText(NewContractStepFour.month_2, "10");
        actions.writeText(NewContractStepFour.month_3, "10");
        actions.writeText(NewContractStepFour.month_4, "10");
        actions.writeText(NewContractStepFour.month_5, "10");
        actions.writeText(NewContractStepFour.month_6, "10");
        actions.writeText(NewContractStepFour.month_7, "10");
        actions.writeText(NewContractStepFour.month_8, "10");
        actions.writeText(NewContractStepFour.month_9, "10");
        actions.writeText(NewContractStepFour.month_10, "10");
        actions.writeText(NewContractStepFour.month_11, "10");
        actions.writeText(NewContractStepFour.month_12, "10");

        actions.clickOnElement(NewContractStepFour.button_save_capacity);
        */
        actions.clickOnElement(By.name("buttonSave"));
        actions.clickOnElement(By.name("buttonNextStep"));

        actions.actual("KROK 5 - Nastavení OPM", "step");

        // Výchozí nastavení fakturace
        actions.actual("Výchozí nastavení - fakturace", "section");
        actions.selectSetByText(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/select[1]"), "Měsíční");
        actions.clickOnElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[2]/button[1]"));
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.clickAndControlToast(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[1]/button[1]"), "Data uložena", "Uložení 'Výchozího nastavení Fakturace' neproběhlo v pořádku");

        actions.clickOnElement(NewContractStepFive.button_5_1);

        actions.setCalendar(NewContractStepFive.nd_delivery_start_at, actions.dateUser(5));
        actions.setCalendar(NewContractStepFive.nd_delivery_end_at, actions.dateUser(20));

        actions.checkboxChecked(NewContractStepFive.nv_use_default_billing);
        actions.clickAndControlToast(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/button[1]"), "Data o OPM uložena", "Nedošlo k uložení dat OPM");

        actions.clickOnElement(By.name("buttonNextStep"));
        actions.clickOnElement(By.name("saveButton"));

        // 6.krok
        actions.actual("KROK 6 - Shrnutí", "step");
        System.out.println("Výroba FVE\n");

        WebElement summary = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]"));
        System.out.println("Shrnutí:");
        System.out.println("Zákazník: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/a[1]")).getText());
        System.out.println("Číslo smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[2]")).getText());
        System.out.println("Typ smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[3]")).getText());
        System.out.println("OPM: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[4]")).getText());

        WebElement document = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[3]/table[1]/tbody[1]"));
        System.out.println("\nDokumenty:");
        List<WebElement> rows = document.findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            System.out.println("Název dokumentu : '" + cells.get(1).getText() + "' Popis: '" + cells.get(2).getText()+"'");
        }

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}
