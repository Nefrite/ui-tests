package cz.nanos.uitests.login;

import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.nanos.uitests.service.AuthorizedScenario;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Login extends AuthorizedScenario {
	public String scenarioDescription = "Přihlášení";
	@Override
	public String getScenarioDescription() {
		return scenarioDescription;
	}
	@Before
	public void init() throws Exception{
	}

	@After
	public void close(){
		WebActions.actual("KONEC SCENARE", "page");
		userLogout();
	}

	/**
	 * Kroky daného scénáře - vykonávají se postupně
	 */
	@Test
	public void loginPageTestCase(){
		userLogin(loginUser, loginPassword);
		System.out.println("Scénář byl úspěšně dokončen.");
	}
}
