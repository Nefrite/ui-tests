package cz.nanos.uitests.login;

import cz.nanos.uitests.service.WebActions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.nanos.uitests.service.AuthorizedScenario;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Logout extends AuthorizedScenario {

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    public String scenarioDescription = "Scénář odhlášení";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }
    
    @Test
    public void logoutTestScenario(){
        userLogout();
        WebActions.actual("KONEC SCENARE", "page");
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}
