package cz.nanos.uitests.newCustomerFO;

import cz.nanos.uitests.pages.*;
import cz.nanos.uitests.pages.NewContract.*;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cz.nanos.uitests.service.AuthorizedScenario;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;




@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsumptionNN extends AuthorizedScenario {
    public String scenarioDescription = "Nová smlouva pro nového zákazníka. Odběr NN.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    public static String contract_type = "";
    public static String voltage = "";

    @Test
    public void ConsumptionNN() throws IOException {
        actions.clickOnElement(HomePage.new_1_consumer);

        WebActions.actual("NEW CONTRACT", "page");
        WebActions.actual("KROK 1 - ZÁKAZNÍK", "step");

        String retezec = actions.uniqueX(8);
        actions.radioSetId(NewContractStepOne.customerType, "typeFO");
        WebActions.actual("Sekce Osobní údaje", "section");
        String jmeno = actions.getNameRandom();
        String prijmeni = actions.getSurnameRandom();
        String datum_narozeni = actions.getBirthdayRandom();
        actions.writeText(NewContractStepOne.personFirstname, jmeno);
        actions.writeText(NewContractStepOne.personLastname, prijmeni);
        actions.setCalendar(NewContractStepOne.calendarBirthday, datum_narozeni);
        actions.clickOnElement(NewContractStepOne.addAddress);

        String town = "Břeclav";
        String psc = "69002";
        String mistni_cast = "";
        String street = "Fintajsova";
        String number_cp = "1347";
        String number_op = "";
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
        //actions.ModalAddressFillForm(driver, "POBOX", retezec);

        // přidání korespondenční adresy - kliknutí na tl, vyber select, a pak otevření Modalu a doplěnní hodnoty
        actions.clickOnElement(NewContractStepOne.addAddressNext);
        WebActions.timeDelay(1000);
        actions.elementsExistance(Arrays.asList(NewContractStepOne.addNewAddress, NewContractStepOne.btn_add_concrete_addr, NewContractStepOne.btn_save_concrete_addr, NewContractStepOne.btnStorno));
        actions.selectSetByValue(NewContractStepOne.addNewAddress, "MAILING");
        actions.clickOnElement(NewContractStepOne.btn_add_concrete_addr);
        actions.ModalAddressFillForm(driver, "POBOX", retezec);
        actions.clickOnElement(NewContractStepOne.btn_save_concrete_addr);

        // založení nového zákazníka, ošetření na tlačítka
        actions.waitVisibility(NewContractStepOne.buttonSearchCustomerFOSP);
        actions.clickAndControlToast(NewContractStepOne.buttonSearchCustomerFOSP, "Zákazník nenalezen", "Zákazník již existuje");
        actions.waitVisibility(NewContractStepOne.buttonCreateNewCustomerFOSP);
        actions.clickAndControlToast(NewContractStepOne.buttonCreateNewCustomerFOSP, "Vytvořen nový zákazník", "Nový zákazník nebyl založen došlo k chybě.");
        System.out.println("Uživatel vytvořen");

        WebActions.actual("Sekce Obecné kontakty", "section");
        actions.clickOnElement(NewContractStepOne.addBasicContact);
        actions.ModalGeneralFillForm(driver);
        // kontrola zadaných hodnot
        actions.tableGetRowValuesGeneral(driver.findElement(By.xpath("//div[contains(@class,'block block-newContract--general-contacts')]//tbody")), "Nákupčí energie");

        WebActions.actual("Sekce Nastavení", "section");
        actions.selectSetByValue(NewContractStepOne.sales_representative_id, "11");
        actions.clickOnElement(NewContractStepOne.primary_contact_person_id);
        actions.selectFirst(NewContractStepOne.primary_contact_person_id);
        actions.writeText(NewContractStepOne.person_memo, "Poznámka u nastavení");
        actions.checkboxChecked(NewContractStepOne.person_vip);
        actions.checkboxChecked(NewContractStepOne.autosend_demands);
        actions.checkboxChecked(NewContractStepOne.autosend_advance_invoice);
        actions.checkboxChecked(NewContractStepOne.autosend_advance_reminders);
        actions.checkboxChecked(NewContractStepOne.autosend_payment_receipts);

        WebActions.actual("Sekce Použití kontaktů", "section");
        actions.clickOnElement(NewContractStepOne.addContact);
        actions.selectFirst(NewContractStepOne.contact_select);
        actions.checkboxChecked(NewContractStepOne.contact_general);
        actions.checkboxChecked(NewContractStepOne.contact_advances);
        actions.checkboxChecked(NewContractStepOne.contact_bill);
        actions.checkboxChecked(NewContractStepOne.contact_invoice);
        actions.checkboxChecked(NewContractStepOne.contact_marketing);
        actions.checkboxChecked(NewContractStepOne.contact_demand);
        actions.checkboxChecked(NewContractStepOne.contact_production);
        actions.clickOnElement(NewContractStepOne.contact_save);
        actions.waitHideLoader();

        // přidání platebních kontaktů
        String payment_type = "bank";
        String unique = actions.uniqueX(10);
        String nazev = "Platba BÚ";
        String kod_banky = "2010";
        actions.clickOnElement(NewContractStepOne.addPaymentContact);
        actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);

        payment_type = "sipo";
        unique = actions.uniqueX(10);
        nazev = "Platba SIPO";
        kod_banky = "2010";
        actions.clickOnElement(NewContractStepOne.addPaymentContact);
        actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);
        actions.clickOnElement(By.name("buttonNextStep"));

        // 2. krok
        WebActions.actual("KROK 2 - TYP SMLOUVY", "step");
        actions.selectSetByText(NewContractStepTwo.contract_type, "Odběr");
        actions.selectSetByText(NewContractStepTwo.voltage, "NN");
        NewContractStepTwo.controlAllElement(driver.findElement(NewContractStepTwo.contract_type).getAttribute("value"));
        actions.clickOnElement(By.name("buttonNextStep"));

        // 3.krok
        WebActions.actual("KROK 3 - NASTAVENI SMLOUVY", "step");
        NewContractStepThree.controlAllElement();
        //NewContractStepThree.fillForm();

        // zalozka nastaveni
        WebActions.actual("Zalozka Nastaveni", "section");
        actions.clickOnElement(NewContractStepThree.nastaveni);
        actions.waitDropdownLoadValue(NewContractStepThree.consumption_tariff_id);
        actions.clickOnElement(NewContractStepThree.consumption_tariff_id);
        actions.selectSetByValue(NewContractStepThree.consumption_tariff_id, NewContractStepThree.selectCheckValueNewContact(NewContractStepThree.consumption_tariff_id));
        actions.selectFirst(NewContractStepThree.contact_person_id);
        actions.writeText(NewContractStepThree.client_code, "značka zákazníka");
        actions.checkboxChecked(NewContractStepThree.summary_advance);
        actions.checkboxChecked(NewContractStepThree.summary_bill);
        actions.checkboxUnChecked(NewContractStepThree.consumption_only);
        actions.checkboxUnChecked(NewContractStepThree.use_default_contact_setting);
        actions.writeText(NewContractStepThree.memo, "Poznámka v nastavení smlouvy");

        // zalozka cena silove elektriny
        WebActions.actual("Zalozka Cena sil. elektřiny", "section");
        actions.clickOnElement(NewContractStepThree.cena_silove_elektriny);
        actions.clickOnElement(NewContractStepThree.btnIcon_AddFee);
        actions.setCalendar(NewContractStepThree.calendarSincenew, actions.dateUser(0));
        actions.setCalendar(NewContractStepThree.calendarUntilnew, actions.dateUser(100));
        actions.clearText(NewContractStepThree.trade_fee_new);
        actions.writeText(NewContractStepThree.trade_fee_new, "50");
        actions.selectSetByValue(NewContractStepThree.confirmed_fee_new, "true");
        actions.clickOnElement(NewContractStepThree.btnSave_new);

        /*
        actions.clickOnElement(NewContractStepThree.btnIcon_AddPrice);
        WebActions.timeDelay(500);
        actions.selectSetByValue(NewContractStepThree.distribution_tariff_new, "D01d");
        actions.setCalendar(NewContractStepThree.calendarSince_price_new, actions.dateUser(0));
        actions.setCalendar(NewContractStepThree.calendarUntil_price_new, actions.dateUser(100));
        actions.writeText(NewContractStepThree.high_tariff_price_new, "20");
        actions.writeText(NewContractStepThree.low_tariff_price_new, "10");
        actions.selectSetByValue(NewContractStepThree.confirmed_price_new, "false");
        */
        actions.clickOnElement(By.name("buttonNextStep"));

        // 4.krok
        actions.actual("KROK 4 - OPM", "step");

        // získání EAN z API
        String EAN = WebActions.getEAN("");
        actions.writeText(NewContractStepFour.addSearchEan, EAN);
        //actions.clickOnElement(NewContractStepFour.buttonSearch);
        actions.clickAndControlToast(NewContractStepFour.buttonSearch, "EAN nenalezen", "Zadané EAN je již použité, " +
                "proto nelze pokračovat dále v zakládání OPM.");

        actions.clickOnElement(NewContractStepFour.buttonCancelChange);

        // kontrola statických elementů
        NewContractStepFour.getStaticElements();
        /**
         *
         */
        //NewContractStepFour.checkAllTypeFactory();

        actions.actual("Obecné", "section");
        actions.writeDistNetwork(NewContractStepFour.dist_network, "0041");
        actions.clickOnElement(By.xpath("//button[@title='Editovat adresu']"));

        town = "Olomouc";
        psc = "77900";
        mistni_cast = "Nová Ulice";
        street = "Litovelská";
        number_cp = "100";
        number_op = "19";
        String address = "";
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);

        actions.radioSet(NewContractStepFour.measurement_type, "B");
        actions.writeText(NewContractStepFour.memo, "Poznámka krok 4");
        actions.writeText(NewContractStepFour.custom_name, "Značka odběr");
        actions.selectSetByValue(NewContractStepFour.phases, "1");
        actions.writeText(NewContractStepFour.capacity, "20");
        actions.selectSetByValue(NewContractStepFour.distribution_tariff_code, "D01D");
        actions.writeText(NewContractStepFour.electrometer_code, "123456");
        actions.clickOnElement(NewContractStepFour.buttonSave);
        actions.tableGetRowValuesByName(driver.findElement(By.xpath("//div[contains(@class,'step step4 activeStep')]//tbody")), EAN);
        actions.clickOnElement(By.name("buttonNextStep"));

        actions.actual("KROK 5 - Nastavení OPM", "step");

        // Výchozí nastavení zálohy
        actions.selectSetByText(NewContractStepFive.oz_advance_frequency, "Měsíční");
        actions.selectSetByText(NewContractStepFive.oz_advance_payment_method, "Příkaz");
        actions.clickOnElement(NewContractStepFive.oz_button_add_contact);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));

        // Výchozí nastavení Vyúř
        actions.selectSetByText(NewContractStepFive.ov_bill_frequency, "Pololetní");
        actions.writeText(NewContractStepFive.ov_bill_payable_period, "20");
        actions.selectSetByText(NewContractStepFive.ov_overpayment_repay_strgy_code, "Úhrada/vratka");
        actions.checkboxUnChecked(NewContractStepFive.ov_exclude_automatic_bill);
        actions.selectSetByText(NewContractStepFive.ov_overpayment_payment_method, "Úhrada/vratka příkazem");
        actions.clickOnElement(NewContractStepFive.ov_button_add_preplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.selectSetByText(NewContractStepFive.ov_debt_payment_method, "Příkaz");
        actions.clickOnElement(NewContractStepFive.ov_button_add_contact_preplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.clickAndControlToast(NewContractStepFive.o_buttonSave, "Data uložena", "Uložení 'Výchozích dat' OPM neproběhlo v pořádku");

        // pokračování stránky - pro konkrétní OPM
        actions.clickOnElement(NewContractStepFive.button_5_1);

        // OPM dodávka
        actions.setCalendar(NewContractStepFive.nd_delivery_start_at, actions.dateUser(0));
        actions.setCalendar(NewContractStepFive.nd_delivery_end_at, actions.dateUser(1));
        actions.selectSetByText(NewContractStepFive.nd_contract_condition_code, "Změna dodavatele");

        // OPM zálohy
        actions.writeText(NewContractStepFive.nz_advance_amount, "500");
        actions.checkboxChecked(NewContractStepFive.nz_use_default_advance);

        // OPM vyúčtování

        // OPM výpověď
        actions.checkboxUnChecked(NewContractStepFive.v_terminate_previous);
        actions.checkboxChecked(NewContractStepFive.nv_use_default_billing);

        /*
        WebActions.actual("Sekce Výpověď", "section");
        actions.checkboxChecked(NewContractStepFive.v_terminate_previous);
        actions.selectSetByValue(NewContractStepFive.v_previous_trader_id, "9");
        actions.selectSetByValue(NewContractStepFive.v_termination_period_unit, "DAY");
        actions.timeDelay(500);
        actions.writeText(NewContractStepFive.v_termination_period, "50");
        actions.clickOnElement(NewContractStepFive.n_buttonSave);
         */
        actions.clickAndControlToast(NewContractStepFive.n_buttonSave, "Data o OPM uložena", "Uložení dat 'konkrétního OPM' neproběhlo v pořádku");

        actions.clickOnElement(By.name("buttonNextStep"));
        actions.clickOnElement(By.name("saveButton"));
        actions.waitHideLoader();

        // 6.krok
        actions.actual("KROK 6 - Shrnutí", "step");
        System.out.println("Spotřeba NN\n");

        WebElement summary = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]"));
        System.out.println("Shrnutí:");
        System.out.println("Zákazník: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/a[1]")).getText());
        System.out.println("Číslo smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[2]")).getText());
        System.out.println("Typ smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[3]")).getText());
        System.out.println("OPM: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[4]")).getText());

        WebElement document = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[3]/table[1]/tbody[1]"));
        System.out.println("\nDokumenty:");
        List<WebElement> rows = document.findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            System.out.println("Název dokumentu : '" + cells.get(1).getText() + "' Popis: '" + cells.get(2).getText()+"'");
        }

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}
