package cz.nanos.uitests.newCustomerFO;

import cz.nanos.uitests.pages.HomePage;
import cz.nanos.uitests.pages.NewContract.*;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;




@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductionNNBioplyn extends AuthorizedScenario {
    public String scenarioDescription = "Nová smlouva pro nového zákazníka. Výroba NN.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    public static String contract_type = "";
    public static String voltage = "";

    @Test
    public void ProductionNNBioplyn(){
        actions.clickOnElement(HomePage.new_3_consumer);

        WebActions.actual("NEW CONTRACT", "page");
        WebActions.actual("KROK 1 - ZÁKAZNÍK", "step");

        actions.radioSetId(NewContractStepOne.customerType, "typeFO");
        WebActions.actual("Sekce Osobní údaje", "section");

        String jmeno = actions.getNameRandom();
        String prijmeni = actions.getSurnameRandom();
        String datum_narozeni = actions.getBirthdayRandom();
        actions.writeText(NewContractStepOne.personFirstname, jmeno);
        actions.writeText(NewContractStepOne.personLastname, prijmeni);
        actions.setCalendar(NewContractStepOne.calendarBirthday, datum_narozeni);
        actions.clickOnElement(NewContractStepOne.addAddress);

        String town = "Břeclav";
        String psc = "69002";
        String mistni_cast = "";
        String street = "Fintajsova";
        String number_cp = "1347";
        String number_op = "";
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);

        // přidání korespondenční adresy - kliknutí na tl, vyber select, a pak otevření Modalu a doplěnní hodnoty
        actions.clickOnElement(NewContractStepOne.addAddressNext);
        actions.elementsExistance(Arrays.asList(NewContractStepOne.addNewAddress, NewContractStepOne.btn_add_concrete_addr, NewContractStepOne.btn_save_concrete_addr, NewContractStepOne.btnStorno));
        actions.selectSetByValue(NewContractStepOne.addNewAddress, "MAILING");
        actions.clickOnElement(NewContractStepOne.btn_add_concrete_addr);
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
        actions.clickOnElement(NewContractStepOne.btn_save_concrete_addr);

        // založení nového zákazníka, ošetření na tlačítka
        actions.waitVisibility(NewContractStepOne.buttonSearchCustomerFOSP);
        actions.clickAndControlToast(NewContractStepOne.buttonSearchCustomerFOSP, "Zákazník nenalezen", "Zákazník již existuje");
        actions.waitVisibility(NewContractStepOne.buttonCreateNewCustomerFOSP);
        actions.clickAndControlToast(NewContractStepOne.buttonCreateNewCustomerFOSP, "Vytvořen nový zákazník", "Nový zákazník nebyl založen došlo k chybě.");
        System.out.println("Uživatel vytvořen");

        WebActions.actual("Sekce Obecné kontakty", "section");
        actions.clickOnElement(NewContractStepOne.addBasicContact);
        actions.ModalGeneralFillForm(driver);
        // kontrola zadaných hodnot
        actions.tableGetRowValuesGeneral(driver.findElement(By.xpath("//div[contains(@class,'block block-newContract--general-contacts')]//tbody")), "Nákupčí energie");

        WebActions.actual("Sekce Nastavení", "section");
        actions.selectSetByValue(NewContractStepOne.sales_representative_id, "11");
        actions.clickOnElement(NewContractStepOne.primary_contact_person_id);
        actions.selectFirst(NewContractStepOne.primary_contact_person_id);
        actions.writeText(NewContractStepOne.person_memo, "Poznámka nastaveni 1. krok");
        actions.checkboxChecked(NewContractStepOne.person_vip);
        actions.checkboxChecked(NewContractStepOne.autosend_demands);
        actions.checkboxChecked(NewContractStepOne.autosend_advance_invoice);
        actions.checkboxChecked(NewContractStepOne.autosend_advance_reminders);
        actions.checkboxChecked(NewContractStepOne.autosend_payment_receipts);

        WebActions.actual("Sekce Použití kontaktů", "section");
        actions.clickOnElement(NewContractStepOne.addContact);
        actions.selectFirst(NewContractStepOne.contact_select);
        actions.checkboxChecked(NewContractStepOne.contact_general);
        actions.checkboxChecked(NewContractStepOne.contact_advances);
        actions.checkboxChecked(NewContractStepOne.contact_bill);
        actions.checkboxChecked(NewContractStepOne.contact_invoice);
        actions.checkboxChecked(NewContractStepOne.contact_marketing);
        actions.checkboxChecked(NewContractStepOne.contact_demand);
        actions.checkboxChecked(NewContractStepOne.contact_production);
        actions.clickOnElement(NewContractStepOne.contact_save);
        actions.waitHideLoader();

        // přidání platebních kontaktů SIPO/BÚ
        String payment_type = "bank";
        String unique = actions.uniqueX(10);
        String nazev = "Bankovní účet";
        String kod_banky = "3030";
        actions.clickOnElement(NewContractStepOne.addPaymentContact);
        actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);
        payment_type = "sipo";
        unique = actions.uniqueX(10);
        nazev = "Platba SIPO";
        kod_banky = "2010";
        actions.clickOnElement(NewContractStepOne.addPaymentContact);
        actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);

        actions.clickOnElement(By.name("buttonNextStep"));

        // 2. krok
        WebActions.actual("KROK 2 - TYP SMLOUVY", "step");
        actions.selectSetByText(NewContractStepTwo.contract_type, "Výroba");
        actions.clickOnElement(By.name("buttonNextStep"));

        // 3.krok
        WebActions.actual("KROK 3 - NASTAVENI SMLOUVY", "step");
        NewContractStepThree.controlAllElement();
        actions.selectSetByText(NewContractStepThree.production_tariff_id, "Výkup");
        actions.selectFirst(NewContractStepThree.contact_person_id);
        actions.writeText(NewContractStepThree.client_code, "Značka");
        actions.writeText(NewContractStepThree.memo, "Poznámka krok 3");
        actions.clickOnElement(NewContractStepThree.buttonNextStep);

        // 4.krok
        actions.actual("KROK 4 - OPM", "step");
        // získání EAN z API
        String EAN = WebActions.getEAN("");
        actions.writeText(NewContractStepFour.addSearchEan, EAN);
        actions.clickAndControlToast(NewContractStepFour.buttonSearch, "EAN nenalezen", "Zadané EAN je již použité, " +
                "proto nelze pokračovat dále v zakládání OPM.");
        //kontrola statických elementů
        //NewContractStepFour.getStaticElements();

        actions.actual("Typ výrobního OPM", "section");
        actions.clickOnElement(By.id("prodNN"));

        actions.actual("Obecné", "section");
        actions.writeDistNetwork(NewContractStepFour.dist_network, "0011");
        actions.clickOnElement(By.xpath("//button[@title='Editovat adresu']"));

        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);

        actions.radioSet(NewContractStepFour.measurement_type, "B");
        actions.writeText(NewContractStepFour.memo, "Poznamka krok 4 - Bioplyn");

        actions.actual("Výroba", "section");
        actions.writeText(NewContractStepFour.power_station_name, "Bioplyn - " + town + " " + street);
        actions.selectSetByText(NewContractStepFour.power_station_type_code, "Bioplynová stanice");
        actions.writeText(NewContractStepFour.power_station_installed_capacity, "2.5");
        actions.clickOnElement(NewContractStepFour.power_station_production_licence_code);
        actions.clickOnElement(NewContractStepFour.power_station_grid_connection_contract_code);
        actions.checkboxUnChecked(NewContractStepFour.power_station_autoreport_ote);

        actions.writeText(NewContractStepFour.power_station_stability_threshold, "40");
        actions.checkboxUnChecked(NewContractStepFour.power_station_autoFixed_power);
        actions.writeText(NewContractStepFour.power_station_fixed_power, "1.5");
        /*
        actions.writeText(NewContractStepFour.custom_name, "Značka odběr");
        actions.selectSetByValue(NewContractStepFour.phases, "1");
        actions.writeText(NewContractStepFour.capacity, "20");
        actions.selectSetByValue(NewContractStepFour.distribution_tariff_code, "D01D");
        actions.writeText(NewContractStepFour.electrometer_code, "123456");
        actions.clickOnElement(NewContractStepFour.buttonSave);
        actions.tableGetRowValuesByName(driver.findElement(By.xpath("//div[contains(@class,'step step4 activeStep')]//tbody")), EAN);
        */
        //actions.clickAndControlToast();

        actions.clickOnElement(By.name("buttonSave"));
        actions.clickOnElement(By.name("buttonNextStep"));
        actions.actual("KROK 5 - Nastavení OPM", "step");

        // Výchozí nastavení fakturace
        actions.actual("Výchozí nastavení - fakturace", "section");
        actions.selectSetByText(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/select[1]"), "Měsíční");
        actions.clickOnElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[2]/button[1]"));
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.clickAndControlToast(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[1]/button[1]"), "Data uložena", "Uložení 'Výchozího nastavení Fakturace' neproběhlo v pořádku");

        actions.clickOnElement(NewContractStepFive.button_5_1);

        actions.setCalendar(NewContractStepFive.nd_delivery_start_at, actions.dateUser(5));
        actions.setCalendar(NewContractStepFive.nd_delivery_end_at, actions.dateUser(20));

        actions.checkboxChecked(NewContractStepFive.nv_use_default_billing);
        actions.clickAndControlToast(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/button[1]"), "Data o OPM uložena", "Nedošlo k uložení dat OPM");

        actions.clickOnElement(By.name("buttonNextStep"));
        actions.clickOnElement(By.name("saveButton"));

        // 6.krok
        actions.actual("KROK 6 - Shrnutí", "step");
        System.out.println("Výroba Bioplynová stanice\n");

        WebElement summary = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]"));
        System.out.println("Shrnutí:");
        System.out.println("Zákazník: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/a[1]")).getText());
        System.out.println("Číslo smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[2]")).getText());
        System.out.println("Typ smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[3]")).getText());
        System.out.println("OPM: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[4]")).getText());

        /*
        WebElement document = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[3]/table[1]/tbody[1]"));
        System.out.println("\nDokumenty:");
        List<WebElement> rows = document.findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            System.out.println("Název dokumentu : '" + cells.get(1).getText() + "' Popis: '" + cells.get(2).getText()+"'");
        }
        */

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}
