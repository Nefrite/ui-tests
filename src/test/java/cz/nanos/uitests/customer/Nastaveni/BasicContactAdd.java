package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.LinkedList;




@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicContactAdd extends AuthorizedScenario {
    public String scenarioDescription = "Přidání základního kontaktu k existujícímu zákazníkovi.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void BasicContactAdd(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();

        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);
        // tabulka Obecne kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block block-newContract--general-contacts']//tbody"));
        LinkedList<String> listIdsRow_old = actions.tableGetAllIdRow(table, "label");

        // přidání nového obecného kontaktu
        String jmeno = actions.getNameRandom();
        String prijmeni = actions.getSurnameRandom();
        String funkce = actions.getPositionRandom();
        String unique = actions.uniqueX(6);
        String phone = "+420721" + unique;
        String email = jmeno + prijmeni + unique +"@gmail.com";
        actions.clickOnElement(Nastaveni.addBasicContact);
        actions.ModalGeneralFillFormFO(jmeno, prijmeni, funkce, email, phone);

        driver.navigate().refresh();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);
        table = driver.findElement(By.xpath("//div[@class='block block-newContract--general-contacts']//tbody"));

        // Získání seznamu jedinečných id pro každý řádek
        LinkedList<String> listIdsRow_new = actions.tableGetAllIdRow(table, "label");

        //Získání id nového řádku v tabulce
        Integer id_row = actions.getIdAddedRow(listIdsRow_new, listIdsRow_old);
        actions.actual("Kontrola uložených dat v tabulce", "section");
        actions.tableCellControlValue(table.findElement(By.name("name_"+id_row)).getText(), jmeno + " " + prijmeni, "Název kontaktu");
        actions.tableCellControlValue(table.findElement(By.name("position_"+id_row)).getText(), funkce, "Funkce");
        actions.tableCellControlValue(table.findElement(By.id("email_list"+id_row)).getText(), email, "E-mailové adresy");
        actions.tableCellControlValue(table.findElement(By.id("phone_number_list"+id_row)).getText(), phone, "Tel. čísla");
        actions.tableCellControlValue(table.findElement(By.name("memo_"+id_row)).getText(), "Memo " + jmeno + " " + prijmeni, "Poznámka");
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}