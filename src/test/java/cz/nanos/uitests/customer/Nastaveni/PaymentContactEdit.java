package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;



@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentContactEdit extends AuthorizedScenario {
    public String scenarioDescription = "Editace platebního kontaktu - lze editovat pouze poznámku.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void PaymentContactEdit(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        // tabulka Platební kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block']//tbody"));
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        String unique = actions.uniqueX(10);
        if (rows.size()>0) {
            actions.ModalPaymentContactEdit(table, unique);
            actions.waitInvisibility(By.name("buttonCloseModal"));
            actions.ModalPaymentContactEditControl(table, unique);
        } else {
            System.err.println("Zákazník nemá žádné platební kontaky, proto nelze editovat.");
            throw new Error ("Zákazník nemá žádné platební kontaky, proto nelze editovat.");
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}