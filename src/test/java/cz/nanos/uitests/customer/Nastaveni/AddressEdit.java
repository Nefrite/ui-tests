package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressEdit extends AuthorizedScenario {
    public String scenarioDescription = "Zákazník - editace adresy pro FO, přidána korespondenční i jiná adresa";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void AddressEdit(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);
        //Nastaveni.controlAllElement();

        String town = "Olomouc";
        String psc = "77900";
        String mistni_cast = "Nová Ulice";
        String street = "Litovelská";
        String number_cp = "100";
        String number_op = "19";
        String address = "";

        if (actions.isElementHiddenNow(By.xpath("//button[@value='MAILING'][@title='Upravit adresu']"))) {
            actions.actual("Přidání korespondenční adresy", "section");
            actions.clickOnElement(Nastaveni.addAddressNext);
            actions.selectSetByValue(Nastaveni.addNewAddress, "MAILING");
            actions.clickOnElement(Nastaveni.btn_add_concrete_addr);
            actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
            actions.clickAndControlToast(Nastaveni.btn_save_concrete_addr, "OK", "Není zobrazen toaster s textem.");
            actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
            address = street + " " + number_cp + "/" + number_op + ", " + psc + " " + town + " - " + mistni_cast;
            if (address.equals(actions.getAddressSetting("mailing"))) {
                System.out.println("Adresa 'korespondenční' uložena v pořádku.");
            } else {
                System.err.println("Adresa 'korespondenční' nebyla uložena ve správném formátu.");
                System.err.println(address + " vs " + actions.getAddressSetting("mailing"));
            }
        } else {
            System.out.println("Adresa 'korespondenční' již byla vyplněna");
        }

        if (actions.isElementHiddenNow(By.xpath("//button[@value='OTHER'][@title='Upravit adresu']"))) {
            actions.actual("Přidání jiné adresy", "section");
            actions.clickOnElement(Nastaveni.addAddressNext);
            actions.selectSetByValue(Nastaveni.addNewAddress, "OTHER");
            actions.clickOnElement(Nastaveni.btn_add_concrete_addr);
            actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
            actions.clickAndControlToast(Nastaveni.btn_save_concrete_addr, "OK", "Není zobrazen toaster s textem.");
            actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
            address = street + " " + number_cp + "/" + number_op + ", " + psc + " " + town + " - " + mistni_cast;
            if (address.equals(actions.getAddressSetting("other"))) {
                System.out.println("Adresa 'jiná' uložena v pořádku.");
            } else {
                System.err.println("Adresa 'jiná' nebyla uložena ve správném formátu.");
                System.err.println(address + " vs " + actions.getAddressSetting("other"));
            }
        } else {
            System.out.println("Adresa 'jiná' již byla vyplněna");
        }

        // kontrola zda již nelze přidat další adresa
        //System.out.println("Kontrola, že již nelze přidat další adresu");
        actions.clickAndControlToast(Nastaveni.addAddressNext, "Všechny typy adres byly vyčerpány.", "Není zobrazen toaster s textem.");

        String type = "mailing";
        town = "Zlín";
        psc = "76001";
        mistni_cast = "";
        street = "Mladcovská";
        number_cp = "205";
        number_op = "";
        actions.editAddressSetting(type);
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
        driver.navigate().refresh();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);
        address = street + " " + number_cp + ", " + psc + " " + town + "";

        if (address.equals(actions.getAddressSetting(type))) {
            System.out.println("Adresa 'korespondenční' uložena ve správném formátu.");
        } else {
            System.err.println("Adresa 'korespondenční' nebyla uložena ve správném formátu.");
            System.err.println(address + " vs " + actions.getAddressSetting(type));
        }

        type = "other";
        town = "Zlín";
        psc = "76001";
        mistni_cast = "";
        street = "Mladcovská";
        number_cp = "205";
        number_op = "";
        actions.editAddressSetting(type);
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
        address = street + " " + number_cp + ", " + psc + " " + town + "";
        driver.navigate().refresh();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        if (address.equals(actions.getAddressSetting(type))) {
            System.out.println("Adresa 'jiná' uložena ve správném formátu.");
        } else {
            System.err.println("Adresa 'jiná' nebyla uložena ve správném formátu.");
            System.err.println(address + " vs " + actions.getAddressSetting(type));
        }

        // odstranění adresy jiná a kontrola odstranění
        if (actions.isElementHiddenNow(By.xpath("//button[@value='OTHER'][@title='Odstranit adresu']"))) {
            System.err.println("Adresa z předešlých kroků měla být vytvořena, někde se stala chyba.");
        } else {
            actions.actual("Odstranění jiné adresy", "section");
            actions.clickOnElement(By.xpath("//button[@value='OTHER'][@title='Odstranit adresu']"));
            System.out.println("Adresa 'jiná' byla odstraněna.");
            actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
            if (actions.isElementHiddenNow(By.xpath("//button[@value='OTHER'][@title='Odstranit adresu']"))) {
                System.out.println("Odstranění adresy 'jiná' proběhlo v pořádku");
            } else {
                System.err.println("Adresa 'jiná' nebyla odstraněna");
            }
        }

        // odstranění adresy korespondenční a kontrola odstranění
        if (actions.isElementHiddenNow(By.xpath("//button[@value='MAILING'][@title='Odstranit adresu']"))) {
            System.err.println("Adresa z předešlých kroků měla být vytvořena, někde se stala chyba.");
        } else {
            actions.clickOnElement(By.xpath("//button[@value='MAILING'][@title='Odstranit adresu']"));
            System.out.println("Adresa 'korespondenční' byla odstraněna.");
            actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
            if (actions.isElementHiddenNow(By.xpath("//button[@value='MAILING'][@title='Odstranit adresu']"))) {
                System.out.println("Odstranění adresy 'korespondenční' proběhlo v pořádku");
            } else {
                System.err.println("Adresa 'korespondenční' nebyla odstraněna");
            }
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}