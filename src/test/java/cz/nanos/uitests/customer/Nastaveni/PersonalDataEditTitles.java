package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonalDataEditTitles extends AuthorizedScenario {

    public String scenarioDescription = "Osobní údaje - scénář na kontrolu přidání a odebrání titulů před i za jménem.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void PersonalDataEditTitles(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        actions.actual("Přidávání titulu před jménem", "section");
        actions.addTitle("Prof.", true);
        actions.addTitle("JUDr.", true);
        actions.addTitle("Ing.", true);
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
        actions.actual("Přidávání titulu za jménem", "section");
        actions.addTitle("Ph.D.", false);
        actions.addTitle("LL.M.", false);
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");

        actions.actual("Kontrola přidaných titulů", "section");
        actions.controlAddedTitle("Prof.", true);
        actions.controlAddedTitle("JUDr.", true);
        actions.controlAddedTitle("Ing.", true);
        actions.controlAddedTitle("Ph.D.", false);
        actions.controlAddedTitle("LL.M.", false);

        actions.timeDelay(3000);

        actions.actual("Odstranění titulů", "section");
        actions.delTitle("Prof.", true);
        actions.delTitle("JUDr.", true);
        actions.delTitle("Ing.", true);
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
        actions.delTitle("Ph.D.", false);
        actions.delTitle("LL.M.", false);
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");

        actions.actual("Kontrola odstranění titulů", "section");
        actions.controlDeletedTitle("Prof.", true);
        actions.controlDeletedTitle("JUDr.", true);
        actions.controlDeletedTitle("Ing.", true);
        actions.controlDeletedTitle("Ph.D.", false);
        actions.controlDeletedTitle("LL.M.", false);

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}