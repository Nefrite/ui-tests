package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UseContactAdd extends AuthorizedScenario {
    public String scenarioDescription = "Zákazník - přidání nového 'případu použití'";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void UseContactAdd(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        // tabulka Obecne kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block block-newContract--use-contacts']//tbody"));
        // Počet použití kontaktů
        Integer pocet_old = actions.tableGetRowNumber(table);
        // kontrole elementů na stránce
        actions.clickOnElement(Nastaveni.addContact);

        if (driver.findElements(Nastaveni.contact_select).size() == 0) {
            System.err.println("Všechny kontakty jsou již použity.");
            throw new Error ("Všechny kontakty jsou již použity.");
        } else {
            actions.actual("Kontrola všech elementů na řádku pro přidání contactUsage.", "section");
            actions.elementExistance(Nastaveni.contact_select);
            actions.elementExistance(Nastaveni.contact_general);
            actions.elementExistance(Nastaveni.contact_advances);
            actions.elementExistance(Nastaveni.contact_bill);
            actions.elementExistance(Nastaveni.contact_invoice);
            actions.elementExistance(Nastaveni.contact_marketing);
            actions.elementExistance(Nastaveni.contact_demand);
            actions.elementExistance(Nastaveni.contact_production);
            actions.elementExistance(Nastaveni.contact_save);
            actions.elementExistance(Nastaveni.contact_storno);
            actions.elementExistance(Nastaveni.contact_delete);

            // Kontrola, zda select nabízí kontakt
            Select select_contact1 = new Select(driver.findElement(Nastaveni.contact_select));
            if (select_contact1.getOptions().size() > 1) {
                actions.selectFirst(Nastaveni.contact_select);
                actions.checkboxChecked(Nastaveni.contact_general);
                actions.checkboxChecked(Nastaveni.contact_advances);
                actions.checkboxChecked(Nastaveni.contact_bill);
                actions.checkboxChecked(Nastaveni.contact_invoice);
                actions.checkboxChecked(Nastaveni.contact_marketing);
                actions.checkboxChecked(Nastaveni.contact_demand);
                actions.checkboxChecked(Nastaveni.contact_production);
                actions.clickOnElement(Nastaveni.contact_save);
                actions.waitHideLoader();
                // pokud je tlačítko save pro nový řádek schováno, došlo ke správnému uložení
                if (!actions.isElementHiddenNow(Nastaveni.contact_save)) {
                    System.err.println("Nepodařilo se uložit řádek, stále je otevřen nový k uložení");
                    throw new Error ("Nepodařilo se uložit řádek, stále je otevřen nový k uložení");
                } else {
                    table = driver.findElement(By.xpath("//div[@class='block block-newContract--use-contacts']//tbody"));
                    if ((pocet_old + 1) == actions.tableGetRowNumber(table)) {
                        System.out.println("Přidání použití kontaktu proběhlo v pořádku.");
                        System.out.println("Původně kontaktů: " + pocet_old + ", nově použitých kontaktů:" + actions.tableGetRowNumber(table));
                    } else {
                        System.err.println("Přidání 'použití kontaktu' neproběhlo v pořádku");
                        throw new Error ("Přidání 'použití kontaktu' neproběhlo v pořádku");
                    }
                }
            } else {
                System.err.println("Není možnost vybrat další použití kontaktů, všechny jsou již zadány");
                throw new Error ("Není možnost vybrat další použití kontaktů, všechny jsou již zadány");
            }
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}