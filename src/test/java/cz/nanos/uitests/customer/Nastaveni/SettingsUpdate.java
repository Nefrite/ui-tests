package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.ui.Select;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SettingsUpdate extends AuthorizedScenario {
    public String scenarioDescription = "Nastavení zákazníka - editace a kontrola všech elementů v sekci 'Nastaveni'";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void Settings(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();
        String selected_value = "";
        String value = "";

        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        actions.actual("Editace části formuláře nastavení", "section");
        Select zprostredkovatel = new Select (driver.findElement(Nastaveni.primary_contact_person_id));
        if (zprostredkovatel.getOptions().size()>2) {
            actions.selectSetByIndex(Nastaveni.primary_contact_person_id, 2);
        } else {
            actions.selectSetByIndex(Nastaveni.primary_contact_person_id, 1);
        }
        value = actions.selectGetSelectedValue(Nastaveni.primary_contact_person_id);

        actions.waitDropdownLoadValue(Nastaveni.sales_representative_id);
        if (actions.selectGetSelectedValue(Nastaveni.sales_representative_id) == "2") {
            selected_value = "1";
        } else {
            selected_value = "2";
        }
        actions.selectSetByValue(Nastaveni.sales_representative_id, selected_value);

        actions.checkboxChecked(Nastaveni.person_vip);
        actions.checkboxChecked(Nastaveni.autosend_advance_reminders);
        actions.checkboxChecked(Nastaveni.autosend_advance_invoice);
        actions.checkboxChecked(Nastaveni.autosend_demands);
        actions.checkboxChecked(Nastaveni.autosend_payment_receipts);
        actions.writeText(Nastaveni.person_memo, "Memo");

        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");
        actions.timeDelay(1000);

        actions.actual("Kontrola uložených hodnot", "section");
        actions.compareWebElementCheckbox(Nastaveni.person_vip, true);
        actions.compareWebElementCheckbox(Nastaveni.autosend_advance_reminders, true);
        actions.compareWebElementCheckbox(Nastaveni.autosend_advance_invoice, true);
        actions.compareWebElementCheckbox(Nastaveni.autosend_demands, true);
        actions.compareWebElementCheckbox(Nastaveni.autosend_payment_receipts, true);
        actions.compareWebElementValue(Nastaveni.person_memo, "Memo");
        actions.compareWebElementValue(Nastaveni.sales_representative_id, selected_value);
        actions.compareWebElementValue(Nastaveni.primary_contact_person_id, value);
        actions.actual("Konec kontroly uložených hodnot", "section");

        zprostredkovatel = new Select (driver.findElement(Nastaveni.primary_contact_person_id));
        if (zprostredkovatel.getOptions().size()>2) {
            actions.selectSetByIndex(Nastaveni.primary_contact_person_id, 2);
        } else {
            actions.selectSetByIndex(Nastaveni.primary_contact_person_id, 1);
        }
        value = actions.selectGetSelectedValue(Nastaveni.primary_contact_person_id);

        if (actions.selectGetSelectedValue(Nastaveni.sales_representative_id) == "2") {
            selected_value = "1";
        } else {
            selected_value = "2";
        }
        actions.selectSetByValue(Nastaveni.sales_representative_id, selected_value);

        actions.checkboxUnChecked(Nastaveni.person_vip);
        actions.checkboxUnChecked(Nastaveni.autosend_advance_reminders);
        actions.checkboxUnChecked(Nastaveni.autosend_advance_invoice);
        actions.checkboxUnChecked(Nastaveni.autosend_demands);
        actions.checkboxUnChecked(Nastaveni.autosend_payment_receipts);

        actions.writeText(Nastaveni.person_memo, "Memo update");
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");

        actions.actual("Kontrola uložených hodnot", "section");
        actions.compareWebElementCheckbox(Nastaveni.person_vip, false);
        actions.compareWebElementCheckbox(Nastaveni.autosend_advance_reminders, false);
        actions.compareWebElementCheckbox(Nastaveni.autosend_advance_invoice, false);
        actions.compareWebElementCheckbox(Nastaveni.autosend_demands, false);
        actions.compareWebElementCheckbox(Nastaveni.autosend_payment_receipts, false);
        actions.compareWebElementValue(Nastaveni.person_memo, "Memo update");
        actions.compareWebElementValue(Nastaveni.sales_representative_id, selected_value);
        actions.compareWebElementValue(Nastaveni.primary_contact_person_id, value);
        actions.actual("Konec kontroly uložených hodnot", "section");

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}