package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonalDataEditName extends AuthorizedScenario {
    public String scenarioDescription = "Osobní údaje - editace jména, příjmení a data narození a jejich kontrola.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void PersonalDataEditName(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        String jmeno = actions.getNameRandom();
        String prijmeni = "";
        String datum = "";

        actions.writeText(Nastaveni.personFirstname, jmeno);
        if (driver.findElement(Nastaveni.personLastname).getAttribute("value").equals("Kalina")) {
            prijmeni = "Kalinak";
        } else {
            prijmeni = "Kalina";
        }
        actions.writeText(Nastaveni.personLastname, prijmeni);

        if (driver.findElement(Nastaveni.calendarBirthday).getAttribute("value").equals("1975-11-11")) {
            datum = actions.dateOld();
        } else {
            datum = "11111975";
        }
        actions.setCalendar(Nastaveni.calendarBirthday, datum);
        actions.clickAndControlToast(Nastaveni.buttonSave, "Zákazník upraven.", "Není zobrazen toaster s textem.");

        driver.navigate().refresh();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        actions.actual("Kontrola uložených hodnot", "section");
        datum = datum.substring(4,8) + "-" + datum.substring(2,4) + "-" + datum.substring(0,2);
        actions.compareWebElementValue(Nastaveni.personFirstname, jmeno);
        actions.compareWebElementValue(Nastaveni.personLastname, prijmeni);
        actions.compareWebElementValue(Nastaveni.calendarBirthday, datum);

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}