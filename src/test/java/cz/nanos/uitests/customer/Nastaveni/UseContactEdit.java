package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UseContactEdit extends AuthorizedScenario {

    public String scenarioDescription = "Zákazník - Editace již existujícího 'případu použití', nelze editovat záznam typu telefonní číslo";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void UseContactEdit(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();

        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        // tabulka Obecne kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block block-newContract--use-contacts']//tbody"));
        List<String> id_rows = actions.tableGetAllIdRow(table, "label");

        Integer pocitadlo = 0;
        String aaa = "";
        String typ = "telefon";

        Boolean zmena = false;

        if (!id_rows.isEmpty()) {
            for (String id_row : id_rows) {
                aaa = table.findElement(By.name("value_" + id_row)).getText();
                if (aaa.contains("@")) {
                    typ = "email";
                } else {
                    if (aaa.contains(",")) {
                        typ = "adresa";
                    } else {
                        typ = "telefon";
                    }
                }

                if ((typ == "email") || (typ == "adresa")) {
                    pocitadlo = 0;
                    if (table.findElement(By.name("GENERAL_" + id_row)).isSelected()) pocitadlo++;
                    if (table.findElement(By.name("ADVANCES_" + id_row)).isSelected()) pocitadlo++;
                    if (table.findElement(By.name("BILL_" + id_row)).isSelected()) pocitadlo++;
                    if (table.findElement(By.name("INVOICE_" + id_row)).isSelected()) pocitadlo++;
                    if (table.findElement(By.name("MARKETING_" + id_row)).isSelected()) pocitadlo++;
                    if (table.findElement(By.name("DEMAND_" + id_row)).isSelected()) pocitadlo++;
                    if (table.findElement(By.name("PRODUCTION_" + id_row)).isSelected()) pocitadlo++;
                    // email 7 checkbox, adresa 2 checkbox, telefon 1 checkbox
                    actions.clickOnElement(By.name("btnEdit_" + id_row));
                    if (typ == "email") {
                        switch (pocitadlo) {
                            case 7:
                                WebActions.actual("Editace užití kontaktu", "section");
                                actions.checkboxUnChecked(By.name("GENERAL_" + id_row));
                                actions.checkboxChecked(By.name("ADVANCES_" + id_row));
                                actions.checkboxChecked(By.name("BILL_" + id_row));
                                actions.checkboxUnChecked(By.name("INVOICE_" + id_row));
                                actions.checkboxChecked(By.name("MARKETING_" + id_row));
                                actions.checkboxUnChecked(By.name("DEMAND_" + id_row));
                                actions.checkboxChecked(By.name("PRODUCTION_" + id_row));
                                actions.clickOnElement(By.name("btnSave_" + id_row));
                                WebActions.actual("Kontrola uložených hodnot", "section");
                                actions.checkboxCheckValue(By.name("GENERAL_" + id_row), false);
                                actions.checkboxCheckValue(By.name("ADVANCES_" + id_row), true);
                                actions.checkboxCheckValue(By.name("BILL_" + id_row), true);
                                actions.checkboxCheckValue(By.name("INVOICE_" + id_row), false);
                                actions.checkboxCheckValue(By.name("MARKETING_" + id_row), true);
                                actions.checkboxCheckValue(By.name("DEMAND_" + id_row), false);
                                actions.checkboxCheckValue(By.name("PRODUCTION_" + id_row), true);
                                zmena = true;
                                break;
                            default:
                                WebActions.actual("Editace užití kontaktu", "section");
                                actions.checkboxChecked(By.name("GENERAL_" + id_row));
                                actions.checkboxChecked(By.name("ADVANCES_" + id_row));
                                actions.checkboxChecked(By.name("BILL_" + id_row));
                                actions.checkboxChecked(By.name("INVOICE_" + id_row));
                                actions.checkboxChecked(By.name("MARKETING_" + id_row));
                                actions.checkboxChecked(By.name("DEMAND_" + id_row));
                                actions.checkboxChecked(By.name("PRODUCTION_" + id_row));
                                actions.clickOnElement(By.name("btnSave_" + id_row));
                                WebActions.actual("Kontrola uložených hodnot", "section");
                                actions.checkboxCheckValue(By.name("GENERAL_" + id_row), true);
                                actions.checkboxCheckValue(By.name("ADVANCES_" + id_row), true);
                                actions.checkboxCheckValue(By.name("BILL_" + id_row), true);
                                actions.checkboxCheckValue(By.name("INVOICE_" + id_row), true);
                                actions.checkboxCheckValue(By.name("MARKETING_" + id_row), true);
                                actions.checkboxCheckValue(By.name("DEMAND_" + id_row), true);
                                actions.checkboxCheckValue(By.name("PRODUCTION_" + id_row), true);
                                zmena = true;
                                break;
                        }
                    }
                    if (typ == "adresa") {
                        switch (pocitadlo) {
                            case 2:
                                WebActions.actual("Editace užití kontaktu", "section");
                                actions.checkboxChecked(By.name("BILL_" + id_row));
                                actions.checkboxUnChecked(By.name("INVOICE_" + id_row));
                                actions.clickOnElement(By.name("btnSave_" + id_row));
                                WebActions.actual("Kontrola uložených hodnot", "section");
                                actions.checkboxCheckValue(By.name("BILL_" + id_row), true);
                                actions.checkboxCheckValue(By.name("INVOICE_" + id_row), false);
                                zmena = true;
                                break;
                            case 1:
                                WebActions.actual("Editace užití kontaktu", "section");
                                actions.checkboxChecked(By.name("BILL_" + id_row));
                                actions.checkboxChecked(By.name("INVOICE_" + id_row));
                                actions.clickOnElement(By.name("btnSave_" + id_row));
                                WebActions.actual("Kontrola uložených hodnot", "section");
                                actions.checkboxCheckValue(By.name("BILL_" + id_row), true);
                                actions.checkboxCheckValue(By.name("INVOICE_" + id_row), true);
                                zmena = true;
                                break;
                        }
                    }
                    System.out.println("Použití kontaktu bylo úspěšně editováno.");
                    break;
                } else {
                    System.err.println("Telefonní číslo nelze editovat, pouze jeden checkbox!");
                }
            }
            // prošli se všechny použití kontaktů a ani jeden nebyl email či adresa.
            if (!zmena) {
                System.err.println("Použití kontaktů obsahuje pouze 'telefonní číslo', které nelze editovat.");
                throw new Error ("Použití kontaktů obsahuje pouze 'telefonní číslo', které nelze editovat.");
            }
        } else { // prázdné řádky
            System.err.println("Tabulka použití kontaktů nesmí být prázdná, není žádný záznam k editaci.");
            throw new Error ("Tabulka použití kontaktů nesmí být prázdná, není žádný záznam k editaci.");
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}