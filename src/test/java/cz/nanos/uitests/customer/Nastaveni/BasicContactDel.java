package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.LinkedList;
import java.util.List;




@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicContactDel extends AuthorizedScenario {
    public String scenarioDescription = "Smazání základního kontaktu - přidání kontaktu, jeho kontrola a smazání.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void BasicContactDel(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();

        // tabulka Obecne kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block block-newContract--general-contacts']//tbody"));
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        Integer pocet1 = rows.size();

        // Získání seznamu jedinečných id pro každý řádek
        LinkedList<String> listIdsRow_old = actions.tableGetAllIdRow(table, "label");

        // přidání nového obecného kontaktu
        String jmeno = actions.getNameRandom();
        String prijmeni = actions.getSurnameRandom();
        String funkce = actions.getPositionRandom();
        String unique = actions.uniqueX(6);
        String email = jmeno + prijmeni + "@gmail.com";
        String phone = "+420721" + unique;
        actions.clickOnElement(Nastaveni.addBasicContact);
        actions.ModalGeneralFillFormFO(jmeno, prijmeni, funkce, email, phone);

        table = driver.findElement(By.xpath("//div[@class='block block-newContract--general-contacts']//tbody"));
        // Získání seznamu jedinečných id pro každý řádek
        LinkedList<String> listIdsRow_new = actions.tableGetAllIdRow(table, "label");
        // Získání id nově přidaného řádku
        Integer id_row = actions.getIdAddedRow(listIdsRow_new, listIdsRow_old);

        // získání rozdílného id_radku
        Integer selected = actions.getNumberAddedRow(listIdsRow_new, listIdsRow_old);
        WebElement selected_row = table.findElement(By.xpath("//tr["+selected+"]"));

        List<WebElement> cells = selected_row.findElements(By.tagName("td"));
        actions.actual("Kontrola uložených dat v tabulce", "section");
        actions.tableCellControlValue(table.findElement(By.name("name_"+id_row)).getText(), jmeno + " " + prijmeni, "Název kontaktu");
        actions.tableCellControlValue(table.findElement(By.name("position_"+id_row)).getText(), funkce, "Funkce");
        actions.tableCellControlValue(table.findElement(By.id("email_list"+id_row)).getText(), email, "E-mailové adresy");
        actions.tableCellControlValue(table.findElement(By.id("phone_number_list"+id_row)).getText(), phone, "Tel. čísla");
        actions.tableCellControlValue(table.findElement(By.name("memo_"+id_row)).getText(), "Memo " + jmeno + " " + prijmeni, "Poznámka");

        rows = table.findElements(By.tagName("tr"));
        Integer pocet2 = rows.size();

        actions.contextClick(selected_row).perform();
        actions.clickOnElement(By.xpath("//a[@class='Smazat']"));
        actions.clickOnElement(By.xpath("//body[@class='no-scroll']/div[@id='root']/div[5]/div[1]/div[2]/button[1]"));
        table = driver.findElement(By.xpath("//div[@class='block block-newContract--general-contacts']//tbody"));
        rows = table.findElements(By.tagName("tr"));
        Integer pocet3 = rows.size();

        System.out.println("Původně: " + pocet1 + ", po přidání: " + pocet2 + ", po smazání: "+ pocet3);
        if ((pocet1 == pocet3)) {
            System.out.println("Smazání obecného kontaktu (id: " + id_row + ") proběhlo v pořádku.");
        } else {
            System.err.println("Smazání obecného kontaktu se nezdařilo. Původně: " + pocet1 + ", po přidání: " + pocet2 + ", po smazání: "+ pocet3);
            throw new Error ("Smazání obecného kontaktu se nezdařilo. Původně: " + pocet1 + ", po přidání: " + pocet2 + ", po smazání: "+ pocet3);
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}
