package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.LinkedList;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UseContactDel extends AuthorizedScenario {
    public String scenarioDescription = "Zákazník - odebrání existujícího 'případu použití'";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }
    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void UseContactDel(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        WebElement table = driver.findElement(By.xpath("//div[@class='block block-newContract--use-contacts']//tbody"));
        LinkedList<String> listIdsRow_old = actions.tableGetAllIdRow(table, "label");
        int pocet_old = listIdsRow_old.size();

        if (listIdsRow_old.size() > 0) {
            String id_row = listIdsRow_old.getLast();
            String contact = table.findElement(By.name("value_" + id_row)).getText();
            System.out.println("Použitý kontakt '" + contact + "' bude smazán");
            actions.clickOnElement(By.name("btn_del_" + id_row));
            actions.clickOnElement(By.xpath("//body[contains(@class,'no-scroll')]/div/div[5]/div[1]/div[2]/button[1]"));
            LinkedList<String> listIdsRow_new = actions.tableGetAllIdRow(table, "label");
            int pocet_new = listIdsRow_new.size();
            if ((pocet_new + 1) == pocet_old) {
                System.out.println("Původní počet použití kontaktů: " + pocet_old + ", po smazání: " + pocet_new);
            } else {
                System.err.println("Odstranění použitého kontaktu '" + contact + "' se nezdařilo!");
                throw new Error ("Odstranění použitého kontaktu '" + contact + "' se nezdařilo!");
            }
        } else {
            System.err.println("Tabulka použití kontaktů nesmí být prázdná, není žádný záznam k odstranění.");
            throw new Error ("Tabulka použití kontaktů nesmí být prázdná, není žádný záznam k odstranění.");
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}