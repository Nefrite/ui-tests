package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentContactAddSipo extends AuthorizedScenario {
    public String scenarioDescription = "Přidání platebního kontaktu.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void PaymentContactAdd(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        //Nastaveni.controlAllElement();

        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        // tabulka Platební kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block']//tbody"));
        Integer row_old = table.findElements(By.tagName("tr")).size();

        String payment_type = "sipo";
        String unique = actions.uniqueX(10);
        String nazev = "Platba SIPO";
        String kod_banky = "2010";

        actions.clickOnElement(Nastaveni.addPaymentContact);
        actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);
        table = driver.findElement(By.xpath("//div[@class='block']//tbody"));
        actions.ModalPaymentContactControl(table, payment_type, unique, nazev, kod_banky);
        int row_new = table.findElements(By.tagName("tr")).size();
        System.out.println("Platební kontakty - původně bylo: " + row_old + ", počet přidaných: " + (row_new-row_old));

        if ((row_old + 1) == row_new) {
            System.out.println("Přidání platebního kontaktu proběhlo v pořádku.");
        } else {
            System.err.println("Přidání nového platebního kontaktu skončilo chybou.");
            throw new Error ("Přidání nového platebního kontaktu skončilo chybou.");
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}