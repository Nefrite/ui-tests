package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;




@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentContactDelBank extends AuthorizedScenario {
    public String scenarioDescription = "Smazání platebního kontaktu (typ BÚ), nejdříve je vytvořen nový platební kontakt a pak je smazán.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void PaymentContactDel(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();
        actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

        // tabulka Platební kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block']//tbody"));
        Integer row_old = table.findElements(By.tagName("tr")).size();

        // přidání BÚ
        String payment_type = "bank";
        String unique = actions.uniqueX(10);
        String nazev = "Platba BÚ ke smazání";
        String kod_banky = "2010";

        actions.clickOnElement(Nastaveni.addPaymentContact);
        actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);
        table = driver.findElement(By.xpath("//div[@class='block']//tbody"));
        actions.ModalPaymentContactControl(table, payment_type, unique, nazev, kod_banky);
        actions.ModalPaymentContactDel(table, unique);

        int row_new = table.findElements(By.tagName("tr")).size();

        System.out.println("Platebních konktaktů původně: " + row_old + ", po odstranění: " + row_new);
        if ((row_old) == row_new) {
            System.out.println("Přidání i následné smazání platebního kontaktu proběhlo v pořádku.");
        } else {
            System.err.println("Odstranění platebního konkaktu se nezdařili.");
            throw new Error ("Odstranění platebního kontaktu se nezdařilo.");
        }

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}