package cz.nanos.uitests.customer.Nastaveni;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.pages.modal.ModalAddGeneralContact;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.List;




@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicContactEdit extends AuthorizedScenario {
    public String scenarioDescription = "Editace existujícího obecného kontaktu.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void BasicContactEdit(){
        actions.search(WebActions.search_customer, WebActions.search_customer_url);
        Nastaveni.controlAllElement();

        // tabulka Obecne kontakty
        WebElement table = driver.findElement(By.xpath("//div[@class='block block-newContract--general-contacts']//tbody"));
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        LinkedList<String> listIdsRow = actions.tableGetAllIdRow(table, "label");
        String cislo = listIdsRow.get(0);

        if (!rows.isEmpty()) {
            actions.contextClick(rows.get(0)).perform();
            actions.clickOnElement(By.xpath("//a[@class='Editovat']"));

            String jmeno_new = actions.getNameRandom();
            String prijmeni_new = actions.getSurnameRandom();
            String funkce_new = actions.getPositionRandom();
            String unique_new = actions.uniqueX(6);
            String email_new = jmeno_new + prijmeni_new + "@gmail.com";
            String phone_new = "+420721" + unique_new;

            actions.ModalGeneralFillFormFO(jmeno_new, prijmeni_new, funkce_new, email_new, phone_new);
            actions.waitHideLoader();
            driver.navigate().refresh();
            actions.waitDropdownLoadValue(Nastaveni.primary_contact_person_id);

            table = driver.findElement(By.xpath("//div[@class='block block-newContract--general-contacts']//tbody"));
            actions.tableCellControlValue(table.findElement(By.name("name_"+cislo)).getText(), jmeno_new + " " + prijmeni_new, "Název kontaktu");
            actions.tableCellControlValue(table.findElement(By.name("position_"+cislo)).getText(), funkce_new, "Funkce");
            actions.tableCellControlValue(table.findElement(By.id("email_list"+cislo)).getText(), email_new, "E-mailové adresy");
            actions.tableCellControlValue(table.findElement(By.id("phone_number_list"+cislo)).getText(), phone_new, "Tel. čísla");
            actions.tableCellControlValue(table.findElement(By.name("memo_"+cislo)).getText(), "Memo " + jmeno_new + " " + prijmeni_new, "Poznámka");
        } else {
            System.err.println("Nelze editovat obecný kontakt, protože zákazník žádný nemá.");
            throw new Error ("Nelze editovat obecný kontakt, protože zákazník žádný nemá.");
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}