package cz.nanos.uitests.customer.Opm;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpmSetting extends AuthorizedScenario {

    public String scenarioDescription = "Nastavení OPM a editace většiny hodnot - kontrola uložených hodnot.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    //public static By buttonSave = By.xpath("//div[@class='new-opm-forms']//div//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By dist_network = By.name("dist_network");
    public static By btn_icon = By.name("btn_icon");
    public static By measurement_type = By.name("measurement_type");
    public static By memo = By.name("memo");

    public static By code = By.name("code");
    public static By custom_name = By.name("custom_name");
    public static By phases = By.name("phases");
    public static By capacity = By.name("capacity");
    public static By billPhases = By.name("billPhases");
    public static By distribution_tariff_code = By.name("distribution_tariff_code");
    public static By electrometer_code = By.name("electrometer_code");

    @Test
    public void opmSetting(){
        actions.search(WebActions.search_opm_NN, WebActions.search_opm_NN_opm);
        WebActions.actual("OPM SETTINGS", "page");
        // Kontrola všech prvků na stránce
        // Nastaveni.controlAllElement();

        actions.actual("Uložení původních hodnot:", "section");
        String dist_network_old = actions.getText(dist_network).substring(0,4);
        String measurement_type_old = actions.radioGet(measurement_type);
        String memo_old = actions.getText(memo);
        String code_old = actions.getText(code);
        String custom_name_old = actions.getText(custom_name);
        String phases_old = actions.getText(phases);
        String capacity_old = actions.getText(capacity);
        String distribution_tariff_code_old = actions.getText(distribution_tariff_code);
        String electrometer_code_old = actions.getText(electrometer_code);
        String billPhases_old = "";
        if (driver.findElements(By.xpath("//input[@value='Účtovat']")).isEmpty()) {
            billPhases_old = "Neúčtovat";
        } else {
            billPhases_old = "Účtovat";
        }

        actions.actual("Zadávání nových hodnot:", "section");
        actions.clearText(dist_network);
        actions.writeDistNetwork(dist_network, "0011");
        actions.clickOnElement(btn_icon);
        String town = "Olomouc";
        String psc = "77900";
        String mistni_cast = "Nová Ulice";
        String street = "Legionářská";
        String number_cp = "789";
        String number_op = "4";
        String address = "";
        if (number_cp.isEmpty()) {
            address = street + " " + number_cp + ", " + psc + " " + town + " - " + mistni_cast;
        } else {
            address = street + " " + number_cp + "/" + number_op + ", " + psc + " " + town + " - " + mistni_cast;
        }
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);

        if (address.equals(driver.findElement(By.name("address.title")).getAttribute("value"))) {
            System.out.println("Adresa uložena ve správném formátu. '" + driver.findElement(By.name("address.title")).getAttribute("value") + "'");
        } else {
            System.err.println("Adresa nebyla uložena ve správném formátu. Očekávaná: '" + address + "' vs uložená: '" + driver.findElement(By.name("address.title")).getAttribute("value") + "'");
        }
        actions.radioSet(measurement_type, "C");
        actions.writeText(memo, "Memo poznámka");
        actions.writeText(code, "123456");
        actions.writeText(custom_name, "Značka dočasná");
        actions.selectSetByValue(phases, "3");
        actions.writeText(capacity, "25");
        actions.selectSetByValue(distribution_tariff_code, "C62D");
        actions.writeText(electrometer_code, "CZ123");
        actions.clickAndControlToast(buttonSave, "Data uložena", "Při uložení nastavení OPM došlo k chybě.");

        driver.navigate().refresh();
        actions.actual("REFRESH PAGE", "section");

        actions.timeDelay(10000);

        actions.actual("Kontrola nově uložených hodnot", "section");
        System.out.println("Distributor a síť '" + actions.getText(dist_network) + "'");
        if (actions.getText(dist_network).contains("0011")) {
            System.out.println("Uložená hodnota '" + dist_network + "' je správná. Uložená: '" + actions.getText(dist_network) + "' Ocekavana: '" + "0011" + "'");
        } else {
            System.err.println("Uložená hodnota '" + dist_network + "' je špatně. Uložená: '" + actions.getText(dist_network) + "' Ocekavana: '" + "0011" + "'");
            throw new Error ("Uložená hodnota '" + dist_network + "' je špatně. Uložená: '" + actions.getText(dist_network) + "' Ocekavana: '" + "0011" + "'");
        }
        //actions.compareWebElementValue(dist_network, dist_network_old);
        actions.compareWebElementRadiobutton(measurement_type, "C");
        actions.compareWebElementValue(memo, "Memo poznámka");
        actions.compareWebElementValue(code, "123456");
        actions.compareWebElementValue(custom_name, "Značka dočasná");
        actions.compareWebElementValue(phases, "3");
        actions.compareWebElementValue(capacity, "25");
        actions.compareWebElementValue(distribution_tariff_code, "C62D");
        actions.compareWebElementValue(electrometer_code, "CZ123");
        actions.actual("Konec kontroly nově uložených hodnot", "section");

        System.out.println("Zadávání původních hodnot:");
        actions.writeDistNetwork(dist_network, dist_network_old.substring(0,4));
        actions.radioSet(measurement_type, measurement_type_old);
        actions.writeText(memo, memo_old);
        actions.writeText(code, code_old);
        actions.writeText(custom_name, custom_name_old);
        actions.selectSetByValue(phases, phases_old);
        actions.writeText(capacity, capacity_old);
        actions.selectSetByValue(distribution_tariff_code, distribution_tariff_code_old);
        actions.writeText(electrometer_code, electrometer_code_old);
        //actions.clickOnElement(buttonSave);
        actions.clickAndControlToast(buttonSave, "Data uložena", "Při uložení nastavení OPM došlo k chybě.");
        actions.waitHideLoader();

        driver.navigate().refresh();
        actions.actual("REFRESH PAGE", "section");

        actions.actual("Kontrola původně uložených hodnot", "section");
        String distributor = actions.getText(dist_network);
        if (distributor.contains(dist_network_old)) {
            System.out.println("Uložená hodnota '" + dist_network + "' je správná. Uložená: '" + distributor + "' Ocekavana: '" + dist_network_old + "'");
        } else {
            System.err.println("Uložená hodnota '" + dist_network + "' je špatně. Uložená: '" + distributor + "' Ocekavana: '" + dist_network_old + "'");
        }
        //actions.compareWebElementValue(dist_network, dist_network_old);
        actions.compareWebElementRadiobutton(measurement_type, measurement_type_old);
        actions.compareWebElementValue(memo, memo_old);
        actions.compareWebElementValue(code, code_old);
        actions.compareWebElementValue(custom_name, custom_name_old);
        actions.compareWebElementValue(phases, phases_old);
        actions.compareWebElementValue(capacity, capacity_old);
        actions.compareWebElementValue(distribution_tariff_code, distribution_tariff_code_old);
        actions.compareWebElementValue(electrometer_code, electrometer_code_old);
        actions.actual("Konec kontroly původně uložených hodnot", "section");

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}