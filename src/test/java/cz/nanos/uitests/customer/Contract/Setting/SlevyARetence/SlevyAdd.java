package cz.nanos.uitests.customer.Contract.Setting.SlevyARetence;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;



@RunWith(SpringRunner.class)
@SpringBootTest
public class SlevyAdd extends AuthorizedScenario {
    public String scenarioDescription = "Scénář na přidání nové slevy v nastavení smlouvy";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    //public static By buttonSave = By.xpath("//div[@class='new-opm-forms']//div//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By nastaveni = By.xpath("//a[@name='Nastavení']");
    public static By cenySilElektriny = By.name("Ceny sil. elektřiny");
    public static By cenaZaOpm = By.name("Cena za OPM");
    public static By slevyARetence = By.name("Slevy a retence");

    // Slevy a retence
    public static By addRetence = By.name("btnIcon_AddRetention");
    public static By addSlevy = By.name("btnIcon_AddDis");

    public static By delpt_ean_new = By.name("delpt_ean_new");
    public static By type_new = By.name("type_new");
    public static By name_new = By.name("name_new");
    public static By percent_amount_new = By.name("percent_amount_new");
    public static By use_once = By.name("use_once");
    public static By calendarSinceValiditynew = By.name("calendarSinceValiditynew");
    public static By calendarUntil_new = By.name("calendarUntil_new");
    public static By btnSave_new = By.name("btnSave_new");
    public static By btnStorno_new = By.name("btnStorno_new");
    public static By btnDelete_new = By.xpath("//*[contains(@title, 'Odstranit')");

    public static By table_retence = By.xpath("//div[@class='grid-page']//div[4]//table[1]//tbody[1]");
    public static By table_slevy = By.xpath("//div[7]//table[1]//tbody[1]");

    @Test
    public void slevyAdd() throws Exception {
        actions.search(WebActions.search_opm_NN, WebActions.search_opm_NN_smlouva);
        actions.timeDelay(5000);
        actions.clickOnElement(nastaveni);
        actions.clickOnElement(slevyARetence);

        WebElement table = driver.findElement(table_slevy);
        LinkedList<String> listIdsRow_old = actions.tableGetAllIdRow(table, "select");

        SimpleDateFormat format_from = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format_to = new SimpleDateFormat("ddMMyyyy");
        String valid_from;
        String valid_to;

        if (listIdsRow_old.size() == 0) {
            valid_from = actions.dateUser(0);
            valid_to = actions.dateUser(2);
        } else {
            // poplatky jiz existuji, proto nově vkládaný musi začínat dalsi den
            String id_row_latest =  actions.getIdRowLatestDate(table, "calendarUntil_", "select");
            Calendar date_row = Calendar.getInstance();
            date_row.setTime(format_from.parse(table.findElement(By.name("calendarUntil_"+id_row_latest)).getAttribute("value")));
            date_row.add(Calendar.DATE, 1);
            valid_from = format_to.format(date_row.getTime());
            date_row.add(Calendar.DATE, 3);
            valid_to = format_to.format(date_row.getTime());
        }

        actions.clickOnElement(addSlevy);
        actions.selectSetOtherValue(delpt_ean_new, "123");
        actions.selectSetByText(type_new, "Procenta");
        actions.writeText(name_new, "Název slevy " + actions.uniqueX(3));
        actions.writeText(percent_amount_new, "10");
        actions.selectSetByText(use_once, "Ano");
        actions.setCalendar(calendarSinceValiditynew, valid_from);
        actions.setCalendar(calendarUntil_new, valid_to);
        actions.clickOnElement(btnSave_new);

        valid_from =valid_from.substring(4,8) + "-" + valid_from.substring(2,4) + "-" + valid_from.substring(0,2);
        valid_to =valid_to.substring(4,8) + "-" + valid_to.substring(2,4) + "-" + valid_to.substring(0,2);
        LinkedList<String> listIdsRow_new = actions.tableGetAllIdRow(table, "select");
        Integer id_row = actions.getIdAddedRow(listIdsRow_new, listIdsRow_old);

        actions.actual("Kontrola nově uložených dat v tabulce", "section");
        /*
        tableCellControlValue(table.findElement(By.name("delpt_ean_" + id_row)).getAttribute("value"), "D01d", "EAN");
        tableCellControlValue(table.findElement(By.name("name_" + id_row)).getAttribute("value"), valid_to, "Název");
        tableCellControlValue(table.findElement(By.name("use_once" + id_row)).getAttribute("value"), "Ano", "Jednorázová");
        */
        actions.tableCellControlValue(table.findElement(By.name("type_" + id_row)).getAttribute("value"), "percent", "Typ");
        actions.tableCellControlValue(table.findElement(By.name("percent_amount_" + id_row)).getAttribute("value"), "10", "Výše slevy");
        actions.tableCellControlValue(table.findElement(By.name("calendarSinceValidity" + id_row)).getAttribute("value"), valid_from, "Začátek platnosti");
        actions.tableCellControlValue(table.findElement(By.name("calendarUntil_" + id_row)).getAttribute("value"), valid_to, "Konec platnosti");
        System.out.println("Všechny hodnoty v přidaném řádku jsou v pořádku.");
        actions.actual("Konec kontroly uložených dat v tabulce", "section-close");

        if ((listIdsRow_old.size() + 1) == listIdsRow_new.size()) {
            System.out.println("Nově přidaná sleva s id: '" + id_row + "' je v pořádku");
        } else {
            System.err.println("Při přidání slevy došlo k chybě.");
            throw new Error ("Při přidání slevy došlo k chybě.");
        }

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}