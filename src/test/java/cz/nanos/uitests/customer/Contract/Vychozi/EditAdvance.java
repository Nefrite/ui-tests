package cz.nanos.uitests.customer.Contract.Vychozi;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EditAdvance extends AuthorizedScenario {
    public String scenarioDescription = "Smlouva, záložka Výchozí, editace sekce 'Zálohy' a vrácení na původní hodnotu.";

    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");
    public static By vychozi = By.xpath("//a[@href='" + WebActions.search_opm_NN_smlouva.replaceAll("setting", "default") + "']");

    // zalohy
    public static By advance_frequency = By.name("advance_frequency");
    public static By advance_payment_method = By.name("advance_payment_method");
    public static By advance_payment_contact = By.name("advance_payment_contact");
    public static By advance_payment_add = By.xpath("//div[@class='block']//div[2]//button[1]");
    public static By advance_payment_del = By.xpath("//div[@class='block']//div[2]//button[2]");

    // vyúčtování
    public static By bill_frequency = By.name("bill_frequency");
    public static By bill_payable_period = By.name("bill_payable_period");
    public static By overpayment_repay_strgy_code = By.name("overpayment_repay_strgy_code");
    public static By overpayment_payment_method = By.name("overpayment_payment_method");
    public static By debt_payment_method = By.name("debt_payment_method");
    public static By exclude_automatic_bill = By.name("exclude_automatic_bill");

    public static By overpayment_payment_contact = By.name("overpayment_payment_contact");
    public static By overpayment_payment_contact_add = By.xpath("//div[@class='items-wrapper undefined']//div[3]//button[1]");
    public static By overpayment_payment_contact_del = By.xpath("//div[@class='items-wrapper undefined']//div[3]//button[2]");

    public static By debt_payment_contact = By.name("debt_payment_contact");
    public static By debt_payment_contact_add = By.xpath("//div[@class='grid-page']//div[4]//button[1]");
    public static By debt_payment_contact_del = By.xpath("//div[@class='grid-page']//div[4]//button[2]");

    @Test
    public void EditAdvance(){
        actions.search(actions.search_opm_NN, actions.search_opm_NN_smlouva);
        actions.clickOnElement(vychozi);

        actions.waitDropdownLoadValue(advance_frequency);
        actions.actual("Uložení původních hodnot:", "section");
        String frekvence_old = actions.selectGetSelectedValue(advance_frequency);
        String zpusob_uhrady_old = actions.selectGetSelectedValue(advance_payment_method);
        String kontakt_uhrady_old = actions.getText(advance_payment_contact);
        System.out.println("All old value: " + frekvence_old + " " + zpusob_uhrady_old + " " + kontakt_uhrady_old);

        actions.actual("Editace na nové hodnoty:", "section");
        actions.selectSetOtherValue(advance_frequency, frekvence_old);
        String frekvence_edit = actions.selectGetSelectedValue(advance_frequency);
        actions.selectSetOtherValue(advance_payment_method, zpusob_uhrady_old);
        String zpusob_uhrady_edit = actions.selectGetSelectedValue(advance_payment_method);
        actions.clickOnElement(advance_payment_add);
        WebElement table = driver.findElement(By.xpath("//table[contains(@class,'pure-table')]//tbody"));
        int vyber = actions.getRandom(1, table.findElements(By.tagName("tr")).size());
        String kontakt_uhrady_edit = driver.findElement(By.xpath("//tr[" + vyber + "]//td[3]")).getText();
        actions.clickOnElement(By.xpath("//tr[" + vyber + "]//td[1]"));
        actions.clickAndControlToast(buttonSave, "Data uložena", "Při uložení záloh došlo k chybě.");

        actions.actual("Kontrola uložených hodnot:", "section");
        actions.compareWebElementValue(advance_frequency, frekvence_edit);
        actions.compareWebElementValue(advance_payment_method, zpusob_uhrady_edit);
        actions.compareWebElementValue(advance_payment_contact, kontakt_uhrady_edit);

        actions.actual("Editace na původní hodnoty:", "section");
        actions.selectSetByValue(advance_frequency, frekvence_old);
        actions.selectSetByValue(advance_payment_method, zpusob_uhrady_old);
        actions.clickOnElement(advance_payment_add);
        table = driver.findElement(By.xpath("//table[contains(@class,'pure-table')]//tbody"));
        WebElement child = table.findElement(By.xpath("//td[contains(text(),'" + kontakt_uhrady_old + "')]"));
        WebElement parent = child.findElement(By.xpath("./.."));
        parent.findElement(By.xpath("td[1]")).click();
        actions.clickAndControlToast(buttonSave, "Data uložena", "Při uložení záloh došlo k chybě.");

        actions.actual("Kontrola původně uložených hodnot:", "section");
        actions.compareWebElementValue(advance_frequency, frekvence_old);
        actions.compareWebElementValue(advance_payment_method, zpusob_uhrady_old);
        actions.compareWebElementValue(advance_payment_contact, kontakt_uhrady_old);

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}