package cz.nanos.uitests.customer.Contract.Setting.Setting;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractSettingSettings extends AuthorizedScenario {
    public String scenarioDescription = "Editace nastavení smlouvy - editace, kontrola a vrácení původních hodnot";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By nastaveni = By.xpath("//a[@name='Nastavení']");
    public static By cenySilElektriny = By.name("Ceny sil. elektřiny");
    public static By cenaZaOpm = By.name("Cena za OPM");
    public static By slevyARetence = By.name("Slevy a retence");

    // Nastaveni
    public static By btnIcon_EditProduct = By.name("btnIcon_EditProduct");
    public static By consumption_tariff_id = By.name("consumption_tariff_id");
    public static By buttonCreateRequest = By.name("buttonCreateRequest");
    public static By calendarDateChange = By.name("calendarDateChange");

    public static By contact_person_id = By.name("contact_person_id");
    public static By client_code = By.name("client_code");
    public static By memo = By.name("memo");
    public static By summary_advance = By.name("summary_advance");
    public static By summary_bill = By.name("summary_bill");
    public static By consumption_only = By.name("consumption_only");
    public static By use_default_contact_setting = By.name("use_default_contact_setting");

    @Test
    public void contractSetting(){
        actions.search(WebActions.search_opm_NN, WebActions.search_opm_NN_smlouva);
        actions.clickOnElement(nastaveni);
        actions.waitVisibility(cenySilElektriny);
        actions.waitDropdownLoadValue(contact_person_id);

        actions.actual("Uložení původních hodnot:", "section");
        String contact_person_id_old = actions.selectGetSelectedValue(contact_person_id);
        String client_code_old = actions.getText(client_code);
        String memo_old = actions.getText(memo);
        Boolean summary_advance_old = actions.checkboxGetValue(summary_advance);
        Boolean summary_bill_old = actions.checkboxGetValue(summary_bill);
        Boolean consumption_only_old = actions.checkboxGetValue(consumption_only);
        Boolean use_default_contact_setting_old = actions.checkboxGetValue(use_default_contact_setting);

        actions.actual("Zadávání nových hodnot:", "section");
        actions.writeText(client_code, "New");
        actions.writeText(memo, "New");
        actions.selectSetOtherValue(contact_person_id , contact_person_id_old);
        actions.checkboxReverseValue(summary_advance);
        actions.checkboxReverseValue(summary_bill);
        actions.checkboxReverseValue(consumption_only);
        actions.checkboxReverseValue(use_default_contact_setting);

        actions.clickAndControlToast(buttonSave, "Nastavení aktualizováno.", "Uložení neproběhlo v pořádku.");
        actions.waitVisibility(cenySilElektriny);
        actions.waitDropdownLoadValue(contact_person_id);

        actions.actual("Kontrola nově uložených hodnot", "section");
        actions.compareWebElementValue(client_code, "New");
        actions.compareWebElementValue(memo, "New");
        actions.compareWebElementCheckbox(summary_advance, !(summary_advance_old));
        actions.compareWebElementCheckbox(summary_bill, !(summary_bill_old));
        actions.compareWebElementCheckbox(consumption_only, !(consumption_only_old));
        actions.compareWebElementCheckbox(use_default_contact_setting, !(use_default_contact_setting_old));

        actions.actual("Zadávání původních hodnot:", "section");
        actions.writeText(client_code, client_code_old);
        actions.writeText(memo, memo_old);
        actions.selectSetByValue(contact_person_id , contact_person_id_old);
        actions.checkboxReverseValue(summary_advance);
        actions.checkboxReverseValue(summary_bill);
        actions.checkboxReverseValue(consumption_only);
        actions.checkboxReverseValue(use_default_contact_setting);

        actions.clickAndControlToast(buttonSave, "Nastavení aktualizováno.", "Uložení neproběhlo v pořádku.");
        actions.waitVisibility(cenySilElektriny);
        actions.waitDropdownLoadValue(contact_person_id);

        actions.actual("Kontrola původně uložených hodnot", "section");
        actions.compareWebElementValue(client_code, client_code_old);
        actions.compareWebElementValue(memo, memo_old);
        actions.compareWebElementValue(contact_person_id, contact_person_id_old);
        actions.compareWebElementCheckbox(summary_advance, summary_advance_old);
        actions.compareWebElementCheckbox(summary_bill, summary_bill_old);
        actions.compareWebElementCheckbox(consumption_only, consumption_only_old);
        actions.compareWebElementCheckbox(use_default_contact_setting, use_default_contact_setting_old);

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}