package cz.nanos.uitests.customer.Contract.Setting.Setting;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static cz.nanos.uitests.service.WebActions.timeDelay;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractSettingSettingsChangeProduct extends AuthorizedScenario {
    public String scenarioDescription = "Změna produktu na existující smlouvě";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    //public static By buttonSave = By.xpath("//div[@class='new-opm-forms']//div//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By nastaveni = By.xpath("//a[@name='Nastavení']");
    public static By cenySilElektriny = By.name("Ceny sil. elektřiny");
    public static By cenaZaOpm = By.name("Cena za OPM");
    public static By slevyARetence = By.name("Slevy a retence");

    // Nastaveni
    public static By btnIcon_EditProduct = By.name("btnIcon_EditProduct");
    public static By consumption_tariff_id = By.name("consumption_tariff_id");
    public static By buttonCreateRequest = By.name("buttonCreateRequest");
    public static By calendarDateChange = By.name("calendarDateChange");

    @Test
    public void contractSetting(){
        actions.search(WebActions.search_opm_NN, WebActions.search_opm_NN_smlouva);
        actions.clickOnElement(nastaveni);
        actions.waitVisibility(cenySilElektriny);

        // zrušení pomocné smlouvy, pokud nějaká je již vytvořená (jinak ošetření změny)
        if (driver.findElements(By.partialLinkText("Změna ")).isEmpty()) {
            //System.err.println("Žádná změna není aktivní");
        } else {
            actions.clickOnElement(By.partialLinkText("Změna "));
            actions.clickOnElement(By.name("buttonCancelContract"));
            actions.clickAndControlToast(By.name("saveButton"), "Pomocná smlouva zrušena", "Při rušení pomocné smlouvy došlo k chybě");
            actions.clickOnElement(By.xpath("//a[contains(@href, '" + WebActions.search_opm_NN_smlouva + "')]"));
            System.out.println("Původní změnová smlouva byla úspěšně odstraněna. \n");
        }

        actions.clickOnElement(nastaveni);
        actions.clickOnElement(btnIcon_EditProduct);
        actions.selectSetOtherValue(consumption_tariff_id, actions.selectGetSelectedValue(consumption_tariff_id));
        actions.setCalendar(calendarDateChange, actions.dateUser(100));
        actions.clickAndControlToast(buttonCreateRequest, "Požadavek na změnu založen", "Nepodařilo se");

        driver.navigate().refresh();
        actions.waitVisibility(cenySilElektriny);

        if (driver.findElements(By.partialLinkText("Změna ")).isEmpty()) {
            System.err.println("Při vytvoření změnové smlouvy došlo k chybě, není zobrazena záložka se změnovou smlouvou!");
            throw new Error ("Při vytvoření změnové smlouvy došlo k chybě, není zobrazena záložka se změmovou smlouvou!");
        } else {
            System.out.println("Změnová smlouva byla úspěšně založena.");
        }
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}