package cz.nanos.uitests.customer.Contract;

import cz.nanos.uitests.pages.NewContract.NewContractStepFive;
import cz.nanos.uitests.pages.NewContract.NewContractStepFour;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;



@RunWith(SpringRunner.class)
@SpringBootTest
public class AddOpm extends AuthorizedScenario {
    public String scenarioDescription = "Přidání OPM ke smlouvě.";

    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    // krok 1
    public static By buttonAddOpm = By.name("buttonAddOpm");
    public static By buttonSearch = By.name("buttonSearch");
    public static By addSearchEan = By.name("addSearchEan");

    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By dist_network = By.name("dist_network");
    public static By btn_icon = By.name("btn_icon");
    public static By measurement_type = By.name("measurement_type");
    public static By memo = By.name("memo");

    public static By code = By.name("code");
    public static By custom_name = By.name("custom_name");
    public static By phases = By.name("phases");
    public static By capacity = By.name("capacity");
    public static By billPhases = By.name("billPhases");
    public static By distribution_tariff_code = By.name("distribution_tariff_code");
    public static By electrometer_code = By.name("electrometer_code");

    public static By buttonNextStep = By.name("buttonNextStep");

    //krok 2
    // Výchozí nastavení
    public static By o_buttonSave = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[4]/button[1]");
    public static By o_buttonCancelChange = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[4]/button[2]");

    // Zálohy
    public static By oz_advance_frequency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/select[1]");
    public static By oz_advance_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/select[1]");

    public static By oz_advance_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/input[1]");
    public static By oz_button_add_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[2]/button[1]");
    public static By oz_button_del_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[2]/button[2]");

    // Vyúčtování
    public static By ov_bill_frequency = By.name("bill_frequency");
    public static By ov_bill_payable_period = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/input[1]");
    public static By ov_overpayment_repay_strgy_code = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/select[1]");
    public static By ov_exclude_automatic_bill = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[2]/div[1]/input[1]");
    public static By ov_overpayment_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[3]/div[1]/select[1]");
    public static By ov_overpayment_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[3]/div[1]/input[1]");

    public static By ov_button_add_preplatek = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[3]/button[1]");
    public static By ov_button_del_preplatek = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[3]/button[2]");
    public static By ov_debt_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[4]/div[1]/select[1]");
    public static By ov_debt_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[4]/div[1]/input[1]");
    public static By ov_button_add_contact_preplatek = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[4]/button[1]");
    public static By ov_button_del_contact_preplatek = By.xpath("//div[@class='items-wrapper undefined']//div[4]//button[2]");

    // tlačítko pro pokračování formuláře
    public static By button_2_1 = By.xpath("//span[@class='step-number'][contains(text(), '2.1') or contains(text(), '5.1')]");

    // druhá část po kliknutí na 5.1.

    public static By n_buttonSave = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/button[1]");
    public static By n_buttonCancelChange = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/button[2]");

    // Dodávka
    public static By nd_delivery_start_at = By.name("delivery_start_at");
    public static By nd_delivery_end_at = By.name("delivery_end_at");
    public static By nd_contract_condition_code = By.name("contract_condition_code");

    // Zálohy
    public static By nz_advance_amount = By.name("advance_amount");
    public static By nz_use_default_advance = By.name("use_default_advance");
    public static By nz_advance_frequency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/select[1]");
    public static By nz_advance_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/select[1]");
    public static By nz_advance_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/input[1]");
    public static By nz_button_add_contact_payment = By.xpath("//div[@class='tab-content']//div[@class='page']//div//button[@title='Přidat kontakt']");
    public static By nz_button_del_contact_payment = By.xpath("//div[@class='tab-content']//div[@class='page']//div//button[@title='Odstranit kontakt']");

    // vyúčtování
    public static By nv_use_default_billing = By.name("use_default_billing");
    public static By nv_bill_frequency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]");
    public static By nv_bill_payable_period = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[1]/div[1]/input[1]");

    public static By nv_overpayment_repay_strgy_code = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/select[1]");
    public static By nv_exclude_automatic_bill = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[1]/input[1]");
    public static By nv_overpayment_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[1]/select[1]");
    public static By nv_overpayment_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[3]/div[1]/input[1]");
    public static By nv_button_add_contact_preplatek = By.xpath("//div[@class='items-wrapper undefined']//div[2]//div[2]//div[3]//button[1]");
    public static By nv_button_del_contact_preplatek = By.xpath("//div[@class='items-wrapper undefined']//div[2]//div[2]//div[3]//button[2]");
    public static By nv_debt_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[4]/div[1]/select[1]");
    public static By nv_debt_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[4]/div[1]/input[1]");
    public static By nv_button_add_contact_nedoplatek = By.xpath("//div[@class='contract-opms-wrapper']//div[4]//button[1]");
    public static By nv_button_del_contact_nedoplatek = By.xpath("//div[@class='contract-opms-wrapper']//div[4]//button[2]");

    // výpověď
    public static By v_terminate_previous = By.name("terminate_previous");
    public static By v_previous_trader_id = By.name("previous_trader_id");
    public static By v_termination_period_unit = By.name("termination_period_unit");
    public static By v_termination_period = By.name("termination_period");
    public static By v_termination_date = By.name("termination_date");
    public static By v_contract_document= By.name("contract_document");
    public static By v_button_add_contact_vypoved = By.xpath("//div[@class='formGroupInputLabel']//button[@title='Přidat kontakt']");
    public static By v_button_del_contact_vypoved = By.xpath("//div[@class='formGroupInputLabel']//button[@title='Odstranit kontakt']");

    @Test
    public void EditAdvance(){
        actions.search(actions.search_opm_NN, actions.search_opm_NN_smlouva);

        //WebElement aaa = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[2]/ul[1]"));
        //actions.waitVisibility(By.name("Slevy a retence"));
        //int pocet_li = aaa.findElements(By.tagName("li")).size();

        actions.clickOnElement(buttonAddOpm);
        actions.actual("KROK 1 - OPM", "page");
        actions.timeDelay(500);

        String parentWindow = driver.getWindowHandle();
        String newOpn = "";
        Set<String> handles = driver.getWindowHandles();
        for(String windowHandle  : handles)
        {
            if(!windowHandle.equals(parentWindow))
            {
                newOpn = windowHandle;
                driver.switchTo().window(newOpn);
            }
        }
        actions.timeDelay(500);

        String EAN = WebActions.getEAN("");
        actions.writeText(addSearchEan, EAN);
        actions.clickAndControlToast(buttonSearch, "EAN nenalezen", "Zadané EAN je již použité, " +
                "proto nelze pokračovat dále v zakládání OPM.");
        actions.actual("Obecné", "section");
        actions.writeDistNetwork(dist_network, "0011");
        actions.clickOnElement(btn_icon);

        String town = "Břeclav";
        String psc = "69002";
        String mistni_cast = "";
        String street = "Fintajsova";
        String number_cp = "1974";
        String number_op = "5";
        String address = "";

        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);

        address = street + " " + number_cp;
        if (number_op.isEmpty()) {
            address += ", " + psc + " " + town;
        } else {
            address += "/" + number_op + ", " + psc + " " + town;
        }
        if (!mistni_cast.isEmpty()) {
            address += " - " + mistni_cast;
        }

        if (address.equals(driver.findElement(By.name("address.title")).getAttribute("value"))) {
            System.out.println("Adresa uložena ve správném formátu. '" + driver.findElement(By.name("address.title")).getAttribute("value") + "'");
        } else {
            System.err.println("Adresa nebyla uložena ve správném formátu. Očekávaná: '" + address + "' vs uložená: '" + driver.findElement(By.name("address.title")).getAttribute("value") + "'");
        }

        actions.radioSet(measurement_type, "C");
        actions.writeText(memo, "Memo poznámka pro nové OPM ke stávající smlouvě");

        actions.actual("Odběr", "section");
        actions.writeText(code, "123456");
        actions.writeText(custom_name, "Značka odběru");
        actions.selectSetByValue(phases, "3");
        actions.writeText(capacity, "25");
        actions.selectSetByValue(distribution_tariff_code, "C62D");
        actions.writeText(electrometer_code, "CZ123");
        actions.clickOnElement(buttonSave);

        // kontrola zda se řádek přidal
        if (driver.findElements(By.xpath("//label[contains(text(), '" + EAN + "')]")).size() != 1) {
            System.err.println("Problém s EAN");
            throw new Error ("Problém s EAN");
        }
        actions.clickOnElement(buttonNextStep);

        actions.actual("KROK 2 - Nastavení OPM", "page");

        // Výchozí nastavení zálohy
        actions.selectSetByText(oz_advance_frequency, "Měsíční");
        actions.selectSetByText(oz_advance_payment_method, "Příkaz");
        actions.clickOnElement(oz_button_add_contact);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));

        // Výchozí nastavení Vyúř
        actions.selectSetByText(ov_bill_frequency, "Pololetní");
        actions.writeText(ov_bill_payable_period, "20");
        actions.selectSetByText(ov_overpayment_repay_strgy_code, "Úhrada/vratka");
        actions.checkboxUnChecked(ov_exclude_automatic_bill);
        actions.selectSetByText(ov_overpayment_payment_method, "Úhrada/vratka příkazem");
        actions.clickOnElement(ov_button_add_preplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.selectSetByText(ov_debt_payment_method, "Příkaz");
        actions.clickOnElement(ov_button_add_contact_preplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.clickAndControlToast(o_buttonSave, "Data uložena", "Uložení 'Výchozích dat' OPM neproběhlo v pořádku");

        // pokračování stránky - pro konkrétní OPM
        actions.clickOnElement(button_2_1);

        // OPM dodávka
        actions.setCalendar(nd_delivery_start_at, actions.dateUser(1));
        actions.setCalendar(nd_delivery_end_at, actions.dateUser(11));
        actions.selectSetByText(nd_contract_condition_code, "Nový odběr");

        // OPM zálohy
        actions.writeText(nz_advance_amount, "450");
        actions.checkboxUnChecked(nz_use_default_advance);
        actions.selectSetByText(nz_advance_frequency, "Čtvrtletní");
        actions.selectSetByText(nz_advance_payment_method, "Složenka");
        actions.clickOnElement(nz_button_del_contact_payment);
        actions.clickOnElement(nz_button_add_contact_payment);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));

        // OPM vyúčtování
        actions.checkboxUnChecked(nv_use_default_billing);
        actions.selectSetByText(nv_bill_frequency, "Dle odečtu");
        actions.writeText(nv_bill_payable_period, "25");
        actions.selectSetByText(nv_overpayment_repay_strgy_code, "Úhrada/vratka");
        actions.checkboxUnChecked(nv_exclude_automatic_bill);
        actions.selectSetByText(nv_overpayment_payment_method, "Úhrada/vratka složenkou");
        actions.clickOnElement(nv_button_del_contact_preplatek);
        actions.clickOnElement(nv_button_add_contact_preplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.selectSetByText(nv_debt_payment_method, "Složenka");
        actions.clickOnElement(nv_button_del_contact_nedoplatek);
        actions.clickOnElement(nv_button_add_contact_nedoplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));

        // OPM výpověď
        actions.checkboxUnChecked(v_terminate_previous);

        actions.clickOnElement(n_buttonSave);
        actions.clickOnElement(buttonNextStep);

        actions.clickOnElement(By.name("saveButton"));

        actions.actual("KROK 3 - Shrnutí OPM", "page");
        // do kroku 3 se nelze dostat

        driver.close();
        driver.switchTo().window(parentWindow);
        driver.navigate().refresh();

        actions.waitVisibility(By.name("Slevy a retence"));
        /*aaa = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[2]/ul[1]"));
        if (pocet_li == aaa.findElements(By.tagName("li")).size()) {
            System.err.println("OPM nebylo přidáno - krok 3 chybí");
            throw new Error ("OPM nebylo přidáno - krok 3 chybí");
        } else {
            System.out.println("OPM bylo přidáno.");
        }
        */
        actions.timeDelay(10000);
    }
}