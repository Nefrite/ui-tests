package cz.nanos.uitests.customer.Contract.Vychozi;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EditVyuctovani extends AuthorizedScenario {
    public String scenarioDescription = "Smlouva, záložka Výchozí, editace sekce 'Vyúčtování' a následné navrácení na původní hodnoty.";

    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");
    public static By vychozi = By.xpath("//a[@href='" + WebActions.search_opm_NN_smlouva.replaceAll("setting", "default") + "']");

    // zalohy
    public static By advance_frequency = By.name("advance_frequency");
    public static By advance_payment_method = By.name("advance_payment_method");
    public static By advance_payment_contact = By.name("advance_payment_contact");
    public static By advance_payment_add = By.xpath("//div[@class='block']//div[2]//button[1]");
    public static By advance_payment_del = By.xpath("//div[@class='block']//div[2]//button[2]");

    // vyúčtování
    public static By bill_frequency = By.name("bill_frequency");
    public static By bill_payable_period = By.name("bill_payable_period");
    public static By overpayment_repay_strgy_code = By.name("overpayment_repay_strgy_code");
    public static By exclude_automatic_bill = By.name("exclude_automatic_bill");
    public static By overpayment_payment_method = By.name("overpayment_payment_method");

    public static By overpayment_payment_contact = By.name("overpayment_payment_contact");
    public static By overpayment_payment_contact_add = By.xpath("//div[@class='items-wrapper undefined']//div[3]//button[1]");
    public static By overpayment_payment_contact_del = By.xpath("//div[@class='items-wrapper undefined']//div[3]//button[2]");

    public static By debt_payment_method = By.name("debt_payment_method");

    public static By debt_payment_contact = By.name("debt_payment_contact");
    public static By debt_payment_contact_add = By.xpath("//div[@class='grid-page']//div[4]//button[1]");
    public static By debt_payment_contact_del = By.xpath("//div[@class='grid-page']//div[4]//button[2]");

    @Test
    public void EditAdvance(){
        actions.search(actions.search_opm_NN, actions.search_opm_NN_smlouva);
        actions.clickOnElement(vychozi);

        actions.actual("Uložení původních hodnot:", "section");
        String frekvence_old = actions.selectGetSelectedValue(bill_frequency);
        String splatnost_old = actions.getText(bill_payable_period);
        String vyporadani_preplatku_old = actions.selectGetSelectedValue(overpayment_repay_strgy_code);
        Boolean vyjmout_z_automatickeho_old = actions.checkboxGetValue(exclude_automatic_bill);
        String uhrada_preplatku_old = actions.selectGetSelectedValue(overpayment_payment_method);
        String kontakt_uhrady_preplatku_old = actions.getText(overpayment_payment_contact);
        String uhrada_nedoplatku_old = actions.selectGetSelectedValue(debt_payment_method);
        String kontakt_uhrady_nedoplatku_old = actions.getText(debt_payment_contact);

        actions.actual("Editace na nové hodnoty:", "section");
        actions.selectSetOtherValue(bill_frequency, frekvence_old);
        String frekvence_edit = actions.selectGetSelectedValue(bill_frequency);
        int splatnost_edit = Integer.parseInt(splatnost_old) + 5;
        actions.writeText(bill_payable_period, "" + splatnost_edit);
        actions.selectSetOtherValue(overpayment_repay_strgy_code, vyporadani_preplatku_old);
        String vyporadani_preplatku_edit = actions.selectGetSelectedValue(overpayment_repay_strgy_code);
        actions.checkboxReverseValue(exclude_automatic_bill);
        actions.selectSetOtherValue(overpayment_payment_method, uhrada_preplatku_old);
        String uhrada_preplatku_edit = actions.selectGetSelectedValue(overpayment_payment_method);

        actions.clickOnElement(overpayment_payment_contact_add);
        WebElement table = driver.findElement(By.xpath("//table[contains(@class,'pure-table')]//tbody"));
        int vyber = actions.getRandom(1, table.findElements(By.tagName("tr")).size());
        String kontakt_uhrady_preplatku_edit = driver.findElement(By.xpath("//tr[" + vyber + "]//td[3]")).getText();
        actions.clickOnElement(By.xpath("//tr[" + vyber + "]//td[1]"));

        actions.selectSetOtherValue(debt_payment_method, uhrada_preplatku_old);
        String uhrada_nedoplatku_edit = actions.selectGetSelectedValue(debt_payment_method);

        actions.clickOnElement(debt_payment_contact_add);
        table = driver.findElement(By.xpath("//table[contains(@class,'pure-table')]//tbody"));
        vyber = actions.getRandom(1, table.findElements(By.tagName("tr")).size());
        String kontakt_uhrady_nedoplatku_edit = driver.findElement(By.xpath("//tr[" + vyber + "]//td[3]")).getText();
        actions.clickOnElement(By.xpath("//tr[" + vyber + "]//td[1]"));
        actions.clickAndControlToast(buttonSave, "Data uložena", "Při uložení záloh došlo k chybě.");

        actions.actual("Kontrola nově uložených hodnot:", "section");
        actions.compareWebElementValue(bill_frequency, frekvence_edit);
        actions.compareWebElementValue(bill_payable_period, "" + splatnost_edit);
        actions.compareWebElementValue(overpayment_repay_strgy_code, vyporadani_preplatku_edit);
        actions.compareWebElementCheckbox(exclude_automatic_bill, !vyjmout_z_automatickeho_old);
        actions.compareWebElementValue(overpayment_payment_method, uhrada_preplatku_edit);
        actions.compareWebElementValue(overpayment_payment_contact, kontakt_uhrady_preplatku_edit);
        actions.compareWebElementValue(debt_payment_method, uhrada_nedoplatku_edit);
        actions.compareWebElementValue(debt_payment_contact, kontakt_uhrady_nedoplatku_edit);

        actions.waitHideToast();

        actions.actual("Editace na původní hodnoty:", "section");
        actions.selectSetByValue(bill_frequency, frekvence_old);
        actions.writeText(bill_payable_period, splatnost_old);
        actions.selectSetByValue(overpayment_repay_strgy_code, vyporadani_preplatku_old);
        actions.checkboxReverseValue(exclude_automatic_bill);

        actions.selectSetByValue(overpayment_payment_method, uhrada_preplatku_old);
        actions.clickOnElement(overpayment_payment_contact_add);
        table = driver.findElement(By.xpath("//table[contains(@class,'pure-table')]//tbody"));
        WebElement child = table.findElement(By.xpath("//td[contains(text(),'" + kontakt_uhrady_preplatku_old + "')]"));
        WebElement parent = child.findElement(By.xpath("./.."));
        parent.findElement(By.xpath("td[1]")).click();

        actions.selectSetByValue(debt_payment_method, uhrada_nedoplatku_old);
        actions.clickOnElement(debt_payment_contact_add);
        table = driver.findElement(By.xpath("//table[contains(@class,'pure-table')]//tbody"));
        child = table.findElement(By.xpath("//td[contains(text(),'" + kontakt_uhrady_nedoplatku_old + "')]"));
        parent = child.findElement(By.xpath("./.."));
        parent.findElement(By.xpath("td[1]")).click();

        actions.actual("Kontrola původně uložených hodnot:", "section");
        actions.compareWebElementValue(bill_frequency, frekvence_old);
        actions.compareWebElementValue(bill_payable_period, splatnost_old);
        actions.compareWebElementValue(overpayment_repay_strgy_code, vyporadani_preplatku_old);
        actions.compareWebElementCheckbox(exclude_automatic_bill, vyjmout_z_automatickeho_old);
        actions.compareWebElementValue(overpayment_payment_method, uhrada_preplatku_old);
        actions.compareWebElementValue(overpayment_payment_contact, kontakt_uhrady_preplatku_old);
        actions.compareWebElementValue(debt_payment_method, uhrada_nedoplatku_old);
        actions.compareWebElementValue(debt_payment_contact, kontakt_uhrady_nedoplatku_old);

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}