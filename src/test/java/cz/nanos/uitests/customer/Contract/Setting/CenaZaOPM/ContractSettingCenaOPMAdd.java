package cz.nanos.uitests.customer.Contract.Setting.CenaZaOPM;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;



@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractSettingCenaOPMAdd extends AuthorizedScenario {
    public String scenarioDescription = "Přidání platby za OPM";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    //public static By buttonSave = By.xpath("//div[@class='new-opm-forms']//div//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By nastaveniTop = By.xpath("//a[contains(@href, '" + WebActions.search_opm_NN_smlouva + "')]");

    public static By nastaveni = By.xpath("//a[@name='Nastavení']");
    public static By cenySilElektriny = By.name("Ceny sil. elektřiny");
    public static By cenaZaOpm = By.name("Cena za OPM");
    public static By slevyARetence = By.name("Slevy a retence");

    // Poplatek za OPM
    public static By btnIcon_AddFee = By.name("btnIcon_AddFee");
    public static By EAN = By.name("new");
    public static By calendarSincenew = By.name("calendarSincenew");
    public static By calendarUntilnew = By.name("calendarUntilnew");
    public static By high_tariff_price_new = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[4]/table[1]/tbody[1]/tr[1]/td[4]/input[1]");
    public static By low_tariff_price_new = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[4]/table[1]/tbody[1]/tr[1]/td[5]/input[1]");
    public static By confirmed_price_new = By.name("elem.id");
    public static By btnSave_new = By.name("btnSave_new");
    public static By btnStorno_new = By.name("btnStorno_new");

    public static By tableBy = By.xpath("//table[@class='pure-table']//tbody");

    @Test
    public void contractSettingCenaOPMAdd() throws Exception {
        WebActions.actual("CONRACT SETTINGS", "page");
        actions.search(WebActions.search_opm_NN, WebActions.search_opm_NN_smlouva);
        actions.clickOnElement(nastaveniTop);
        actions.clickOnElement(cenaZaOpm);

        WebElement table = driver.findElement(tableBy);
        LinkedList<String> listIdsRow_old = actions.tableGetAllIdRow(table, "select");
        int pocet_old = listIdsRow_old.size();

        SimpleDateFormat format_from = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format_to = new SimpleDateFormat("ddMMyyyy");
        String valid_from;
        String valid_to;

        if (listIdsRow_old.size() == 0) {
            valid_from = actions.dateUser(0);
            valid_to = actions.dateUser(2);
        } else {
            // poplatky jiz existuji, proto nove vkladany musi zacinat dalsi den
            String id_row_latest =  actions.getIdRowLatestDate(table, "calendarUntil", "select");
            System.err.println(id_row_latest);
            Calendar date_row = Calendar.getInstance();
            date_row.setTime(format_from.parse(table.findElement(By.name("calendarUntil"+id_row_latest)).getAttribute("value")));
            date_row.add(Calendar.DATE, 1);
            valid_from = format_to.format(date_row.getTime());
            date_row.add(Calendar.DATE, 5);
            valid_to = format_to.format(date_row.getTime());
        }

        WebActions.actual("Přidání poplatku za OPM", "section");
        actions.clickOnElement(btnIcon_AddFee);
        actions.selectFirst(EAN);
        actions.setCalendar(calendarSincenew, valid_from);
        actions.setCalendar(calendarUntilnew, valid_to);
        actions.writeText(high_tariff_price_new, "40");
        actions.writeText(low_tariff_price_new, "15");
        actions.selectSetByValue(confirmed_price_new, "false");
        actions.clickOnElement(btnSave_new);
        actions.waitHideLoader();
        //actions.waitInvisibility(btnSave_new);

        actions.refresh();
        actions.timeDelay(2000);

        // po refresh čekám na zobrazní prvku z původní tabulky
        actions.clickOnElement(cenaZaOpm);
        actions.waitVisibility(By.name("calendarUntil"+listIdsRow_old.get(0)));

        table = driver.findElement(tableBy);
        LinkedList<String> listIdsRow_new = actions.tableGetAllIdRow(table, "select");
        int pocet_new = listIdsRow_new.size();
        if ((pocet_old + 1) == pocet_new) {
            System.out.println("Přidání 'Cena za OPM' proběhla v pořádku. Původně :" + pocet_old + ", nově :" + pocet_new);
        } else {
            System.err.println("Při přidání 'Cena za OPM' došlo k chybě. Původně :" + pocet_old + ", nově :" + pocet_new);
            throw new Error ("Při přidání 'Cena za OPM' došlo k chybě. Původně :" + pocet_old + ", nově :" + pocet_new);
        }

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}