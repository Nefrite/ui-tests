package cz.nanos.uitests.customer.Contract.Setting.SlevyARetence;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;



@RunWith(SpringRunner.class)
@SpringBootTest
public class RetenceAdd extends AuthorizedScenario {
    public String scenarioDescription = "Scénář na přidání nové retence v nastavení smlouvy";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    //public static By buttonSave = By.xpath("//div[@class='new-opm-forms']//div//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By nastaveni = By.xpath("//a[@name='Nastavení']");
    public static By cenySilElektriny = By.name("Ceny sil. elektřiny");
    public static By cenaZaOpm = By.name("Cena za OPM");
    public static By slevyARetence = By.name("Slevy a retence");

    // Slevy a retence
    public static By addRetence = By.name("btnIcon_AddRetention");
    public static By addSlevy = By.name("btnIcon_AddDis");

    public static By sleva_new = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/input[1]");
    public static By calendarSincenew = By.name("calendarSincenew");
    public static By calendarUntilnew = By.name("calendarUntilnew");
    public static By stav_new = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[4]/table[1]/tbody[1]/tr[1]/td[4]/select[1]");

    public static By delpt_ean_new = By.name("delpt_ean_new");
    public static By type_new = By.name("type_new");
    public static By name_new = By.name("name_new");
    public static By percent_amount_new = By.name("percent_amount_new");
    public static By use_once = By.name("use_once");
    public static By calendarSinceValiditynew = By.name("calendarSinceValiditynew");
    public static By calendarUntil_new = By.name("calendarUntil_new");
    public static By btnSave_new = By.name("btnSave_new");
    public static By btnStorno_new = By.name("btnStorno_new");
    public static By btnDelete_new = By.xpath("//*[contains(@title, 'Odstranit')");



    public static By table_retence = By.xpath("//div[@class='grid-page']//div[4]//table[1]//tbody[1]");
    public static By table_slevy = By.xpath("//div[7]//table[1]//tbody[1]");

    @Test
    public void slevyAdd() throws Exception {
        actions.search(WebActions.search_opm_NN, WebActions.search_opm_NN_smlouva);
        actions.clickOnElement(nastaveni);
        actions.clickOnElement(slevyARetence);
        actions.waitHideLoader();

        WebElement table = driver.findElement(table_retence);
        LinkedList<String> listIdsRow_old = actions.tableGetAllIdRow(table, "input");
        SimpleDateFormat format_from = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format_to = new SimpleDateFormat("ddMMyyyy");
        String valid_from;
        String valid_to;

        if (listIdsRow_old.size() == 0) {
            valid_from = actions.dateUser(0);
            valid_to = actions.dateUser(2);
        } else {
            // poplatky jiz existuji, proto nově vkládaný musi začínat dalsi den
            String id_row_latest =  actions.getIdRowLatestDate(table, "calendarUntil", "input");
            Calendar date_row = Calendar.getInstance();
            date_row.setTime(format_from.parse(table.findElement(By.name("calendarUntil"+id_row_latest)).getAttribute("value")));
            date_row.add(Calendar.DATE, 1);
            valid_from = format_to.format(date_row.getTime());
            date_row.add(Calendar.DATE, 3);
            valid_to = format_to.format(date_row.getTime());
        }
        actions.clickOnElement(addRetence);

        String retence = "" + actions.uniqueX(3);
        actions.writeText(sleva_new, retence);
        actions.setCalendar(calendarSincenew, valid_from);
        actions.setCalendar(calendarUntilnew, valid_to);
        actions.selectSetByText(stav_new, "Potvrzeno");
        actions.clickOnElement(btnSave_new);
        actions.waitHideLoader();

        valid_from =valid_from.substring(4,8) + "-" + valid_from.substring(2,4) + "-" + valid_from.substring(0,2);
        valid_to =valid_to.substring(4,8) + "-" + valid_to.substring(2,4) + "-" + valid_to.substring(0,2);
        LinkedList<String> listIdsRow_new = actions.tableGetAllIdRow(table, "input");
        Integer id_row = actions.getIdAddedRow(listIdsRow_new, listIdsRow_old);

        actions.actual("Kontrola nově uložených dat v tabulce", "section");
        actions.tableCellControlValue(table.findElement(By.name(id_row+"")).getAttribute("value"), retence, "Slevy");
        actions.tableCellControlValue(table.findElement(By.name("calendarSince" + id_row)).getAttribute("value"), valid_from, "Platnost od");
        actions.tableCellControlValue(table.findElement(By.name("calendarUntil" + id_row)).getAttribute("value"), valid_to, "Platnost do");
        System.out.println("Všechny hodnoty v přidaném řádku jsou v pořádku.");
        actions.actual("Konec kontroly uložených dat v tabulce", "section-close");

        if ((listIdsRow_old.size() + 1) == listIdsRow_new.size()) {
            System.out.println("Nově přidaná sleva s id: '" + id_row + "' je v pořádku");
        } else {
            System.err.println("Při přidání slevy došlo k chybě.");
            throw new Error ("Při přidání slevy došlo k chybě.");
        }

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
     }
}