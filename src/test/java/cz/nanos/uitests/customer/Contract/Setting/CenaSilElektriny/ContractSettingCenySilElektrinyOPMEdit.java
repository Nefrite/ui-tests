package cz.nanos.uitests.customer.Contract.Setting.CenaSilElektriny;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractSettingCenySilElektrinyOPMEdit extends AuthorizedScenario {
    public String scenarioDescription = "Cena silové energie - editace existujícího záznamu, pokud neexistuje vytvoří";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    //public static By buttonSave = By.xpath("//div[@class='new-opm-forms']//div//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static By buttonSave = By.name("buttonSave");
    public static By buttonCancelChange = By.name("buttonCancelChange");

    public static By nastaveniTop = By.xpath("//a[contains(@href, '" + WebActions.search_opm_NN_smlouva + "')]");

    public static By nastaveni = By.xpath("//a[@name='Nastavení']");
    public static By cenySilElektriny = By.name("Ceny sil. elektřiny");
    public static By cenaZaOpm = By.name("Cena za OPM");
    public static By slevyARetence = By.name("Slevy a retence");

    // Nastaveni
    public static By btnIcon_EditProduct = By.name("btnIcon_EditProduct");
    public static By consumption_tariff_id = By.name("consumption_tariff_id");
    public static By buttonCreateRequest = By.name("buttonCreateRequest");
    public static By calendarDateChange = By.name("calendarDateChange");

    /*
    public static By contact_person_id = By.name("contact_person_id");
    public static By client_code = By.name("client_code");
    public static By memo = By.name("memo");
    public static By summary_advance = By.name("summary_advance");
    public static By summary_bill = By.name("summary_bill");
    public static By consumption_only = By.name("consumption_only");
    public static By use_default_contact_setting = By.name("use_default_contact_setting");
    */

    // cena silové elektriny
    public static By btnIcon_AddFee = By.name("btnIcon_AddFee");
    public static By btnIcon_AddPrice = By.name("btnIcon_AddPrice");

    // Poplatek za OPM
    public static By calendarSincenew = By.name("calendarSince_fee_new");
    public static By calendarUntilnew = By.name("calendarUntil_new");
    public static By trade_fee_new = By.name("trade_fee_new");
    public static By confirmed_fee_new = By.name("confirmed_fee_new");
    public static By btnSave_new = By.name("btnSave_new");
    public static By btnIcon_Storno = By.name("btnIcon_Storno");
    public static By btnIcon_Remove = By.name("btn_del_fee_new");
    //public static By editovat = By.xpath("/button[@title='Editovat']");

    // Dle distribučních sazeb
    public static By distribution_tariff_new = By.name("distribution_tariff_new");
    public static By calendarSince_price_new = By.name("calendarSince_price_new");
    public static By calendarUntil_price_new = By.name("calendarUntil_price_new");
    public static By high_tariff_price_new = By.name("high_tariff_price_new");
    public static By low_tariff_price_new = By.name("low_tariff_price_new");
    public static By confirmed_price_new = By.name("confirmed_price_new");
    //public static By btnIcon_Save = By.name("btnSave_new");
    //public static By btnIcon_Storno = By.name("btnIcon_Storno");
    //public static By btnIcon_Remove = By.name("btn_del_price_new");

    public static By table1By = By.xpath("//body//div[contains(@class,'tabs-content')]//div//div[2]//div[3]//table[1]//tbody[1]");
    public static By table2By = By.xpath("//body//div[3]//div[3]//table[1]//tbody[1]");

    @Test
    public void contractSettingCenySilElektrinyOPMAdd() throws Exception {
        WebActions.actual("CONRACT SETTINGS", "page");
        actions.search(WebActions.search_opm_NN, WebActions.search_opm_NN_smlouva);
        actions.clickOnElement(nastaveniTop);
        actions.clickOnElement(cenySilElektriny);

        WebElement table = driver.findElement(table1By);
        LinkedList<String> listIdsRow_old = actions.tableGetAllIdRow(table, "input");
        SimpleDateFormat format_from = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format_to = new SimpleDateFormat("ddMMyyyy");
        String valid_from = "";
        String valid_to = "";

        String id_row;
        if (listIdsRow_old.size() == 0) {
            System.out.println("V tabulce není žádný poplatek za OPM, bude přidán nový.");
            actions.clickOnElement(btnIcon_AddFee);
            WebActions.actual("Přidání poplatku za OPM", "section");
            actions.setCalendar(calendarSincenew, actions.dateUser(0));
            actions.setCalendar(calendarUntilnew, actions.dateUser(1));
            actions.writeText(trade_fee_new, "50");
            actions.selectSetByValue(confirmed_fee_new, "false");
            actions.clickOnElement(btnSave_new);
            actions.waitInvisibility(btnSave_new);

            listIdsRow_old = actions.tableGetAllIdRow(table, "input");
            id_row = listIdsRow_old.getLast();

            actions.actual("Kontrola nově uložených dat v tabulce", "section");
            actions.tableCellControlValue(table.findElement(By.name("calendarSince_fee_"+id_row)).getAttribute("value"), actions.dateUserRightFormat(0), "Platnost od");
            actions.tableCellControlValue(table.findElement(By.name("calendarUntil_"+id_row)).getAttribute("value"), actions.dateUserRightFormat(1), "Platnost do");
            actions.tableCellControlValue(table.findElement(By.name("trade_fee_"+id_row)).getAttribute("value"), "50", "Cena");
            actions.tableCellControlValue(table.findElement(By.name("confirmed_fee_"+id_row)).getAttribute("value"), "false", "Stav");
            System.out.println("Všechny hodnoty v přidaném řádku jsou v pořádku.");
            actions.actual("Konec kontroly uložených dat v tabulce", "section-close");
        } else {
            // poplatky jiz existuji, proto editovaný vkladany musi zacinat dalsi den
            id_row =  actions.getIdRowLatestDate(table, "calendarUntil_", "input");
        }

        Calendar date_row = Calendar.getInstance();
        date_row.setTime(format_from.parse(table.findElement(By.name("calendarUntil_"+id_row)).getAttribute("value")));
        date_row.add(Calendar.DATE, 1);
        valid_from = format_to.format(date_row.getTime());
        date_row.add(Calendar.DATE, 1);
        valid_to = format_to.format(date_row.getTime());

        WebActions.actual("Editace nejnovějšího poplatku za OPM", "section");
        actions.clickOnElement(By.name("btnEdit_" + id_row));
        actions.setCalendar(By.name("calendarSince_fee_" + id_row), valid_from);
        actions.setCalendar(By.name("calendarUntil_" + id_row), valid_to);
        int aaa = Integer.parseInt(driver.findElement(By.name("trade_fee_" + id_row)).getAttribute("value"));
        aaa++;
        actions.writeText(By.name("trade_fee_" + id_row), aaa+"");
        actions.selectSetByValue(By.name("confirmed_fee_" + id_row), "false");
        actions.clickOnElement(By.name("btnSave_" + id_row));
        actions.waitVisibility(By.name("btnEdit_" + id_row));

        valid_from =valid_from.substring(4,8) + "-" + valid_from.substring(2,4) + "-" + valid_from.substring(0,2);
        valid_to =valid_to.substring(4,8) + "-" + valid_to.substring(2,4) + "-" + valid_to.substring(0,2);

        actions.actual("Kontrola nově uložených dat v tabulce", "section");
        actions.tableCellControlValue(table.findElement(By.name("calendarSince_fee_" + id_row)).getAttribute("value"), valid_from, "Platnost od");
        actions.tableCellControlValue(table.findElement(By.name("calendarUntil_" + id_row)).getAttribute("value"), valid_to, "Platnost do");
        actions.tableCellControlValue(table.findElement(By.name("trade_fee_" + id_row)).getAttribute("value"), aaa+"", "Cena");
        actions.tableCellControlValue(table.findElement(By.name("confirmed_fee_" + id_row)).getAttribute("value"), "false", "Stav");
        System.out.println("Všechny hodnoty v přidaném řádku jsou v pořádku.");
        actions.actual("Konec kontroly uložených dat v tabulce", "section-close");
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}