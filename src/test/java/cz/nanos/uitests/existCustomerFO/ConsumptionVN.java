package cz.nanos.uitests.existCustomerFO;

import cz.nanos.uitests.pages.HomePage;
import cz.nanos.uitests.pages.NewContract.*;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsumptionVN extends AuthorizedScenario {
    public String scenarioDescription = "Nová smlouva pro nového zákazníka. Odběr NN.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }

    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    public static String contract_type = "";
    public static String voltage = "";

    @Test
    public void ConsumptionNN(){
        actions.clickOnElement(HomePage.new_3_consumer);

        WebActions.actual("NEW CONTRACT", "page");
        WebActions.actual("KROK 1 - ZÁKAZNÍK", "step");

        actions.radioSetId(NewContractStepOne.customerType, "typeFO");
        WebActions.actual("Sekce Osobní údaje", "section");
        String jmeno = "Jan";
        String prijmeni = "Sekanina";
        String datum_narozeni = "05021979";
        actions.writeText(NewContractStepOne.personFirstname, jmeno);
        actions.writeText(NewContractStepOne.personLastname, prijmeni);
        actions.setCalendar(NewContractStepOne.calendarBirthday, datum_narozeni);
        actions.clickOnElement(NewContractStepOne.buttonSearchCustomerFOSP);
        actions.waitHideLoader();
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.waitHideLoader();

        System.out.println("Existující uživatel nalezen.");

        // kontrola zda je již zadán nějaký obecný kontakt
        actions.waitDropdownLoadValue(NewContractStepOne.primary_contact_person_id);
        WebElement table1 = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/form[1]/div[2]/div[3]/div[1]/div[1]/div[2]/table[1]/tbody[1]"));
        if (table1.findElements(By.tagName("tr")).size() == 0) {
            WebActions.actual("Sekce Obecné kontakty", "section");
            actions.clickOnElement(NewContractStepOne.addBasicContact);
            actions.ModalGeneralFillForm(driver);
            // kontrola zadaných hodnot
            actions.tableGetRowValuesGeneral(driver.findElement(By.xpath("//div[contains(@class,'block block-newContract--general-contacts')]//tbody")), "Nákupčí energie");
        } else {
            //System.err.println("Zákazník má záznamy v tabulce obecné kontakty");
        }

        System.out.println("Akce nastavení bez změny, nechány původní hodnoty.");

        WebElement table2 = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/form[1]/div[2]/div[4]/div[1]/div[1]/div[3]/table[1]/tbody[1]"));
        if (table2.findElements(By.tagName("tr")).size() == 0) {
            WebActions.actual("Sekce Použití kontaktů", "section");
            actions.clickOnElement(NewContractStepOne.addContact);
            actions.selectFirst(NewContractStepOne.contact_select);
        actions.checkboxChecked(NewContractStepOne.contact_general);
        actions.checkboxChecked(NewContractStepOne.contact_advances);
        actions.checkboxChecked(NewContractStepOne.contact_bill);
        actions.checkboxChecked(NewContractStepOne.contact_invoice);
        actions.checkboxChecked(NewContractStepOne.contact_marketing);
        actions.checkboxChecked(NewContractStepOne.contact_demand);
        actions.checkboxChecked(NewContractStepOne.contact_production);
            actions.clickOnElement(NewContractStepOne.contact_save);
            actions.waitHideLoader();
            actions.tableGetRowValuesGeneral(driver.findElement(By.xpath("//div[contains(@class,'block block-newContract--general-contacts')]//tbody")), "Nákupčí energie");
        } else {
            //System.err.println("Zákazník má záznamy v tabulce použití kontaktů");
        }

        WebElement table3 = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/form[1]/div[2]/div[5]/div[1]/div[1]/div[1]/div[3]/table[1]/tbody[1]"));
        if (table3.findElements(By.tagName("tr")).size() == 0) {
            // přidání platebních kontaktů
            String payment_type = "bank";
            String unique = actions.uniqueX(10);
            String nazev = "Platba BÚ";
            String kod_banky = "2010";
            actions.clickOnElement(NewContractStepOne.addPaymentContact);
            actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);

            payment_type = "sipo";
            unique = actions.uniqueX(10);
            nazev = "Platba SIPO";
            kod_banky = "2010";
            actions.clickOnElement(NewContractStepOne.addPaymentContact);
            actions.ModalPaymentContactAdd(driver, payment_type, unique, nazev, kod_banky);
            actions.clickOnElement(By.name("buttonNextStep"));
        } else {
            //System.err.println("Zákazník má záznamy v tabulce platební kontakty");
        }
        actions.clickOnElement(By.name("buttonNextStep"));

        // 2. krok
        WebActions.actual("KROK 2 - TYP SMLOUVY", "step");
        actions.selectSetByText(NewContractStepTwo.contract_type, "Odběr");
        actions.selectSetByText(NewContractStepTwo.voltage, "VN");
        actions.clickOnElement(By.name("buttonNextStep"));

        // 3.krok
        WebActions.actual("KROK 3 - NASTAVENI SMLOUVY", "step");
        NewContractStepThree.controlAllElement();
        // zalozka nastaveni
        WebActions.actual("Zalozka Nastaveni", "section");
        actions.clickOnElement(NewContractStepThree.nastaveni);
        actions.waitDropdownLoadValue(NewContractStepThree.contact_person_id);
        actions.selectFirst(NewContractStepThree.contact_person_id);
        actions.writeText(NewContractStepThree.client_code, "značka zákazníka");
        actions.checkboxChecked(NewContractStepThree.consumption_only);
        actions.checkboxChecked(NewContractStepThree.use_default_contact_setting);
        actions.writeText(NewContractStepThree.memo, "Poznámka v nastavení smlouvy");

        /*
        actions.clickOnElement(NewContractStepThree.btnIcon_AddPrice);
        WebActions.timeDelay(500);
        actions.selectSetByValue(NewContractStepThree.distribution_tariff_new, "D01d");
        actions.setCalendar(NewContractStepThree.calendarSince_price_new, actions.dateUser(0));
        actions.setCalendar(NewContractStepThree.calendarUntil_price_new, actions.dateUser(100));
        actions.writeText(NewContractStepThree.high_tariff_price_new, "20");
        actions.writeText(NewContractStepThree.low_tariff_price_new, "10");
        actions.selectSetByValue(NewContractStepThree.confirmed_price_new, "false");
        */
        actions.clickOnElement(By.name("buttonNextStep"));

        // 4.krok
        actions.actual("KROK 4 - OPM", "step");

        // získání EAN z API
        String EAN = WebActions.getEAN("");
        actions.writeText(NewContractStepFour.addSearchEan, EAN);
        //actions.clickOnElement(NewContractStepFour.buttonSearch);
        actions.clickAndControlToast(NewContractStepFour.buttonSearch, "EAN nenalezen", "Zadané EAN je již použité, " +
                "proto nelze pokračovat dále v zakládání OPM.");
        actions.clickOnElement(NewContractStepFour.buttonCancelChange);

        actions.actual("Obecné", "section");
        actions.writeDistNetwork(NewContractStepFour.dist_network, "0011");
        actions.clickOnElement(By.xpath("//button[@title='Editovat adresu']"));
        String town = "Břeclav";
        String psc = "69002";
        String mistni_cast = "";
        String street = "Fintajsova";
        String number_cp = "1347";
        String number_op = "";
        String address = "";
        actions.ModalAddressFillAddress(driver, "HOUSE", town, psc, mistni_cast, street, number_cp, number_op);
        actions.radioSet(NewContractStepFour.measurement_type, "A");
        actions.writeText(NewContractStepFour.memo, "Poznámka krok 4");
        actions.writeText(NewContractStepFour.custom_name, "Značka odběr");

        actions.actual("Odběr", "section");
        actions.writeText(NewContractStepFour.reserved_power, "20");
        actions.writeText(NewContractStepFour.reserved_capacity_year, "20");
        actions.writeText(NewContractStepFour.control_stage_3, "10");
        actions.writeText(NewContractStepFour.control_stage_4, "10");
        actions.writeText(NewContractStepFour.control_stage_5, "10");
        actions.writeText(NewContractStepFour.control_stage_6, "10");
        actions.writeText(NewContractStepFour.control_stage_7_kwh, "15");
        actions.writeText(NewContractStepFour.control_stage_7_hr, "15");

        /*
        actions.clickOnElement(NewContractStepFour.btnIcon_AddMonth);
        actions.writeText(NewContractStepFour.month_1, "10");
        actions.writeText(NewContractStepFour.month_2, "10");
        actions.writeText(NewContractStepFour.month_3, "10");
        actions.writeText(NewContractStepFour.month_4, "10");
        actions.writeText(NewContractStepFour.month_5, "10");
        actions.writeText(NewContractStepFour.month_6, "10");
        actions.writeText(NewContractStepFour.month_7, "10");
        actions.writeText(NewContractStepFour.month_8, "10");
        actions.writeText(NewContractStepFour.month_9, "10");
        actions.writeText(NewContractStepFour.month_10, "10");
        actions.writeText(NewContractStepFour.month_11, "10");
        actions.writeText(NewContractStepFour.month_12, "10");
        actions.clickOnElement(NewContractStepFour.button_save_capacity);
        */

        actions.clickOnElement(NewContractStepFour.buttonSave);

        actions.clickOnElement(By.name("buttonNextStep"));

        actions.actual("KROK 5 - Nastavení OPM", "step");

        // Výchozí nastavení zálohy
        actions.selectSetByText(NewContractStepFive.oz_advance_frequency, "Měsíční");
        actions.selectSetByText(NewContractStepFive.oz_advance_payment_method, "Příkaz");
        actions.clickOnElement(NewContractStepFive.oz_button_add_contact);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));

        // Výchozí nastavení Vyúčtování
        actions.selectSetByText(NewContractStepFive.ov_bill_frequency, "Pololetní");
        actions.writeText(NewContractStepFive.ov_bill_payable_period, "20");
        actions.selectSetByText(NewContractStepFive.ov_overpayment_repay_strgy_code, "Úhrada/vratka");
        actions.checkboxUnChecked(NewContractStepFive.ov_exclude_automatic_bill);
        actions.selectSetByText(NewContractStepFive.ov_overpayment_payment_method, "Úhrada/vratka příkazem");
        actions.clickOnElement(NewContractStepFive.ov_button_add_preplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.selectSetByText(NewContractStepFive.ov_debt_payment_method, "Příkaz");
        actions.clickOnElement(NewContractStepFive.ov_button_add_contact_preplatek);
        actions.clickOnElement(By.xpath("//button[@title='Zvolit']"));
        actions.clickAndControlToast(NewContractStepFive.o_buttonSave, "Data uložena", "Uložení 'Výchozích dat' OPM neproběhlo v pořádku");

        // pokračování stránky - pro konkrétní OPM
        actions.clickOnElement(NewContractStepFive.button_5_1);

        // OPM dodávka
        actions.setCalendar(NewContractStepFive.nd_delivery_start_at, actions.dateUser(0));
        actions.setCalendar(NewContractStepFive.nd_delivery_end_at, actions.dateUser(1));
        actions.selectSetByText(NewContractStepFive.nd_contract_condition_code, "Změna dodavatele");

        // OPM zálohy
        actions.writeText(NewContractStepFive.nz_advance_amount, "500");
        actions.checkboxChecked(NewContractStepFive.nz_use_default_advance);

        // OPM vyúčtování

        // OPM výpověď
        actions.checkboxUnChecked(NewContractStepFive.v_terminate_previous);
        actions.checkboxChecked(NewContractStepFive.nv_use_default_billing);

        /*
        WebActions.actual("Sekce Výpověď", "section");
        actions.checkboxChecked(NewContractStepFive.v_terminate_previous);
        actions.selectSetByValue(NewContractStepFive.v_previous_trader_id, "9");
        actions.selectSetByValue(NewContractStepFive.v_termination_period_unit, "DAY");
        actions.timeDelay(500);
        actions.writeText(NewContractStepFive.v_termination_period, "50");
        actions.clickOnElement(NewContractStepFive.n_buttonSave);
         */
        actions.clickAndControlToast(NewContractStepFive.n_buttonSave, "Data o OPM uložena", "Uložení dat 'konkrétního OPM' neproběhlo v pořádku");

        actions.actual("Smluvní ceník", "section");
        //timeDelay(2000);
        actions.clickOnElement(NewContractStepFive.smluvni_cenik);
        actions.setCalendar(NewContractStepFive.calendarSince, actions.dateUser(2));
        actions.setCalendar(NewContractStepFive.calendarUntil, actions.dateUser(20));
        actions.selectSetByText(NewContractStepFive.confirmed, "Nepotvrzeno");
        actions.radioSet(NewContractStepFive.billing_type, "fixed_price");
        actions.writeText(NewContractStepFive.high_tariff_price, "33");
        actions.writeText(NewContractStepFive.low_tariff_price, "22");
        actions.writeText(NewContractStepFive.trade_fee, "99");

        actions.actual("Účtování poplatků", "section");
        actions.checkboxChecked(NewContractStepFive.charge_oze);
        actions.checkboxChecked(NewContractStepFive.charge_system_services);
        actions.checkboxChecked(NewContractStepFive.charge_reserved_capacity_exceed);
        actions.checkboxChecked(NewContractStepFive.charge_power_factor_exceed);
        actions.checkboxChecked(NewContractStepFive.charge_ote);
        actions.checkboxChecked(NewContractStepFive.charge_reactive_energy);
        actions.checkboxChecked(NewContractStepFive.charge_reserved_power_exceed);
        actions.checkboxChecked(NewContractStepFive.HWork1);
        actions.checkboxChecked(NewContractStepFive.HWeek1);

        actions.clickOnElement(NewContractStepFive.smluvni_cenik_save);
        actions.clickAndControlToast(NewContractStepFive.smluvni_cenik_save, "Aktualizoval se ceník", "Chyba při uložení ceníku");

        actions.clickOnElement(By.name("buttonNextStep"));
        actions.clickOnElement(By.xpath("//button[contains(text(),'Ano')]"));

        // 6.krok
        actions.actual("KROK 6 - Shrnutí", "step");
        System.out.println("Spotřeba NN\n");

        WebElement summary = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]"));
        System.out.println("Shrnutí:");
        System.out.println("Zákazník: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/a[1]")).getText());
        System.out.println("Číslo smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[2]")).getText());
        System.out.println("Typ smlouvy: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[3]")).getText());
        System.out.println("OPM: " + driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[4]")).getText());

        WebElement document = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[4]/div[3]/table[1]/tbody[1]"));
        System.out.println("\nDokumenty:");
        List<WebElement> rows = document.findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            System.out.println("Název dokumentu : '" + cells.get(1).getText() + "' Popis: '" + cells.get(2).getText()+"'");
        }

        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}
