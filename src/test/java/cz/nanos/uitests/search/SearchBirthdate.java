package cz.nanos.uitests.search;

import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchBirthdate extends AuthorizedScenario {
    public String scenarioDescription = "Vyhledání zákazníka podle data narození.";
    @Override
    public String getScenarioDescription() {
        return scenarioDescription;
    }
    @Before
    public void init() throws Exception{
        userLogin(loginUser, loginPassword);
    }

    @After
    public void close(){
    }

    @Test
    public void searchBirthdate(){
        actions.search("13.6.1974", "/customer/3779/setting");
        System.out.println("Scénář byl úspěšně dokončen.");
        WebActions.actual("KONEC SCENARE", "page");
        userLogout();
    }
}