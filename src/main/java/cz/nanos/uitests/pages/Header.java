package cz.nanos.uitests.pages;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;

import java.util.Arrays;
import java.util.LinkedList;

public class Header {
    public static final By phrase = By.name("phrase");
    public static final By logout = By.name("logout");
    public static final By show_results = By.xpath("//a[@class='show-results']");

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     * @return
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(phrase, logout));
        return list;
    }

    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement() {
        // kontrola statických elementů
        WebActions.elementsExistance(Header.getStaticElements());
    }

    public static void logout() {

    }
}
