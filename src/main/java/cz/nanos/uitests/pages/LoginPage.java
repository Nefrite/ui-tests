package cz.nanos.uitests.pages;

import java.util.Arrays;
import java.util.LinkedList;

import cz.nanos.uitests.pages.HomePage;
import cz.nanos.uitests.pages.LoginPage;
import cz.nanos.uitests.service.AuthorizedScenario;
import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;

/**
 * Definice stranky a jejich elementu, prip. podminky, atd
 */
public class LoginPage {

	public static final By username = By.name("username");
	public static final By password = By.name("password");
	public static final By buttonLogin = By.name("buttonLogin");
	//public static final By chyba = By.name("chyba"); // test pro neexistujici prvek

	/**
	 * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
	 */
	public static LinkedList<By> getStaticElements() {
		LinkedList<By> list = new LinkedList<By>();
		list.addAll(Arrays.asList(username, password, buttonLogin));
		return list;
	}

	/**
	 * Kontrola všech elementů
	 */
	public static void controlAllElement() {
		// kontrola statických elementů
		WebActions.actual("LOGIN PAGE - Kontrola statických elementů", "control");
		WebActions.elementsExistance(LoginPage.getStaticElements());
	}
}
