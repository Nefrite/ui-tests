package cz.nanos.uitests.pages.NewContract;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;

public class NewContractStepOne {

    public static final String url = "/new/1";

    /**
     * Firemní/osobní údaje
     * Rozdílné FO/SP a FOP/PO
     */
    public static By customerType = By.name("customerType");

    // FO/SP
    public static By personTitles = By.name("person.titles");
    public static By personFirstname = By.name("person.firstname");
    public static By personLastname = By.name("person.lastname");
    public static By personDegrees = By.name("person.degrees");
    public static By calendarBirthday = By.name("calendarBirthday");
    public static By personTaxCode = By.name("person.tax_code");
    public static By buttonSearchCustomerFOSP = By.name("buttonSearchCustomerFOSP");

    // FOP/PO
    public static By personRegCode = By.name("person.reg_code");
    public static By personName = By.name("person.name");
    public static By personBusinessFormCode = By.name("person.business_form_code");
    public static By buttonSearchCustomerFOPPO = By.name("buttonSearchCustomerFOPPO");
    public static By registrationFileNumber = By.name("person.registration_file_number");
    public static By registrationCourtId = By.name("person.registration_court_id");
    //public static By personTaxCode = By.name("person.tax_code");

    public static By success_info = By.className("className");  // zobrazené potvrzení, že uživatel byl zobrazen

    /**
     * Adrresy
     */
    // FO/SP
    public static By addAddress = By.name("btn_add_domicile");
    // FOP/PO
    public static By addAdressHq = By.name("btn_add_hq");

    // společné
    public static By addAddressNext = By.name("btn_add_new_type");
    // přidání korespondenční/jiné adresy
    public static By addNewAddress = By.name("addNewAddress");
    public static By btn_add_concrete_addr = By.name("btn_add_concrete_addr");
    public static By btn_save_concrete_addr = By.name("btn_save_concrete_addr");
    public static By btnStorno = By.name("btnStorno");



    /**
     * Obecné kontakty
     */
    public static By addBasicContact = By.name("btn_addBasicContact");

    /**
     * Nastavení
     * Rozdílné FO/SP a FOP/PO
     */
    // FO/SP
    public static By sales_representative_id = By.name("sales_representative_id");
    public static By primary_contact_person_id = By.name("primary_contact_person_id");
    public static By person_memo = By.name("person.memo");
    public static By person_vip = By.name("person.vip");
    public static By autosend_demands = By.name("autosend_demands");
    public static By autosend_advance_invoice = By.name("autosend_advance_invoice");
    public static By autosend_advance_reminders = By.name("autosend_advance_reminders");
    public static By autosend_payment_receipts = By.name("autosend_payment_receipts");

    // po vyhledání existujícího zákazníka
    public static By buttonCreateNewCustomerFOSP = By.name("buttonCreateNewCustomerFOSP");
    public static By buttonCreateNewCustomerFOPPO = By.name("buttonCreateNewCustomerFOPPO");

    // FOP/PO
    public static By responsible_contact_person_id = By.name("responsible_contact_person_id");
    //public static By sales_representative_id = By.name("sales_representative_id");
    public static By intermediary = By.name("intermediary");
    public static By memo = By.name("memo");
    public static By VIPcustomer = By.name("VIPcustomer");
    //public static By autosend_demands = By.name("autosend_demands");
    //public static By autosend_advance_invoice = By.name("autosend_advance_invoice");
    public static By reverse_charge = By.name("reverse_charge");
    //public static By autosend_advance_reminders = By.name("autosend_advance_reminders");
    //public static By autosend_payment_receipts = By.name("autosend_payment_receipts");


    /**
     * Použití kontaktů
     */
    public static By addContact = By.name("btn_add_contactUsage");
    public static By contact_select = By.name("value");
    public static By contact_general = By.name("GENERAL_new");
    public static By contact_advances = By.name("ADVANCES_new");
    public static By contact_bill = By.name("BILL_new");
    public static By contact_invoice = By.name("INVOICE_new");
    public static By contact_marketing = By.name("MARKETING_new");
    public static By contact_demand = By.name("DEMAND_new");
    public static By contact_production = By.name("PRODUCTION_new");
    public static By contact_save = By.name("btnSave_new");
    public static By contact_storno = By.name("btnIcon_Storno");
    public static By contact_delete = By.name("btnIcon");

    //public static By addContact = new By.ByLinkText("Přidat nový");
    /**
     * Platební kontakt
     */
    public static By addPaymentContact = By.name("btnIcon_Add");

    
    public static List<By> getFixedElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.clear();

        System.out.println("Mapping newConstactStepOne");
        list.add(customerType);

        /**
         * Adrresy
         */
        list.addAll(Arrays.asList(addAddress, addAddressNext, addAddressNext));


        /**
         * Obecné kontakty
         */
        list.addAll(Arrays.asList(addBasicContact));

        /**
         * Firemni / osobni udaje
         */
        // viz FO vs FOP
        
        /**
         * Nastavení
         */
        //viz FO vs FOP

        /**
         * Použití kontaktů
         */
        list.addAll(Arrays.asList(addContact, contact_select, contact_general, contact_advances, contact_bill, contact_invoice,
                contact_marketing, contact_demand, contact_production, contact_save, contact_storno, contact_delete));

        /**
         * Platební kontakt
         */
        list.add(addPaymentContact);

        return list;
    }
    
    /**
     * FO/SP
     */
    public static List<By> getElements_FO() {
    	LinkedList<By> list = new LinkedList<By>();
    	//osobni udaje
        list.addAll(Arrays.asList(personTitles, personFirstname, personLastname, personDegrees, calendarBirthday,
                personTaxCode, buttonSearchCustomerFOSP, addAddress, addAddressNext));

        //nastaveni
        list.addAll(Arrays.asList(sales_representative_id, primary_contact_person_id, person_memo, person_vip, autosend_demands,
                autosend_advance_invoice, autosend_advance_reminders, autosend_payment_receipts));
        return list;

    }
    
    /**
     * FOP/PO
     */
    public static List<By> getElements_FOP() {
    	LinkedList<By> list = new LinkedList<By>();
    	//firemni udaje
        list.addAll(Arrays.asList(personRegCode, personName, personTaxCode, personBusinessFormCode, buttonSearchCustomerFOPPO));

        //nastaveni
        list.addAll(Arrays.asList(responsible_contact_person_id, sales_representative_id, intermediary, memo, VIPcustomer,
                autosend_demands, autosend_advance_invoice, reverse_charge, autosend_advance_reminders, autosend_payment_receipts));
        return list;
    }
}
