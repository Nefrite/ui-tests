package cz.nanos.uitests.pages.NewContract;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.LinkedList;

public class NewContractStepFive extends WebActions{
    static WebDriver driver;

    public NewContractStepFive(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static By buttonPreviousStep = By.name("buttonPreviousStep");
    public static By buttonNextStep = By.name("buttonNextStep");

    // Výchozí nastavení
    public static By o_buttonSave = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[4]/button[1]");
    public static By o_buttonCancelChange = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[1]/button[2]");

    // Zálohy
    public static By oz_advance_frequency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/select[1]");
    public static By oz_advance_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/select[1]");
    public static By oz_advance_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/input[1]");
    public static By oz_button_add_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[2]/button[1]");
    public static By oz_button_del_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[2]/button[2]");

    // Vyúčtování
    public static By ov_bill_frequency = By.xpath("//div[@class='step step5 activeStep']//div[3]//div[1]//div[1]//div[1]//div[1]//div[1]//select[1]");
    public static By ov_bill_payable_period = By.xpath("//div[@class='step step5 activeStep']//div[3]//div[1]//div[1]//div[2]//div[1]//div[1]//input[1]");
    public static By ov_overpayment_repay_strgy_code = By.xpath("//div[@class='step step5 activeStep']//div[3]//div[1]//div[1]//div[1]//div[2]//div[1]//select[1]");
    public static By ov_exclude_automatic_bill = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[2]/div[1]/input[1]");
    public static By ov_overpayment_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[3]/div[1]/select[1]");
    public static By ov_overpayment_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[3]/div[1]/input[1]");
    public static By ov_button_add_preplatek = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[3]/button[1]");
    public static By ov_button_del_preplatek = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[3]/button[2]");
    public static By ov_debt_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[4]/div[1]/select[1]");
    public static By ov_debt_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[4]/div[1]/input[1]");
    public static By ov_button_add_contact_preplatek = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[4]/button[1]");
    public static By ov_button_del_contact_preplatek = By.xpath("//div[@class='items-wrapper undefined']//div[4]//button[2]");

    // tlačítko pro pokračování formuláře
    public static By button_5_1 = By.xpath("//span[contains(text(),'5.1')]");

    // druhá část po kliknutí na 5.1.

    public static By n_buttonSave = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/button[1]");
    public static By n_buttonCancelChange = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/button[2]");

    // Dodávka
    public static By nd_delivery_start_at = By.name("delivery_start_at");
    public static By nd_delivery_end_at = By.name("delivery_end_at");
    public static By nd_contract_condition_code = By.name("contract_condition_code");

    // Zálohy
    public static By nz_advance_amount = By.name("advance_amount");
    public static By nz_use_default_advance = By.name("use_default_advance");
    public static By nz_advance_frequency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/select[1]");
    public static By nz_advance_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/select[1]");
    public static By nz_advance_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/input[1]");
    public static By nz_button_add_contact_payment = By.xpath("//div[@class='tab-content']//div[@class='page']//div//button[@title='Přidat kontakt']");
    public static By nz_button_del_contact_payment = By.xpath("//div[@class='tab-content']//div[@class='page']//div//button[@title='Odstranit kontakt']");

    // vyúčtování
    public static By nv_use_default_billing = By.name("use_default_billing");
    public static By nv_bill_frequency = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]");
    public static By nv_bill_payable_period = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[1]/div[1]/input[1]");

    public static By nv_overpayment_repay_strgy_code = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/select[1]");
    public static By nv_exclude_automatic_bill = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[1]/input[1]");
    public static By nv_overpayment_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[1]/select[1]");
    public static By nv_overpayment_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[3]/div[1]/input[1]");
    public static By nv_button_add_contact_preplatek = By.xpath("//div[@class='items-wrapper undefined']//div[2]//div[2]//div[3]//button[1]");
    public static By nv_button_del_contact_preplatek = By.xpath("//div[@class='items-wrapper undefined']//div[2]//div[2]//div[3]//button[2]");
    public static By nv_debt_payment_method = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[1]/div[4]/div[1]/select[1]");
    public static By nv_debt_payment_contact = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/div[2]/div[2]/div[4]/div[1]/input[1]");
    public static By nv_button_add_contact_nedoplatek = By.xpath("//div[@class='contract-opms-wrapper']//div[4]//button[1]");
    public static By nv_button_del_contact_nedoplatek = By.xpath("//div[@class='contract-opms-wrapper']//div[4]//button[2]");

    // výpověď
    public static By v_terminate_previous = By.name("terminate_previous");
    public static By v_previous_trader_id = By.name("previous_trader_id");
    public static By v_termination_period_unit = By.name("termination_period_unit");
    public static By v_termination_period = By.name("termination_period");
    public static By v_termination_date = By.name("termination_date");
    public static By v_contract_document= By.name("contract_document");
    public static By v_button_add_contact_vypoved = By.xpath("//div[@class='formGroupInputLabel']//button[@title='Přidat kontakt']");
    public static By v_button_del_contact_vypoved = By.xpath("//div[@class='formGroupInputLabel']//button[@title='Odstranit kontakt']");

    // záložka Smluvní ceník
    //public static By smluvni_cenik = By.name("Smluvni ceník");
    public static By smluvni_cenik = By.xpath("//a[@name='Smluvní ceník']");
    public static By calendarSince = By.name("calendarSince");
    public static By calendarUntil = By.name("calendarUntil");
    public static By confirmed = By.name("confirmed");
    public static By billing_type = By.name("billing_type");
    public static By high_tariff_price = By.name("high_tariff_price");
    public static By low_tariff_price = By.name("low_tariff_price");
    public static By trade_fee = By.name("trade_fee");
    public static By charge_oze = By.name("charge_oze");
    public static By charge_system_services = By.name("charge_system_services");
    public static By charge_reserved_capacity_exceed = By.name("charge_reserved_capacity_exceed");
    public static By charge_power_factor_exceed = By.name("charge_power_factor_exceed");
    public static By charge_ote = By.name("charge_ote");
    public static By charge_reactive_energy = By.name("charge_reactive_energy");
    public static By charge_reserved_power_exceed = By.name("charge_reserved_power_exceed");
    public static By HWork1 = By.name("HWork1");
    public static By HWeek1 = By.name("HWeek1");
    public static By dt_price_offset = By.name("dt_price_offset");

    public static By smluvni_cenik_save = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[7]/div[4]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/button[1]");

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(buttonPreviousStep, buttonNextStep));
        return list;
    }

    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement() {
        WebActions.actual("Kontrola statických elementů na stránce", "control");
        elementsExistance(NewContractStepFive.getStaticElements());
    }
}
