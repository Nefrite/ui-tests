package cz.nanos.uitests.pages.NewContract;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.LinkedList;

public class NewContractStepTwo extends WebActions{
    static WebDriver driver;

    public NewContractStepTwo(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static By buttonPreviousStep = By.name("buttonPreviousStep");
    public static By buttonNextStep = By.name("buttonNextStep");
    public static By contract_type = By.name("contract_type");
    public static By voltage = By.name("voltage");

    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement(String value) {
        System.out.println("Kontrola fixních elementů - NewContractStepTwo");
        LinkedList<By> list = new LinkedList<By>();
        switch (value) {
            case "consumption":
                list.addAll(Arrays.asList(buttonPreviousStep, buttonNextStep, contract_type, voltage));
                break;
            case "production":
                list.addAll(Arrays.asList(buttonPreviousStep, buttonNextStep, contract_type));
                break;
            default:
                System.err.println("Chyba");
                throw new Error ("Chyba");
        }
        WebActions.elementsExistance(list);
    }

    public void set_contract_type_production () {
        selectSetByValue(contract_type, "production");
        System.out.println("Typ smlouvy výroba");
    }

    public void set_contract_type_consumption_NN () {
        selectSetByValue(contract_type, "consumption");
        selectSetByValue(voltage, "NN");
    }

    public void set_contract_type_consumption_VN () {
        selectSetByValue(contract_type, "consumption");
        selectSetByValue(voltage, "VN");
    }
}
