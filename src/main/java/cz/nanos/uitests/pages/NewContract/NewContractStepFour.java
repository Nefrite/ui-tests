package cz.nanos.uitests.pages.NewContract;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;

public class NewContractStepFour extends WebActions {
    static WebDriver driver;

    public NewContractStepFour(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static By buttonPreviousStep = By.name("buttonPreviousStep");
    public static By buttonNextStep = By.name("buttonNextStep");

    public static By addSearchEan = By.name("addSearchEan");
    public static By buttonSearch = By.name("buttonSearch");

    // po vyhledání EAN
    public static By buttonSave = By.xpath("//div[@class='new-opm-forms']//div//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static By buttonCancelChange = By.xpath("//div[contains(@class,'new-opm-forms')]//div//button[contains(@name,'buttonCancelChange')][contains(text(),'Zrušit změny')]");

    // typ výrobního OPM - pro výrobu
    public static By productionOPMtype = By.name("productionOPMtype");

    // sekce obecné
    public static By dist_network = By.name("dist_network");
    public static By btn_icon = By.name("btn_icon");
    public static By measurement_type = By.name("measurement_type");
    public static By memo = By.xpath("//div[@class='formGroupInputLabel']//div//textarea[@name='memo']");

    // odběr
    //public static By btn_icon = By.name("btn_icon");
    public static By code = By.name("code");
    public static By custom_name = By.name("custom_name");
    public static By phases = By.name("phases");
    public static By capacity = By.name("capacity");
    public static By distribution_tariff_code = By.name("distribution_tariff_code");
    public static By electrometer_code = By.name("electrometer_code");

    // odběr VN
    public static By reserved_power = By.name("reserved_power");
    public static By reserved_capacity_year = By.name("reserved_capacity_year");
    public static By control_stage_3 = By.name("control_stage_3");
    public static By control_stage_4 = By.name("control_stage_4");
    public static By control_stage_5 = By.name("control_stage_5");
    public static By control_stage_6 = By.name("control_stage_6");
    public static By control_stage_7_kwh = By.name("control_stage_7_kwh");
    public static By control_stage_7_hr = By.name("control_stage_7_hr");

    public static By btnIcon_AddMonth = By.name("btnIcon_AddMonth");
    public static By month_1 = By.name("month_1");
    public static By month_2 = By.name("month_2");
    public static By month_3 = By.name("month_3");
    public static By month_4 = By.name("month_4");
    public static By month_5 = By.name("month_5");
    public static By month_6 = By.name("month_6");
    public static By month_7 = By.name("month_7");
    public static By month_8 = By.name("month_8");
    public static By month_9 = By.name("month_9");
    public static By month_10 = By.name("month_10");
    public static By month_11 = By.name("month_11");
    public static By month_12 = By.name("month_12");
    public static By button_save_capacity = By.name(""+Calendar.getInstance().get(Calendar.YEAR));

    // výroba
    //public static By btn_icon = By.name("btn_icon");
    public static By power_station_name = By.name("power_station.name");
    public static By power_station_type_code = By.name("power_station.type_code");
    public static By power_station_installed_capacity = By.name("power_station.installed_capacity");
    public static By power_station_production_licence_code = By.name("power_station.production_licence_code");
    public static By power_station_grid_connection_contract_code = By.name("power_station.grid_connection_contract_code");
    public static By power_station_autoreport_ote = By.name("power_station.autoreport_ote");

    // atomová, termální

    // biomasa, bioplynová, vodní
    public static By power_station_stability_threshold = By.name("power_station.stability_threshold");
    public static By power_station_autoFixed_power = By.name("power_station.autoFixed_power");
    public static By power_station_fixed_power = By.name("power_station.fixed_power");

    // fotovoltaika
    public static By power_station_prediction_region = By.name("power_station.prediction_region");
    public static By power_station_prediction_index_1 = By.name("power_station.prediction_index_1");
    public static By power_station_prediction_index_2 = By.name("power_station.prediction_index_2");

    // kogenerační jednotka

    public static By power_station_flexible = By.name("power_station.flexible");
    public static By power_station_stability_threshold_kogen = By.xpath("//form//div[14]//div[1]//input[1]");
    public static By power_station_autoFixed_power_kogen = By.xpath("//input[@id='power_station.autoFixed_power']");
    public static By power_station_fixed_power_kogen = By.xpath("//form//div[16]//div[1]//input[1]");

    // větrná
    public static By power_station_prediction_region_wind = By.xpath("//form//div[22]//div[1]//input[1]");
    public static By power_station_prediction_index_1_wind = By.xpath("//form//div[23]//div[1]//input[1]");
    public static By power_station_prediction_index_2_wind = By.xpath("//form//div[24]//div[1]//input[1]");
    public static By power_station_prediction_map = By.name("power_station.prediction_map");

    /**
     * Elementy u kterých záleží na hodnotě dropdown
     */
    public static LinkedList<By> getVariablesElements(String selected_value) {
        LinkedList<By> listVariables = new LinkedList<By>();
        switch (selected_value)
        {
            case "BIOMASS":
                listVariables.add(power_station_stability_threshold);
                listVariables.add(power_station_autoFixed_power);
                listVariables.add(power_station_fixed_power);
                break;
            case "WATER":
                listVariables.add(power_station_stability_threshold);
                listVariables.add(power_station_autoFixed_power);
                listVariables.add(power_station_fixed_power);
                break;
            case "BIOGAS":
                listVariables.add(power_station_stability_threshold);
                listVariables.add(power_station_autoFixed_power);
                listVariables.add(power_station_fixed_power);
                break;
            case "WIND":
                listVariables.add(power_station_prediction_region_wind);
                listVariables.add(power_station_prediction_index_1_wind);
                listVariables.add(power_station_prediction_index_2_wind);
                listVariables.add(power_station_prediction_map);
                break;
            case "PHOTOVOLTAIC":
                listVariables.add(power_station_prediction_region);
                listVariables.add(power_station_prediction_index_1);
                listVariables.add(power_station_prediction_index_2);
                break;
            case "COGENERATION":
                //listVariables.addAll(Arrays.asList(power_station_flexible1, power_station_stability_threshold1, power_station_autoFixed_power1, power_station_fixed_power1));
                listVariables.add(power_station_flexible);
                listVariables.add(power_station_stability_threshold_kogen);
                listVariables.add(power_station_autoFixed_power_kogen);
                listVariables.add(power_station_fixed_power_kogen);
                break;
            case "NUCLEAR":
            case "THERMAL":
                break;
            default:
                System.err.println("Neplatná hodnota elementu power_station.type_code");
                break;
        }
        return listVariables;
    }

    /**
     * Kontrola všech elementů
     */
    public void changeTypeFactory(String value) {
        System.out.println("Kontrola elementů pro nastavenou elektrárnu: "+ value);
        selectSetByValue(power_station_type_code, value);
        WebActions.timeDelay(200);
        elementsExistance(getVariablesElements(value));
        System.out.println("-------------------------------------");
    }

    /**
     * Kontrola všech elementů - které se mění v závislosti na vybraném typu elektrárny
     */
    public void checkAllTypeFactory() {
        changeTypeFactory("NUCLEAR");
        changeTypeFactory("BIOMASS");
        changeTypeFactory("BIOGAS");
        changeTypeFactory("PHOTOVOLTAIC");
        changeTypeFactory("THERMAL");
        changeTypeFactory("WIND");
        changeTypeFactory("WATER");
    }

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(buttonPreviousStep, buttonNextStep, addSearchEan, buttonSearch));
        return list;
    }

    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement() {
        // kontrola statických elementů
        WebActions.actual("Kontrola statických elementů na stránce", "control");
        elementsExistance(NewContractStepFour.getStaticElements());
    }

    /**
     *
     * @param id_row
     */
    public static void controllRow(String id_row) {
        System.out.println("Kontrola nově přidaného řádku");
        WebActions.tableControllCell("eans_"+id_row);
        WebActions.tableControllCell("dist_network_"+id_row);
        WebActions.tableControllCell("address_string_"+id_row);
        WebActions.tableControllCell("measurement_type_"+id_row);
        WebActions.tableControllCell("distribution_tariff_code_"+id_row);
        WebActions.tableControllCell("phases_string_"+id_row);
        WebActions.tableControllCell("custom_name_"+id_row);
    }

    public static void fillForm(WebDriver driver) {
        /*
        driver.findElement(By.xpath("//input[@name='dist_network' and @autocomplete='false']")).sendKeys("0365");
        WebActions.timeDelay(1000);
        driver.findElement(By.xpath("//li[@value='0365']")).click();
        WebActions.timeDelay(1000);
        driver.findElement(By.xpath("//button[@title='Editovat adresu']")).click();
        WebActions.timeDelay(1000);
        ModalAddNewAddress.fillForm(driver, "POBOX", WebActions.unique());

        WebActions.timeDelay(1000);
        WebActions.radioSet("measurement_type", "B");
        actions.writeText(NewContractStepFour.memo, "Poznámka krok 4");
        actions.writeText(NewContractStepFour.custom_name, "Značka odběr");
        actions.selectSet(NewContractStepFour.phases, "1");
        actions.writeText(NewContractStepFour.capacity, "20");
        actions.selectSet(NewContractStepFour.distribution_tariff_code, "D01D");
        actions.writeText(NewContractStepFour.electrometer_code, "123456");
        */
    }
}
