package cz.nanos.uitests.pages.NewContract;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.LinkedList;

public class NewContractStepThree extends WebActions{
    static WebDriver driver;

    public NewContractStepThree(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static By buttonPreviousStep = By.name("buttonPreviousStep");
    public static By buttonNextStep = By.name("buttonNextStep");

    // záložky
    public static By nastaveni = By.name("Nastavení");
    public static By cena_silove_elektriny = By.name("Ceny sil. elektřiny");

    // Záložka nastavení
    // odber NN
    public static By consumption_tariff_id = By.name("consumption_tariff_id");
    public static By contact_person_id = By.name("contact_person_id");
    public static By client_code = By.name("client_code");
    public static By summary_advance = By.name("summary_advance");
    public static By summary_bill = By.name("summary_bill");
    public static By consumption_only = By.name("consumption_only");
    public static By use_default_contact_setting = By.name("use_default_contact_setting");
    public static By memo = By.name("memo");

    // odběr VN - obsahuje stejné WebElementy jako pro NN
    /*
    public static By contact_person_id = By.name("contact_person_id");
    public static By client_code = By.name("client_code");
    public static By consumption_only = By.name("consumption_only");
    public static By use_default_contact_setting = By.name("use_default_contact_setting");
    public static By memo = By.name("memo");
    */

    // výroba
    public static By production_tariff_id = By.name("production_tariff_id");
    /*
    public static By contact_person_id = By.name("contact_person_id");
    public static By client_code = By.name("client_code");
    public static By memo = By.name("memo");
    */

    // Záložka cena silové elektřiny
    // odběr/spotřeba NN
    public static By btnIcon_AddFee = By.name("btnIcon_AddFee");
    public static By btnIcon_AddPrice = By.name("btnIcon_AddPrice");

    // Poplatek za OPM
    public static By calendarSincenew = By.name("calendarSince_fee_new");
    public static By calendarUntilnew = By.name("calendarUntil_new");
    public static By trade_fee_new = By.name("trade_fee_new");
    public static By confirmed_fee_new = By.name("confirmed_fee_new");
    //public static By btnIcon_Save = By.xpath("/button[@title='Uložit']");
    public static By btnSave_new = By.name("btnSave_new");
    public static By btnIcon_Storno = By.name("btnIcon_Storno");
    public static By btnIcon_Remove = By.name("btn_del_fee_new");
    //public static By editovat = By.xpath("/button[@title='Editovat']");

    // Dle distribučních sazeb
    public static By distribution_tariff_new = By.name("distribution_tariff_new");
    public static By calendarSince_price_new = By.name("calendarSince_price_new");
    public static By calendarUntil_price_new = By.name("calendarUntil_price_new");
    public static By high_tariff_price_new = By.name("high_tariff_price_new");
    public static By low_tariff_price_new = By.name("low_tariff_price_new");
    public static By confirmed_price_new = By.name("confirmed_price_new");
    //public static By btnIcon_Save = By.name("btnSave_new");
    //public static By btnIcon_Storno = By.name("btnIcon_Storno");
    //public static By btnIcon_Remove = By.name("btn_del_fee_new");

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(buttonPreviousStep, buttonNextStep));
        // , btnIcon_AddFee, btnIcon_AddPrice
        return list;
    }

    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement() {
        WebActions.actual("Kontrola statických elementů na stránce", "control");
        elementsExistance(NewContractStepThree.getStaticElements());
        timeDelay(1000);
    }
}
