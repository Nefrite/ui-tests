package cz.nanos.uitests.pages;

import java.util.Arrays;
import java.util.LinkedList;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;

public class HomePage {
	
	public static final String url = "/home";
	
    public static final By queue_1_unbilled = By.name("queue_1_unbilled");
    public static final By queue_1_refund = By.name("queue_1_refund");
    public static final By queue_1_overdued = By.name("queue_1_overdued");
    public static final By queue_1_unused = By.name("queue_1_unused");
    public static final By queue_1_accounting = By.name("queue_1_accounting");
    public static final By queue_1_encashments = By.name("queue_1_encashments");
    public static final By queue_1_taxes = By.name("queue_1_taxes");
    public static final By queue_1_inclusion = By.name("queue_1_inclusion");
    public static final By queue_1_consumer = By.name("queue_1_consumer");
    public static final By queue_1_supplier = By.name("queue_1_supplier");
    public static final By new_1_consumer = By.name("new_1_consumer");
    public static final By upload_1_document = By.name("upload_1_document");

    public static final By queue_2_unbilled = By.name("queue_2_unbilled");
    public static final By queue_2_refund = By.name("queue_2_refund");
    public static final By queue_2_overdued = By.name("queue_2_overdued");
    public static final By queue_2_unused = By.name("queue_2_unused");
    public static final By queue_2_accounting = By.name("queue_2_accounting");
    public static final By queue_2_encashments = By.name("queue_2_encashments");
    public static final By queue_2_taxes = By.name("queue_2_taxes");
    public static final By queue_2_inclusion = By.name("queue_2_inclusion");
    public static final By queue_2_consumer = By.name("queue_2_consumer");
    public static final By queue_2_supplier = By.name("queue_2_supplier");
    public static final By new_2_consumer = By.name("new_2_consumer");
    public static final By upload_2_document = By.name("upload_2_document");

    public static final By queue_3_unbilled = By.name("queue_3_unbilled");
    public static final By queue_3_refund = By.name("queue_3_refund");
    public static final By queue_3_overdued = By.name("queue_3_overdued");
    public static final By queue_3_unused = By.name("queue_3_unused");
    public static final By queue_3_accounting = By.name("queue_3_accounting");
    public static final By queue_3_encashments = By.name("queue_3_encashments");
    public static final By queue_3_taxes = By.name("queue_3_taxes");
    public static final By queue_3_inclusion = By.name("queue_3_inclusion");
    public static final By queue_3_consumer = By.name("queue_3_consumer");
    public static final By queue_3_supplier = By.name("queue_3_supplier");
    public static final By new_3_consumer = By.name("new_3_consumer");
    public static final By upload_3_document = By.name("upload_3_document");

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     * @return 
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(queue_1_unbilled, queue_1_refund, queue_1_overdued,
                queue_1_unused, queue_1_accounting, queue_1_encashments, queue_1_taxes,
                queue_1_inclusion, queue_1_consumer, queue_1_supplier, new_1_consumer, upload_1_document,

                queue_2_unbilled, queue_2_refund, queue_2_overdued,
                queue_2_unused, queue_2_accounting, queue_2_encashments, queue_2_taxes,
                queue_2_inclusion, queue_2_consumer, queue_2_supplier, new_2_consumer, upload_2_document,

                queue_3_unbilled, queue_3_refund, queue_3_overdued,
                queue_3_unused, queue_3_accounting, queue_3_encashments, queue_3_taxes,
                queue_3_inclusion, queue_3_consumer, queue_3_supplier, new_3_consumer, upload_3_document));
        return list;
    }

    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement() {
        WebActions.actual("HOMEPAGE - Kontrola statických elementů", "control");
        WebActions.elementsExistance(HomePage.getStaticElements());
    }
}