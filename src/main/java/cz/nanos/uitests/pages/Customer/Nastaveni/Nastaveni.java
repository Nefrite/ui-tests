package cz.nanos.uitests.pages.Customer.Nastaveni;

import cz.nanos.uitests.pages.HomePage;
import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.LinkedList;

public class Nastaveni {

    public static By buttonSave = By.name("buttonSave");

    // osobní údaje
    public static By personTitles = By.name("person.titles");
    public static By personFirstname = By.name("person.firstname");
    public static By personLastname = By.name("person.lastname");
    public static By personDegrees = By.name("person.degrees");
    public static By calendarBirthday = By.name("calendarBirthday");
    public static By personTaxCode = By.name("person.tax_code");

    // nastavení
    public static By sales_representative_id = By.name("sales_representative_id");
    public static By primary_contact_person_id = By.name("primary_contact_person_id");
    public static By person_memo = By.name("person.memo");
    public static By person_vip = By.name("person.vip");
    public static By autosend_demands = By.name("autosend_demands");
    public static By autosend_advance_invoice = By.name("autosend_advance_invoice");
    public static By autosend_advance_reminders = By.name("autosend_advance_reminders");
    public static By autosend_payment_receipts = By.name("autosend_payment_receipts");

    /**
     * Adresy
     */
    public static By addAddressNext = By.name("btn_add_new_type");
    public static By addNewAddress = By.name("addNewAddress");
    public static By btn_add_concrete_addr = By.name("btn_add_concrete_addr");
    public static By btn_save_concrete_addr = By.name("btn_save_concrete_addr");


    public static By editDomicileAddress = By.name("btn_add_domicile");

    /**
     * Obecné kontakty
     */
    public static By addBasicContact = By.name("btn_addBasicContact");

    /**
     * Použití kontaktů
     */
    public static By addContact = By.name("btn_add_contactUsage");
    public static By contact_select = By.name("value");
    public static By contact_general = By.name("GENERAL_new");
    public static By contact_advances = By.name("ADVANCES_new");
    public static By contact_bill = By.name("BILL_new");
    public static By contact_invoice = By.name("INVOICE_new");
    public static By contact_marketing = By.name("MARKETING_new");
    public static By contact_demand = By.name("DEMAND_new");
    public static By contact_production = By.name("PRODUCTION_new");
    public static By contact_save = By.name("btnSave_new");
    public static By contact_storno = By.name("btnStorno_new");
    public static By contact_delete = By.name("btn_del_new");


    /**
     * Platební kontakt
     */
    public static By addPaymentContact = By.name("btnIcon_Add");


    public static void checkAllEnabledCheckbox(LinkedList<WebElement> elements) {
        for (WebElement element: elements) {
            if (element.isEnabled()) {
                if (!element.isSelected() ) {
                    element.click();
                    System.out.println("Element '" + element.getAttribute("name") + "' je zaškrtnutý.");
                } else {
                    System.out.println("Element '" + element.getAttribute("name") + "' již byl zaškrtnutý.");
                }
            } else {
                System.out.println("Element '" + element.getAttribute("name") + "' je nedostupný.");
            }
        }
    }

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     * @return
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(personTitles, personFirstname, personLastname, personDegrees,
                calendarBirthday, personTaxCode,
                sales_representative_id, primary_contact_person_id, person_memo, person_vip,
                autosend_demands, autosend_advance_invoice, autosend_advance_reminders, autosend_payment_receipts,
                addAddressNext,
                addBasicContact,
                addContact,
                addPaymentContact
                ));
        System.out.println("Nastaveni - Kontrola fixních elementů");
        return list;
    }


    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement() {
        WebActions.actual("Kontrola statických elementů na stránce", "control");
        //WebActions.actual("CUSTOMER NASTAVENI - NASTAVENI", "section");
        // kontrola statických elementů
        WebActions.elementsExistance(Nastaveni.getStaticElements());
    }

    public static void controlAllElement(LinkedList listElements, String nadpis) {
        WebActions.actual(nadpis, "control");
        WebActions.elementsExistance(listElements);
    }
}
