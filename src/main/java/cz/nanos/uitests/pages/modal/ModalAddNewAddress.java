package cz.nanos.uitests.pages.modal;

import java.nio.file.WatchEvent;
import java.util.Arrays;
import java.util.LinkedList;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ModalAddNewAddress extends WebActions {
    static WebDriver driver;

    public ModalAddNewAddress(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static final By buttonCloseModal = By.name("buttonCloseModal");
    public static final By buttonSave = By.name("btnSaveAddress");
    public static final By buttonDisplayAddress = By.name("buttonDisplayAddress");

    public static final By country_code  = By.name("country_code");
    public static final By town_name = By.name("town_name");
    public static final By post_code = By.name("post_code");
    public static final By local_place_name = By.name("local_place_name");
    public static final By street_name = By.name("street_name");
    public static final By number_type = By.name("number_type");
    // dropdown number_type
    //HOUSE
    public static final By house_nr = By.name("house_nr");
    public static final By orientation_nr = By.name("orientation_nr");
    //REGISTRATION
    public static final By registration_nr = By.name("registration_nr");
    //PLOT
    public static final By plot_number = By.name("plot_number");
    //POBOX
    public static final By pobox = By.name("pobox");

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     * @return 
     */
    public static LinkedList<By> getStaticElements2() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(buttonCloseModal, buttonSave, buttonDisplayAddress));
        list.addAll(Arrays.asList(country_code, town_name, post_code, local_place_name, street_name, number_type));
        return list;
    }

    /**
     * Elementy u kterých záleží na hodnotě dropdown
     */
    public static LinkedList<By> getVariablesElements2(String selected_value) {
        System.out.println("Kontrola proměnlivých elementů");
        LinkedList<By> listVariables = new LinkedList<By>();
        switch (selected_value)
        {
            case "HOUSE":
                listVariables.add(house_nr);
                listVariables.add(orientation_nr);
                break;
            case "REGISTRATION":
                listVariables.add(registration_nr);
                break;
            case "PLOT":
                listVariables.add(plot_number);
                break;
            case "POBOX":
                listVariables.add(pobox);
                break;
        }
        return listVariables;
    }

    /**
     * Kontrola dle hodnoty dropdown menu
     */
    public void checkAddressDropDown2(String selected_value) {
        System.out.println("Kontrola WebElementu pro dropdown s hodnotou: " + selected_value);
        selectSetByValue(number_type, selected_value);
        WebActions.elementsExistance(getVariablesElements2(selected_value));
        WebActions.timeDelay(500);
    }

    /**
     * Precondition: správně vyplněný formulář
     * Uloženo a zavřeno modální okno
     */
    public static void save(WebDriver driver) {
        driver.findElement(buttonSave).click();
        System.out.println("ModalAddNewAddress Save and closed");
    }

    /**
     * Smazaní změn ve formuláří, zobrazena původní adresa
     */
    public static void clearChange() {
        driver.findElement(buttonDisplayAddress).click();
        System.out.println("ModalAddNewAddress zobrazena původní adresa.");
    }

    /**
     * Zavření modálního okna
     */
    public static void close() {
        driver.findElement(buttonCloseModal).click();
        System.out.println("ModalAddNewAddress zavřen křížkem.");
    }
}
