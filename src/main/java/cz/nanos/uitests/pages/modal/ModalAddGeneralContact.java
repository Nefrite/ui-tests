package cz.nanos.uitests.pages.modal;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.LinkedList;

public class ModalAddGeneralContact extends WebActions {
    static WebDriver driver;

    public ModalAddGeneralContact(WebDriver driver) {
        super(driver);
        this.driver = driver;

    }

    public static final By buttonCloseModal = By.name("buttonCloseModal");
    //public static final By buttonSave = By.name("buttonSave");
    public static final By buttonSave = By.xpath("//div[@class='modal-footer']//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static final By buttonStorno = By.name("buttonStorno");
    public static final By buttonReset = By.name("btnReset");

    public static final By firstname = By.name("firstname");
    public static final By lastname = By.name("lastname");
    public static final By salutation_first_name = By.name("salutation_first_name");
    public static final By salutation_last_name = By.name("salutation_last_name");
    public static final By sex = By.name("sex");
    public static final By position = By.name("position");
    public static final By emails = By.name("emails");
    public static final By btn_addMail = By.name("btn_addMail");
    public static final By phone_numbers = By.name("phone_numbers");
    public static final By btn_addPhone = By.name("btn_addPhone");
    public static final By memo = By.name("memo");

    public static final By btn_del_phone_numbers_0 = By.name("btn_del_phone_numbers_0");
    public static final By btn_del_emails_0 = By.name("btn_del_emails_0");

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(buttonCloseModal, buttonSave, buttonStorno, buttonReset));
        list.addAll(Arrays.asList(firstname, lastname, salutation_first_name, salutation_last_name, sex, position,
                emails, btn_addMail, phone_numbers, btn_addPhone, memo));
        return list;
    }

    /**
     * Kontrola všech elementů
     */
    public static void controlAllElement() {
        WebActions.elementsExistance(ModalAddGeneralContact.getStaticElements());
    }
}