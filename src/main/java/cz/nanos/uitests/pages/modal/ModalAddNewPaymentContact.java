package cz.nanos.uitests.pages.modal;

import cz.nanos.uitests.service.WebActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ModalAddNewPaymentContact extends WebActions {
    /*
    static WebDriver driver;

    public ModalAddNewPaymentContact(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }
    */

    public ModalAddNewPaymentContact(WebDriver driver) {
        super(driver);
    }

    protected static WebActions actions;


    public static final By buttonCloseModal = By.name("buttonCloseModal");
    public static final By buttonSave = By.xpath("//div[@class='modal-footer']//button[@name='buttonSave'][contains(text(),'Uložit')]");
    public static final By buttonCancelChange = By.name("buttonCancelChange");

    public static final By type = By.name("type");

    // typ číslo účtu
    public static final By account_prefix = By.name("account_prefix");
    public static final By account_number = By.name("account_number");
    public static final By bank_code = By.name("bank_code");

    // typ sipo
    public static final By sipo_code = By.name("sipo_code");

    // společné pro oba typy
    public static final By name = By.name("name");
    public static final By memo = By.name("memo");

    /**
     * Kontrola všech elementů
     */
    public void controlAllElement() {
        // kontrola statických elementů
        checkTypeDropDown("S");
        checkTypeDropDown("A");
    }

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     */
    public static LinkedList<By> getStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(buttonCloseModal, buttonSave, buttonCancelChange));
        list.addAll(Arrays.asList(type, name, memo));
        return list;
    }

    /**
     * Elementy u kterých záleží na hodnotě dropdown
     */
    public static LinkedList<By> getVariablesElements(WebDriver driver) {
        //System.out.println("Kontrola proměnlivých elementů");
        LinkedList<By> listVariables = new LinkedList<By>();
        switch (actions.selectGetSelectedValue(type)) {
            case "A":
                listVariables.addAll(Arrays.asList(account_prefix, account_number, bank_code));
                break;
            case "S":
                //listVariables.addAll(Arrays.asList(sipo_code));
                listVariables.add(sipo_code);
                break;
        }
        return listVariables;
    }

    /**
     * Kontrola dle hodnoty dropdown menu
     */
    public void checkTypeDropDown (String selected_value) {
        System.out.println("Kontrola WebElementu pro dropdown s hodnotou: " + selected_value);
        actions.selectSetByValue(type, selected_value);
        WebActions.elementsExistance(getVariablesElements(driver));
        WebActions.timeDelay(500);
    }

    /**
     * Precondition: správně vyplněný formulář
     * Uloženo a zavřeno modální okno
     */
    public static void save(WebElement button) {
        button.click();
        System.out.println("ModalAddNewPaymentContact uložen pomocí tlačítka 'Uložit'");
    }

    /**
     * Smazaní změn ve formuláří, zobrazena původní adresa
     */
    public static void clearChange() {
        driver.findElement(buttonCancelChange).click();
        System.out.println("ModalAddNewPaymentContact zobrazeno původní nastavení.");
    }

    /**
     * Zavření modálního okna
     */
    public static void close() {
        driver.findElement(buttonCloseModal).click();
        System.out.println("ModalAddNewPaymentContact zavřen křížkem.");
    }

}
