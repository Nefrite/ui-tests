package cz.nanos.uitests.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.nanos.uitests.pages.Customer.Nastaveni.Nastaveni;
import cz.nanos.uitests.pages.Header;
import cz.nanos.uitests.pages.NewContract.NewContractStepOne;
import cz.nanos.uitests.pages.modal.ModalAddGeneralContact;
import cz.nanos.uitests.pages.modal.ModalAddNewAddress;
import cz.nanos.uitests.pages.modal.ModalAddNewPaymentContact;
import org.awaitility.Awaitility;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.LinkedList;

/**
 * Test case actions provider
 */
public class WebActions extends Actions {

    public static WebDriver driver;
    public WebDriverWait wait;
    public static WebDriverWait waitStatic;

    public static String search_customer = "Kalina";
    public static String search_customer_url = "/customer/1725/setting";

    public static String search_opm_NN = "859182400306127673";
    public static String search_opm_NN_opm = "/opm/3438/setting";
    public static String search_opm_NN_smlouva = "/customer/1725/contracts/C/2138/setting";

    public static String basicUrl="http://localhost:3000";

    public WebActions (WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 100);
    }

    /**
     * zapíše text do vyhledávacího okna text a klikne na adresu kterou vrátí našeptávač
     * @param search_text
     * @param url
     */
    public void search(String search_text, String url){
        click(driver.findElement(Header.phrase));
        writeText(Header.phrase, search_text);
        clickLinkByHref(url);
        checkUrl(WebActions.basicUrl+url);
    }

    /**
     * Checks existence of given elements set
     */
    public static void elementsExistance(List<By> elements){
        for (By by : elements) {
            try {
                WebElement element = driver.findElement(by);
                if (element.isDisplayed()) {
                } else {
                    System.err.println("WebElement 'name=" + element.getAttribute("name") + "' not displayed");
                }
            } catch (NoSuchElementException e) {
                System.err.println("WebElement 'name=" + by + "' doesnt exist on page!");
                driver.quit();
                throw new Error ("WebElement 'name=" + by + "' doesnt exist on page!");
            }
        }
    }

    public static void elementExistance(By elementBy) {
            try {
                WebElement element = driver.findElement(elementBy);
                if (element.isDisplayed()) {
                    System.out.println("WebElement 'name=" + element.getAttribute("name") + "' displayed");
                } else {
                    System.err.println("WebElement 'name=" + element.getAttribute("name") + "' not displayed");
                }
            } catch (NoSuchElementException e) {
                System.err.println("WebElement 'name=" + elementBy + "' doesnt exist on page!");
            }
        timeDelay(100);
    }

    /**
     * Vrátí text z WebElementu
     * @param elementBy
     * @return
     */
    public String getText(By elementBy) {
        waitVisibility(elementBy);
        System.out.println("Element '" + elementBy + "' má zadán text: '" + driver.findElement(elementBy).getAttribute("value") +"'");
        return driver.findElement(elementBy).getAttribute("value");
    }

    /**
     * Zapíše text do WebElementu
     * @param elementBy
     * @param text
     */
    public void writeText (By elementBy, String text) {
        waitVisibility(elementBy);
        try {
            clearText(elementBy);
            driver.findElement(elementBy).sendKeys(text);
            System.out.println("Do WebElementu '" + elementBy + "' zapsán text: '" + text + "'");
        } catch (Exception e) {
            System.err.println("Do WebElement '" + elementBy + "' se nepodařilo zapsat : '" + text + "'");
            driver.close();
            throw new Error ("Do WebElement '" + elementBy + "' se nepodařilo zapsat : '" + text + "'");
        }
    }

    /**
     * Clear text from WebElement
     * @param elementBy
     */
    public void clearText (By elementBy) {
        waitVisibility(elementBy);
        try {
            driver.findElement(elementBy).clear();
        } catch (Exception e) {
            System.err.println("Z webElement '" + elementBy + "' se nepodařil odstranit text.");
            driver.close();
            throw new Error ("Z webElement '" + elementBy + "' se nepodařil odstranit text.");
        }
    }

    /**
     * Zapíše do kalendáře hodnotu ze Stringu
     * @param elementBy
     * @param date
     */
    public void setCalendar (By elementBy, String date) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).click();
        driver.findElement(elementBy).sendKeys(Keys.LEFT);
        driver.findElement(elementBy).sendKeys(Keys.LEFT);
        driver.findElement(elementBy).sendKeys(date);
        System.out.println("Calendar " + elementBy + " set on date: " + date.substring(0,2) + "." + date.substring(2,4) + "." +date.substring(4,8));
    }

    /**
     * Kliknutí levým tlačítkem na WebElement
     * Pokud po kliknutí zobrazen Loader, tak čeká najeho schování
     * @param elementBy
     * @throws Exception
     */
    public void clickOnElement(By elementBy){
		waitVisibility(elementBy);
		try {
		    wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
            wait.until(ExpectedConditions.elementToBeClickable(elementBy));
            driver.findElement(elementBy).click();
        } catch (Exception e) {
            System.err.println("WebElement '" + driver.findElement(elementBy).getAttribute("name") + "' is not clickable!");
            driver.quit();
            throw new Error ("WebElement '" + elementBy + "' is not clickable!\n\n" + e);
        }
        waitHideLoader();
    }

    /**
     * kline na WebElement a zkontroluje toast s danou hláškou
     * nečeká na konkrétní text v toasteru, ale porovná první nalezený (kvůli timeoutu)
     * @param elementBy
     * @param text_toaster
     * @param text_error
     * @throws Exception
     */
    public void clickAndControlToast(By elementBy, String text_toaster, String text_error){
        clickOnElement(elementBy);
        try {
            //wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//p[@class='toast-content'][contains(text(),'" + text_toaster+ "')]")));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[@class='toast-content']")));
            //wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//p[@class='toast-content']")));
            if (text_toaster.contains(driver.findElement(By.xpath("//p[@class='toast-content']")).getText())) {
                System.out.println("Toaster s textem '" + text_toaster + "' zobrazen - vše v pořádku.");
            } else {
                System.err.println(text_error);
                System.err.println("Obsah toasteru: '" + driver.findElement(By.xpath("//p[@class='toast-content']")).getText() + "'");
                throw new Error ("Obsah toasteru: '" + driver.findElement(By.xpath("//p[@class='toast-content']")).getText() + "'");
            }
        } catch (Exception e) {
            System.err.println("Obsah toasteru: '" + driver.findElement(By.xpath("//p[@class='toast-content']")).getText() + "'");
            driver.quit();
            throw new Error (text_error + "\n" + e);
        }
    }

    /**
     * Funkce čeká na skrytí Toasteru, protože
     * @throws Exception
     */
    public void waitHideToast(){
        //System.err.println(LocalDateTime.now());
        try {
            wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//p[@class='toast-content']"))));
            //System.err.println(LocalDateTime.now());
        } catch (Exception e) {
            System.err.println("Toast zobrazen více jak 30s");
            driver.quit();
            throw new Error ("Toast zobrazen více jak 30s" + "\n" + e);
        }
    }
/*
    clickOnElement(elementBy);
        try {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//p[@class='toast-content'][contains(text(),'" + text_toaster+ "')]")));
        //wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//p[@class='toast-content']")));
        System.out.println("Toaster s textem '" + text_toaster + "' zobrazen - vše v pořádku.");
    } catch (Exception e) {
        System.err.println("V toasteru neočekáváná hodnota: '" + driver.findElement(By.xpath("//p[@class='toast-content']")).getText() + "'");
        System.err.println(text_error);
        throw new Error (text_error + "\n" + e);
    }
    */

    /**
     * Čekání na viditelnost elementu
     * @param elementBy
     */
    public void waitVisibility(By elementBy) {
        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
        } catch (NoSuchElementException  e) {
            System.err.println("WebElement '" + elementBy + "' v timeoutu 30s nebyl zobrazen!\n" + e);
            driver.quit();
            throw new Error ("WebElement '" + elementBy + "' v timeoutu 30s nebyl zobrazen!");
        }
	}

    /**
     * Čekání na načtení dat z API, a také nez se vybere jedna hodnota
     * timeout pro načtení dat do selectu 30 sekund
     * @param elementBy
     */
    public void waitDropdownLoadValue(By elementBy) {
        waitVisibility(elementBy);
        Select dropdown = new Select(driver.findElement(elementBy));
        if (dropdown.getOptions().size() == 1) {
            LocalDateTime before = LocalDateTime.now();
            try {
                /*
                wait.until(
                        ExpectedConditions.presenceOfNestedElementLocatedBy(elementBy, By.tagName("option"))
                );
                */
                Awaitility.await("Waiting for API response")
                        .atMost(30, TimeUnit.SECONDS)
                        //.until(() -> (dropdown.getOptions().size() > 1) && (!dropdown.getFirstSelectedOption().getAttribute("value").isEmpty()));
                        .until(() -> (dropdown.getOptions().size() > 1));
            } catch (Exception e) {
                System.err.println("Načtení hodnot z API do WebElementu '" + elementBy + "' trvalo více jak 30s.");
                driver.quit();
                throw new Error ("Načtení hodnot z API do WebElementu '" + elementBy + "' trvalo více jak 30s.");
            }
            System.out.println("WebElement '" + elementBy + "' neměl načtenou hodnotu z API.");
            System.out.println("Čekání na odpověď API ze serveru: " + ChronoUnit.MILLIS.between(before, LocalDateTime.now()) + " ms.");
        }
    }

    public void waitDropdownLoadValue2(By elementBy) {
        waitVisibility(elementBy);

        Select dropdown = new Select (driver.findElement(elementBy));
        int pocitadlo = 0;
        int maximum = 30000;
        while ((dropdown.getOptions().size() == 1) && (pocitadlo < maximum)) {
            timeDelay(50);
            pocitadlo += 50;
            System.err.println("pocitadlo " + pocitadlo);
        }

        System.err.println("Čekání na načtení hodnot z API: " + pocitadlo + " ms.");

        if (pocitadlo == maximum) {
            System.err.println("Načtení hodnot do WebElementu '" + elementBy + "' trvalo více jak " + maximum + " ms.");
            throw new Error ("Načtení hodnot do WebElementu '" + elementBy + "' trvalo více jak " + maximum + " ms.");
        }

        timeDelay(1000);
    }

    /**
     * funkce která čeká na zmniznutí WebElementu, timeout 30s
     * @param elementBy
     */
    public void waitInvisibility(By elementBy) {
        LocalDateTime from =  LocalDateTime.now();
        if (!driver.findElements(elementBy).isEmpty()) {
            try {
                wait.until(ExpectedConditions.invisibilityOf(driver.findElement(elementBy)));
                System.out.println("Čekání na skrytí WebElementu '" + elementBy + "'. Čekání od: " + from + " do: " + LocalDateTime.now());
            } catch (NoSuchElementException  e) {
                System.err.println("WebElement '" + elementBy + "' v timeoutu 30s je stále zobrazen!");
                driver.quit();
                throw new Error ("WebElement '" + elementBy + "' v timeoutu 30s je stále zobrazen!");
            }
        }
        timeDelay(100);
    }

    /**
     * Čeká do okamžiku, než bude schován Loader
     */
    public void waitHideLoader () {
        turnOffImplicitWaits();
        if (driver.findElements(By.className("rai-spinner-inner")).size() > 0) {
            //System.err.println("LOADER");
            //System.err.println("start " + java.time.LocalDateTime.now());
            wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.className("rai-spinner-inner"))));
            //System.err.println("stop " + java.time.LocalDateTime.now());
        }
        turnOnImplicitWaits();
    }

    /**
     * vypnutí timeout
     */
    private void turnOffImplicitWaits() {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    /**
     * zapnutí implicitního timeout
     */
    private void turnOnImplicitWaits() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    /**
     * okamžitá kontrola, zda je daný element zobrazen
     * @param elementBy
     * @return
     */
    public boolean isElementHiddenNow(By elementBy) {
        turnOffImplicitWaits();
        boolean result = false;
        try {
            result = ExpectedConditions.invisibilityOfElementLocated(elementBy).apply(driver);
        }
        finally {
            turnOnImplicitWaits();
        }
        return result;
    }

    public void refresh () {
        driver.navigate().refresh();
        waitHideLoader();
    }

    /**
     * Print actual URL
     */
    public void getUrl () {
        System.out.println("Current url: " + driver.getCurrentUrl());
    }

    /**
     * Check the current url with expected url
     * @param url
     */
    public void checkUrl (String url) {
        timeDelay(500);
        if (!driver.getCurrentUrl().equals(url) ) {
            System.err.println("Wrong address! ");
            System.err.println("Current: " + driver.getCurrentUrl() + " - Expected: " + url);
        }
        System.out.println("Current: " + driver.getCurrentUrl() + " - Expected: " + url);
    }

    /**
     * vrátí náhodné číslo
     * @param min
     * @param max
     * @return
     */
    public int getRandom (int min, int max) {
        return (min + (int)(Math.random() * ((max - min) + 1)));
    }

    /**
     * Kliknutí na WebElement, který má zadanou href
     * @param href
     */
    public void clickLinkByHref(String href){
        waitVisibility(By.xpath("//a[contains(@href, '" + href + "')]"));
        clickOnElement(By.xpath("//a[contains(@href, '" + href + "')]"));
    }

    /**
     * Kliknutí na kontextové menu
     * @param by
     * @param row_id
     * @param menu_item
     */
    public void contextMenuClick (String by, String row_id, String menu_item) {
        System.out.println("Vybrána akce: " +  menu_item);
        Actions action = new Actions(driver);
        switch (by) {
            case "name":
                action.moveToElement(driver.findElement(By.className(row_id))).contextClick().build().perform();
                break;
            case "linkText":
                action.moveToElement(driver.findElement(By.linkText(row_id))).contextClick().build().perform();
                break;
            default:
                System.err.println("!!! Wrong parameter By. !!!");
        }
        try {
            By selected_item = By.className(menu_item);
            clickOnElement(selected_item);
        } catch (Exception e) {
            e.printStackTrace();
        }
        timeDelay(100);
    }

    /**
     * Zaškrtni checkbox
     * @param elementBy
     */
    public void checkboxChecked (By elementBy) {
        waitVisibility(elementBy);
        WebElement checkbox = driver.findElement(elementBy);
        if (checkbox.isEnabled()) {
            if (!checkbox.isSelected() ) {
                checkbox.click();
                System.out.println("Element '" + checkbox.getAttribute("name") + "' je zaškrtnutý.");
            } else {
                System.out.println("Element '" + checkbox.getAttribute("name") + "' již byl zaškrtnutý.");
            }
        } else {
            System.out.println("Element '" + checkbox.getAttribute("name") + "' je nedostupný.");
        }
        timeDelay(100);
    }

    // Checkbox - unchecked

    /**
     * Odškrtnutý checkbox
     * @param elementBy
     */
    public void checkboxUnChecked(By elementBy) {
        waitVisibility(elementBy);
        WebElement checkbox = driver.findElement(elementBy);
        if (checkbox.isEnabled()) {
            if (checkbox.isSelected() ) {
                checkbox.click();
                System.out.println("Element '" + checkbox.getAttribute("name") + "' je nezaškrtnutý.");
            } else {
                System.out.println("Element '" + checkbox.getAttribute("name") + "' již byl nezaškrtnutý.");
            }
        } else {
            System.out.println("Element '" + checkbox.getAttribute("name") + "' je nedostupný.");
        }
        timeDelay(100);
    }

    /**
     * Nastaví checkbox na požadovanou hodnotu
     * @param elementBy
     * @param value
     */
    public void checkboxSetValue(By elementBy, Boolean value) {
        waitVisibility(elementBy);
        WebElement checkbox = driver.findElement(elementBy);
        if (checkbox.isEnabled()) {
            if (value) {
                if (!checkbox.isSelected()) {
                    click(checkbox);
                }
            } else {
                if (checkbox.isSelected()) {
                    click(checkbox);
                }
            }
        } else {
            System.err.println("Checkbox '" + elementBy + "' není dostupný!");
            throw new Error ("Checkbox '" + elementBy + "' není dostupný!");
        }
    }

    /**
     *
     * @param elementBy
     * @throws Exception
     */
    public void checkboxReverseValue(By elementBy){
        waitVisibility(elementBy);
        if (driver.findElement(elementBy).isEnabled()) {
            clickOnElement(elementBy);
        } else {
            System.err.println("Checkbox '" + elementBy + "' není dostupný!");
            throw new Error ("Checkbox '" + elementBy + "' není dostupný!");
        }
    }

    /**
     *
     * @param elementBy
     * @return
     */
    public Boolean checkboxGetValue (By elementBy) {
        waitVisibility(elementBy);
        return driver.findElement(elementBy).isSelected();
    }

    /**
     * Kontroluje, zda daný checkbox je zaškrtnutý/odškrtnutý na danou hodnotu
     * @param elementBy
     * @param hodnota
     */
    public void checkboxCheckValue (By elementBy, Boolean hodnota) {
        waitVisibility(elementBy);
        WebElement checkbox = driver.findElement(elementBy);
        if (checkbox.isSelected() == hodnota) {
            if (checkbox.isSelected()) {
                System.out.println("Element '" + elementBy + "' je zaškrtnutý, a je to v pořádku!");
            } else {
                System.out.println("Element '" + elementBy + "' je odškrtnutý, a je to v pořádku!");
            }
        } else {
            System.err.println("Ulozena hodnota u elementu '" + elementBy + "' je špatná!");
            System.err.println(hodnota);
            throw new Error ("Ulozena hodnota u elementu '" + elementBy + "' je špatná!");
        }
    }

    /**
     * Dropdown - vrátí vybranou hodnotu
     * @param elementBy
     * @return
     */
    public String selectGetSelectedValue(By elementBy) {
        waitDropdownLoadValue(elementBy); // čekání na element, aby nebyl prázdný než se načtou data z API
        Select dropdown = new Select(driver.findElement(elementBy));
        String vyber = dropdown.getFirstSelectedOption().getAttribute("value");
        System.out.println("Select '" + elementBy + "' má hodnotu: '" + dropdown.getFirstSelectedOption().getAttribute("value") + "'");
        return vyber;
    }

    /**
     *
     * @param elementBy
     * @return
     */
    public String selectGetSelectedText(By elementBy) {
        waitDropdownLoadValue(elementBy); // čekání na element, aby nebyl prázdný než se načtou data z API
        Select dropdown = new Select(driver.findElement(elementBy));
        System.out.println("Selected text: " + dropdown.getFirstSelectedOption().getText());
        timeDelay(100);
        return dropdown.getFirstSelectedOption().getText();
    }

    /**
     * Select - nastaví se na vybranou hodnotu value
     * @param elementBy
     * @param selected_value
     */
    public void selectSetByValue(By elementBy, String selected_value) {
        waitDropdownLoadValue(elementBy); // čekání na element, aby nebyl prázdný než se načtou data z API
        Select dropdown = new Select(driver.findElement(elementBy));
        try {
            dropdown.selectByValue(selected_value);
        } catch (NoSuchElementException exception) {
            System.err.println("Select '" + elementBy + "' nemá hodnotu '" + selected_value + "'\n\n" + exception);
            driver.quit();
            throw new Error (exception);
        }
        System.out.println("Select '" + elementBy + "' nastaven na hodnotu '" + selected_value + "'");
        timeDelay(100);
    }

    /**
     * Select - nastaví se na hodnotu podle zobrazeného text
     * @param elementBy
     * @param text
     */
    public void selectSetByText(By elementBy, String text){
        waitDropdownLoadValue(elementBy); // čekání na element, aby nebyl prázdný než se načtou data z API
        Select dropdown = new Select(driver.findElement(elementBy));
        try {
            dropdown.selectByVisibleText(text);
        } catch (NoSuchElementException exception) {
            System.err.println("Select '" + elementBy + "' nemá text '" + text + "'\n\n" + exception);
            driver.quit();
            throw new Error (exception);
        }
        System.out.println("Select '" + elementBy + "' nastaven na hodnotu '" + text + "'");
        timeDelay(100);
    }

    /**
     *
     * @param elementBy
     * @param selected_index
     */
    public static void selectSetByIndex (By elementBy, int selected_index) {
        Select dropdown = new Select(driver.findElement(elementBy));
        List<WebElement> option = dropdown.getOptions();
        if (selected_index < option.size()) {
            try {
                dropdown.selectByIndex(selected_index);
            } catch (java.util.NoSuchElementException exception) {
                System.err.println("Select '" + elementBy + "' nejde nastavit na index '" + selected_index + "' \n" + exception);
                driver.quit();
                throw new Error (exception);
            }
            System.out.println("Select " + elementBy + " nastaven na index: '" + selected_index + "'");
        } else {
            System.err.println("Vybraný index je větší než velikost nabídky dropdown");
            driver.quit();
            throw new Error ("Vybraný index je větší než velikost nabídky dropdown");
        }
        timeDelay(500);
    }

    /**
     * Změní vybranou hodnotu v selectu na jinou, pokud je pouze jedna, nechá ji.
     * @param elementBy
     * @param value
     */
    public void selectSetOtherValue (By elementBy, String value) {
        Select dropdown = new Select(driver.findElement(elementBy));
        List<WebElement> options = dropdown.getOptions();
        if (options.size() > 1) {
            for (WebElement option : options) {
                if (value.equals(option.getAttribute("value"))) {
                } else {
                    if ((option.getAttribute("value")).isEmpty()) {
                    } else {
                        selectSetByValue(elementBy, option.getAttribute("value"));
                        timeDelay(100);
                        return;
                    }
                }
            }
        } else {
            System.out.println("Dropdown má pouze jednu hodnotu, proto nelze změnit a zůstane stejná.");
        }
        timeDelay(100);
    }

    /**
     * Nastaví dropdown na první dostupnou (enabled) hodnotu
     * @param elementBy
     */
    public void selectFirst (By elementBy) {
        waitVisibility(elementBy);
        Select dropdown = new Select(driver.findElement(elementBy));
        dropdown.selectByIndex(1);
        timeDelay(100);
    }

    /**
     * Vrací aktuálně vybranou hodnotu z radioButton
     * @param elementBy
     */
    public String radioGet (By elementBy) {
        waitVisibility(elementBy);
        String value = "";
        List<WebElement> radiobuttons = driver.findElements(elementBy);
        for(WebElement radio : radiobuttons) {
            if (radio.isSelected()) {
                value = radio.getAttribute("value");
                break;
            }
        }
        timeDelay(100);
        return value;
    }

    // RadioButton - set selected value

    /**
     * Nastaví radioButton na hodnotu value
     * @param elementBy
     * @param value
     */
    public void radioSet (By elementBy, String value) {
        List<WebElement> radiobuttons = driver.findElements(elementBy);
        Boolean selected = false;
        for(WebElement radio : radiobuttons) {
            if (radio.getAttribute("value").equals(value)) {
                radio.click();
                System.out.println("U radiobuttonu '" + radio.getAttribute("name") + "' vybrána hodnota '" + value + "'.");
                selected = true;
            } else {
            }
        }
        if (!selected) {
            System.err.println("Hodnota value '" + value + "' neexistuje u daného radiobuttonu!");
        }
        timeDelay(100);
    }

    /**
     * Nastaví radioButton na hodnotu id
     * @param elementBy
     * @param value
     */
    public void radioSetId (By elementBy, String value) {
        List<WebElement> radiobuttons = driver.findElements(elementBy);
        Boolean selected = false;
        for(WebElement radio : radiobuttons) {
            if (radio.getAttribute("id").equals(value)) {
                radio.click();
                System.out.println("U radiobuttonu '" + radio.getAttribute("id") + "' vybrána hodnota ");
                selected = true;
            } else {
            }
        }
        if (!selected) {
            System.err.println("Hodnota id '" + value + "' neexistuje u daného radiobuttonu!");
        }
        timeDelay(100);
    }

    /**
     * Vrací unikátní číslo vytvořené pomocí data, lze nastavit počet číslic
     * @param pocet
     * @return
     */
    public String uniqueX(Integer pocet) {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String retezec = dateFormat.format(date);
        return retezec.substring(14-pocet);
    }

    /**
     * Vrací datum přesně 20 let staré
     * @return
     */
    public String dateOld() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -20);
        Date result = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        return dateFormat.format(result);
    }

    // vrací datum v závislosti na zadaném počtu dní

    /**
     * Vrací datum, které lze upravit podle
     * @param day
     * @return
     */
    public String dateUser(int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date result = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        return dateFormat.format(result);
    }

    /**
     * vrátí data ve správném formátu, tak aby se dalo porovnávat s formulářem
     * @param day
     * @return
     */
    public String dateUserRightFormat(int day) {
        String date = dateUser(day);
        String result = date.substring(4,8) + "-" + date.substring(2,4) + "-" + date.substring(0,2);
        return result;
    }

    /**
     *
     * @param table
     * @param nazev_sloupce
     * @param type
     * @return
     * @throws ParseException
     */
    public String getIdRowLatestDate(WebElement table, String nazev_sloupce, String type) throws Exception {
        LinkedList<String> listIdsRow_old = tableGetAllIdRow(table, type);
        Calendar novy = Calendar.getInstance();
        Calendar date_row = Calendar.getInstance();
        String id_row = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for(String str: listIdsRow_old)
        {
            //System.err.println(str);
            date_row.setTime(sdf.parse(table.findElement(By.name(nazev_sloupce+str)).getAttribute("value")));
            if (date_row.after(novy)) {
                //System.err.println(sdf.format(date_row.getTime()) + " je novější " + sdf.format(novy.getTime()));
                novy.setTime(date_row.getTime());
                id_row = str;
            } else {
                //System.err.println(sdf.format(date_row.getTime()) + " je starší " + sdf.format(novy.getTime()));
            }
            //System.err.println("Nejnovější " + sdf.format(novy.getTime()));
        }
        return id_row;
    }

    public String getBirthdayRandom() {
        Calendar cal = Calendar.getInstance();
        int days = 7300 + (int)(Math.random()*365);
        cal.add(Calendar.DATE, -days);
        //cal.add(Calendar.YEAR, -20);
        Date result = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        return dateFormat.format(result);
    }

    public String getNameRandom () {
        String[] name = {"Lubor", "Jan", "Petr", "Pavel", "Jaroslav", "Martin", "Tom", "Miroslav", "Franta", "Josef", "Filip"};
        return name[(int)(Math.random()*10)];
    }

    public String getSurnameRandom () {
        String[] surname = {"Kost" ,"Boss" ,"Klaun" ,"Koala" ,"Hora" ,"Svoboda" ,"Datel" ,"Kalba" ,"Hosa" ,"Plha", "Korda"};
        return surname[(int)(Math.random()*10)];
    }

    public String getPositionRandom () {
        String[] surname = {"Manager" ,"Architekt" ,"Fakturant" ,"Copywriter" ,"Chemik" ,"Designer" ,"Ekonom" ,"Personalista" ,"Projektant" ,"Referent", "Technik"};
        return surname[(int)(Math.random()*10)];
    }

    /**
     *
     * @param miliseconds
     */
    public static void timeDelay(long miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            driver.quit();
        }
    }

    /**
     * Funkce pro výpis ruzných oddělovačů do logu
     * @param text
     * @param type
     */
    public static void actual(String text, String type) {
        int max_length = 0;
        char znak = '.';
        StringBuilder text_out = new StringBuilder();
        switch (type) {
            case "page":
                    max_length=60;
                    znak='+';
                    text_out.append("\n");
                break;
            case "step":
                max_length=50;
                znak='+';
                text_out.append("\n");
                break;
            case "section":
                max_length=35;
                znak='.';
                text_out.append("\n");
                break;
            case "section-close":
                max_length=35;
                znak='.';
                //text_out.append("\n");
                break;
            case "modal_open":
                max_length=30;
                znak='+';
                text_out.append("\n");
                break;
            case "modal_close":
                max_length=30;
                znak='-';
                //text_out.append("\n");
                break;
            case "control":
                max_length=30;
                znak='*';
                text_out.append("\n");
                break;
            case "getICO":
                max_length=30;
                znak='-';
                text_out.append("\n");
                break;
        }

        for (int i = 0; i < (max_length - text.length() / 2); i++) {
            text_out.append(znak);
        }
        text_out.append(" " + text + " ");
        for (int i = 0; i < (max_length - text.length() / 2); i++) {
            text_out.append(znak);
        }
        if (text.length() % 2 != 1) {
            text_out.append(znak);
        }
        System.out.println(text_out);
    }

    /**
     * Funkce vrací EAN získány pomocí EAN
     * @param apiURL
     * @return
     * @throws IOException
     */
    public static String getEAN(String apiURL) {
        try {
            if (apiURL == null) {
                //apiURL="https://nanos-api-tst.nanox.cz/tools/generated-ean/?grid_code=0365";
            }
            //apiURL="https://nanos-api-tst.nanox.cz/tools/generated-ean/?grid_code=0365";
            apiURL="https://nanos-api-tst.nanox.cz/tools/generated-ean/";
            URL url = new URL(apiURL);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            int position;
            String inputLine;
            String ean = "";
            while ((inputLine = in.readLine()) != null) {
                position = inputLine.lastIndexOf("ean");
                if (position > 0) {
                    ean = inputLine.substring(position + 7);
                    ean = ean.replaceAll("\"", "");
                }
            }
            in.close();
            System.out.println("EAN: " + ean);
            return ean;
        } catch (Exception e) {
            System.err.println("API nevrátilo EAN!");
            driver.quit();
            throw new Error ("API nevrátilo EAN!");
        }
    }


    public static void selectCheckValue(By elementBy) {
        //String value_search[] = {"4","14","16"};
        Select dropdown = new Select(driver.findElement(elementBy));
        List<WebElement> options = dropdown.getOptions();
        for (WebElement option : options) {
            System.err.println(option.getText());
            /*if (contains(option.getAttribute("value"))) {
                return option.getAttribute("value");
            }*/
        }
    }

    /**
     * Zda select obsahuje alespoň jednu z hodnot
     * @param elementBy
     * @return
     */
    public static String selectCheckValueNewContact(By elementBy){
        String value_search[] = {"4","14","16"};
        Select dropdown = new Select(driver.findElement(elementBy));
        List<WebElement> options = dropdown.getOptions();
        for (WebElement option : options) {
            if (Arrays.asList(value_search).contains(option.getAttribute("value"))) {
                return option.getAttribute("value");
            }
        }
        System.err.println("Chyba - nenalezena ani jeden z požadovaných.");
        throw new Error ("Chyba - nenalezena ani jeden z požadovaných.");
    }

    /**
     * Ze stránky generátoru IČO (http://www.snarper.cz/RC_generator.aspx), vrací validní náhodné IČO
     * @return ico
     * @throws IOException
     */
    public String getICO() throws IOException {
        try {
            actual("Otevreni prohlizece - getICO", "getICO");
            WebDriver driverIC = new ChromeDriver();
            driverIC.get("http://www.snarper.cz/RC_generator.aspx");
            driverIC.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driverIC.findElement(By.id("ctl00_MainContent_btnGenerateICO_CD")).click();
            timeDelay(1000);
            String ico = driverIC.findElement(By.name("ctl00$MainContent$ASPxTextBox2")).getAttribute("value");
            driverIC.close();
            actual("Zavreni prohlizece - getICO", "getICO");
            return ico;
        } catch (Exception e) {
            System.err.println("Problém se získání validního IČO!");
        }
        return null;
    }

    /**
     * Ze stránky generátoru IČO (http://www.snarper.cz/RC_generator.aspx), vrací validní náhodné IČO
     * @param url
     * @return ico
     * @throws IOException
     */
    public String getICO(String url) throws IOException {
        try {
            actual("Otevreni prohlizece - getICO", "getICO");
            WebDriver driverIC = new ChromeDriver();
            driverIC.get(url);
            driverIC.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driverIC.findElement(By.id("ctl00_MainContent_btnGenerateICO_CD")).click();
            timeDelay(1000);
            String ico = driverIC.findElement(By.name("ctl00$MainContent$ASPxTextBox2")).getAttribute("value");
            driverIC.close();
            actual("Zavreni prohlizece - getICO", "getICO");
            return ico;
        } catch (Exception e) {
            System.err.println("Problém se získání validního IČO!");
        }
        return null;
    }

    /**
     * NewContract PO - krok 1
     * Ověří zda dané IČO není použito v NANO
     * náhodné validní IČO získává z funkce getIČO
     * @return
     * @throws IOException
     */
    public void getUnusedICO()throws IOException {
        System.out.println("Hledaní nepoužítého IČO:");
        String hledane_ico = "";
        do {
            hledane_ico = getICO("http://www.snarper.cz/RC_generator.aspx");
            writeText(NewContractStepOne.personRegCode, hledane_ico);
            //writeText(NewContractStepOne.personRegCode, "28618611");
            driver.findElement(NewContractStepOne.buttonSearchCustomerFOPPO).click();
            System.out.println("Čekání na výsledek vyhledávání - 6s");
            // čekání kvůli odpovědi ze serveru, zobrazený modal nalezeného zákazníka
            timeDelay(6000);
            System.out.println("Konec čekání - 6s");
            if (driver.findElements(By.xpath("/html[1]/body[1]/div[1]/div[4]/div[1]/button[1]")).isEmpty()) {
                System.out.println("Nalezené IČO: " + hledane_ico+ ".");
            } else {
                timeDelay(1000);
                System.out.println("Zabrazeno modální okno s nalezeným IČO " + hledane_ico + " (NANOS/ARES)");
                System.out.println("Modal bude zavřen a spuštěno nové hledání ICO");
                driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[4]/div[1]/button[1]")).click();
                hledane_ico = "";
                clearText(NewContractStepOne.personRegCode);
            }
        } while ((hledane_ico == ""));
        //return hledane_ico;
        timeDelay(500);
    }

    /**
     * Porovnává dva LinkedListy, a vrací rozdílný prvek (jednoznačný id řádku)
     * @param new_ids
     * @param old_ids
     * @return
     */
    public static Integer getIdAddedRow (LinkedList<String> new_ids, LinkedList<String> old_ids){
        for (String row_id: new_ids) {
            if (old_ids.lastIndexOf(row_id) == -1) {
                System.out.println("Nově přidaný řádek do tabulky má identifikátor: "+ row_id + ".");
                return Integer.parseInt(row_id);
            }
        }
        System.out.println("Do tabulky nebyl přidán žádný řádek.");
        throw new Error ("Do tabulky nebyl přidán žádný řádek.");
    }

    /**
     * Porovnává dva LinkedListy, a vrací pořadí rozdílného prvku
     * @param new_ids
     * @param old_ids
     * @return
     */
    public static Integer getNumberAddedRow(LinkedList<String> new_ids, LinkedList<String> old_ids) {
        Integer i = 0;
        for (String row_id: new_ids) {
            i++;
            if (old_ids.lastIndexOf(row_id) != -1) {
            } else {
                //System.out.println("Nově přidaný řádek je v tabulce na pozici: " + i + ".");
                return i;
            }
        }
        return -1;
    }

    /**
     * získá id daného řádku, podle hledané hodnoty ve vybrané tabulce
     * @param xpath
     * @param search_text
     * @return
     */
    public static String tableGetIdRow(String search_text, String xpath) {
        try {
            WebElement table = driver.findElement(By.xpath(xpath));
            String aaa = table.findElement(By.xpath("//*[text() = '" + search_text + "']")).getAttribute("name");
            System.out.println(aaa.substring(aaa.lastIndexOf('_') + 1));
            return aaa.substring(aaa.lastIndexOf('_') + 1);
        } catch (Exception E) {
            System.err.println("Zadaný text v tabulce nenalezen.");
            return "";
        }
    }

    /**
     * Vrací seznam identifikátoru pro jednotlivé řádky tabulky
     * @param table
     * @return
     */
    public static LinkedList<String> tableGetAllIdRow (WebElement table, String type) {
        //System.err.println("TableGetAllIdrow start: " + java.time.LocalDateTime.now());
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        if (table.findElements(By.tagName("tr")).size()==0) {
            System.err.println("Nastaven na 10s.");
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        LinkedList<String> linkedlist = new LinkedList<String>();
        // prochází po řádcích a bere identifikátor z prvního sloupce
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            String retezec = cells.get(0).findElement(By.xpath(type + "[1]")).getAttribute("name");
            String id_row = retezec.substring(retezec.lastIndexOf('_') + 1);
            System.err.println("id_row " + id_row);
            linkedlist.add(id_row);
        }
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //System.err.println("TableGetAllIdrow konec: " + java.time.LocalDateTime.now());
        return linkedlist;
    }

    /**
     * Vrací seznam identifikátoru pro jednotlivé řádky tabulky
     * @param table
     * @return
     */
    public static LinkedList<String> tableGetAllIdRow (WebElement table, Integer column, String type) {
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        LinkedList<String> linkedlist = new LinkedList<String>();
        // prochází po řádcích a bere identifikátor z prvního sloupce
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            String retezec = cells.get(column).findElement(By.xpath(type+"[1]")).getAttribute("name");
            String id_row = retezec.substring(retezec.lastIndexOf('_') + 1);
            linkedlist.add(id_row);
        }
        return linkedlist;
    }

    /**
     * kontrola obsahu buňky v tabulce
     * @param obsah_bunky
     * @param pattern
     * @param nazev_sloupce
     */
    public static void tableCellControlValue(String obsah_bunky, String pattern, String nazev_sloupce){
        //if (pattern.contains(obsah_bunky)) {
        if (obsah_bunky.contains(pattern)) {
            System.out.println("Hodnota ve sloupci '" + nazev_sloupce + "' je doplněna správně. '" + pattern + "'");
        } else {
            System.err.println("Hodnota ve sloupci '" + nazev_sloupce + "' není správná.");
            System.err.println("Obsah buňky tabulky: '" + obsah_bunky + "' vs Očekávaná hodnota: '" + pattern + "'.");
            throw new IllegalStateException("Hodnota ve sloupci '" + nazev_sloupce + "' není ve správném formátu. '" + pattern + "'");
        }
    }

    /**
     * Kontroluje zda v tabulce je neprázdná hodnota
     * @param name
     */
    public static void tableControllCell(String name) {
        String text = driver.findElement(By.name(name)).getText();
        if (text != null) {
            System.out.println("Buňka tabulky '" + name + "' má hodnotu '"+ text +"'");
        } else {
            System.err.println("Buňka tabulky '" + name + "' je prázdná");
        }
    }

    /**
     * vrátí počet řádku v tabulce
     * @param tbody
     * @return
     */
    public static Integer tableGetRowNumber (WebElement tbody) {
        List<WebElement> rows = tbody.findElements(By.tagName("tr"));
        return rows.size();
    }

    /**
     * Výpis obsahu jednoho řádku tabulky
     * @param tbody
     * @param search_text
     */
    public static void tableGetRowValuesGeneral(WebElement tbody, String search_text) {
        List<WebElement> rows = tbody.findElements(By.tagName("tr"));
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            for (WebElement cell : cells) {
                if (cell.getText() != null) {
                    System.out.println("Obsah bunky: " + cell.getText());
                } else {
                    System.err.println("Prázdna bunka, problem");
                }
            }
        }
    }

    /**
     *
     * Použití při zakládání nové smlouvy
     * @param search_text
     */
    public static void tableGetRowValuesByName(WebElement tbody, String search_text) {
        List<WebElement> rows = tbody.findElements(By.tagName("tr"));
        for (WebElement row : rows) {
            String id_row = row.findElement(By.xpath("//*[text() = '" + search_text + "']")).getAttribute("name");
            id_row = id_row.substring(id_row.lastIndexOf('_') + 1);
            WebActions.tableControllCell("eans_"+id_row);
            WebActions.tableControllCell("dist_network_"+id_row);
            WebActions.tableControllCell("address_string_"+id_row);
            WebActions.tableControllCell("measurement_type_"+id_row);
            WebActions.tableControllCell("distribution_tariff_code_"+id_row);
            WebActions.tableControllCell("phases_string_"+id_row);
            WebActions.tableControllCell("custom_name_"+id_row);
            break;
        }
    }

    /**
     * porovná textovou hodnotu WebElementu se zadanou hodnotou
     * @param elementBy
     * @param ocekavana_hodnota
     */
    public void compareWebElementValue (By elementBy, String ocekavana_hodnota) {
        String value = driver.findElement(elementBy).getAttribute("value");
        if (value.equals(ocekavana_hodnota)) {
            System.out.println("Uložená hodnota '" + elementBy + "' je správná.  Uložená: '" + value + "' Ocekavana: '" + ocekavana_hodnota + "'");
        } else {
            System.err.println("Uložená hodnota '" + driver.findElement(elementBy).getAttribute("value") + "' je rozdílná od očekávané hodnoty '" + ocekavana_hodnota + "'.");
            throw new Error ("Uložená hodnota '" + driver.findElement(elementBy).getAttribute("value") + "' je rozdílná od očekávané hodnoty '" + ocekavana_hodnota + "'.");
        }
    }

    /**
     * porovná zaškrtnutí checkboxu s očekávanou hodnotou
     * @param elementBy
     * @param ocekavana_hodnota
     */
    public void compareWebElementCheckbox (By elementBy, Boolean ocekavana_hodnota){
        Boolean aktualni = driver.findElement(elementBy).isSelected();
        if (aktualni == ocekavana_hodnota) {
            System.out.println("Checkbox '" + elementBy + "' má správnou hodnotu.");
        } else {
            System.err.println("Checkbox '" + elementBy + "' má hodnotu '"+ driver.findElement(elementBy).getAttribute("value") + "' má rozdílnou hodnotu od očekávané'" + ocekavana_hodnota + "'.");
            throw new Error ("Checkbox '" + elementBy + "' má hodnotu '"+ driver.findElement(elementBy).getAttribute("value") + "' má rozdílnou hodnotu od očekávané'" + ocekavana_hodnota + "'.");
        }
    }

    /**
     * porovná zaškrtnutou hodnotu radiobuttonu s očekávanou hodnotou
     * @param elementBy
     * @param ocekavana_hodnota
     */
    public void compareWebElementRadiobutton (By elementBy, String ocekavana_hodnota){
        List<WebElement> radiobuttons = driver.findElements(elementBy);
        String value = "";
        for(WebElement radio : radiobuttons) {
            if (radio.isSelected()) {
                //System.out.println("Selected value '" + radio.getAttribute("value")+"'");
                value = radio.getAttribute("value");
                break;
            }
        }
        if (value.equals(ocekavana_hodnota)) {
            System.out.println("Uložená hodnota radiobuttonu '" + elementBy + "' je správná.  Uložená: '" + value + "' Ocekavana: '" + ocekavana_hodnota + "'");
        } else {
            System.err.println("Uložená hodnota radiobuttonu '" + elementBy + "' je špatně.  Uložená: '" + value + "' Ocekavana: '" + ocekavana_hodnota + "'");
            throw new Error ("Uložená hodnota radiobuttonu '" + elementBy + "' je špatně.  Uložená: '" + value + "' Ocekavana: '" + ocekavana_hodnota + "'");
        }
    }

    /**
     *
     * @param titul
     * @param titul_pred
     */
    public void addTitle(String titul, Boolean titul_pred) {
        WebElement titles_act;
        if (titul_pred) {
            driver.findElement(Nastaveni.personTitles).click();
            timeDelay(500);
            titles_act = driver.findElement(By.xpath("//ul[@id='rw_1_listbox'][@class='rw-list']"));
        } else {
            driver.findElement(Nastaveni.personDegrees).click();
            timeDelay(500);
            titles_act = driver.findElement(By.xpath("//ul[@id='rw_2_listbox'][@class='rw-list']"));
        }

        List<WebElement> titles = titles_act.findElements(By.tagName("li"));
        // Čekání až se načtou všechny tituly
        while (titles.size() == 1) {
            timeDelay(50);
            titles = titles_act.findElements(By.tagName("li"));
        }

        for (WebElement title: titles) {
            //System.out.println(title.getText() + " vs "+ titul);
            if (titul.equals(title.getText())) {
                System.out.println("Titul '" + title.getText() + "' doplněn.");
                title.click();
                timeDelay(500);
                return;
            }
        }
        System.out.println("Požadovaný titul '" + titul + "' je u daného zákazníka aktivní.");
    }


    /**
     * Kontrola přidání titulu
     * @param titul
     */
    public void controlAddedTitle(String titul, boolean titul_pred){
        WebElement titles_act;
        if (titul_pred) {
            titles_act = driver.findElement(By.xpath("//ul[@id='rw_1_taglist'][@class='rw-multiselect-taglist']"));
        } else {
            titles_act = driver.findElement(By.xpath("//ul[@id='rw_2_taglist'][@class='rw-multiselect-taglist']"));
        }
        List<WebElement> titles = titles_act.findElements(By.tagName("li"));
        for (WebElement title: titles) {
            if (titul.equals(title.findElement(By.xpath("span")).getText())) {
                System.out.println("Titul '" + title.findElement(By.xpath("span")).getText() + "' je aktivován");
                return;
            }
        }
        System.err.println("Titul '" + titul + "' nenalezen, nebyl mezi aktivními u daného zákazníka.");
        throw new Error ("Titul '" + titul + "' nenalezen, nebyl mezi aktivními u daného zákazníka.");
    }

    /**
     * Odstraní titul
     * @param titul
     */
    public void delTitle(String titul, Boolean titul_pred){
        timeDelay(500);
        WebElement titles_act;
        if (titul_pred) {
            titles_act = driver.findElement(By.xpath("//ul[@id='rw_1_taglist'][@class='rw-multiselect-taglist']"));
        } else {
            titles_act = driver.findElement(By.xpath("//ul[@id='rw_2_taglist'][@class='rw-multiselect-taglist']"));
        }
        List<WebElement> titles = titles_act.findElements(By.tagName("li"));
        for (WebElement title: titles) {
            if (titul.equals(title.findElement(By.xpath("span")).getText())) {
                System.out.println("Titul '" + title.findElement(By.xpath("span")).getText() + "' je odstraněn");
                title.findElement(By.xpath("div/button")).click();
                return;
            }
        }
        System.err.println("Titul '" + titul + "' nenalezen, nebyl mezi aktivními u daného zákazníka.");
        throw new Error ("Titul '" + titul + "' nenalezen, nebyl mezi aktivními u daného zákazníka.");
    }

    /**
     *
     * @param titul
     * @param titul_pred
     */
    public void controlDeletedTitle(String titul, Boolean titul_pred) {
        if (isElementHiddenNow(By.xpath("//span[(text() ='" + titul + "')]"))) {
            System.out.println("Titul '" + titul + "'není zobrazen");
        } else {
            System.err.println("Titul '" + titul + "' je stále aktivní, ke smazání nedošlo!");
            throw new Error ("Titul '" + titul + "' je stále aktivní, ke smazání nedošlo!");
        }
    }

    /**
     * získání textové podoby adresy ze stránky nastavení
     * @param type
     * @return
     */
    public String getAddressSetting(String type){
        String xpath;
        switch (type) {
            case "other":
                xpath = "//button[@value='OTHER'][@title='Upravit adresu']";
                break;
            case "mailing":
                xpath = "//button[@value='MAILING'][@title='Upravit adresu']";
                break;
            case "domicile":
                xpath = "//button[@name='btn_add_domicile'][@title='Upravit adresu']";
                break;
            default:
                System.err.println("Neplatná volba pro získání adresy.");
                throw new Error ("Neplatná volba pro získání adresy.");
        }

        String address = "";
        if (driver.findElements(By.xpath(xpath)).size() > 0) {
            WebElement child = driver.findElement(By.xpath(xpath));
            WebElement parent = child.findElement(By.xpath("./.."));
            WebElement address_element = parent.findElement(By.xpath("label[2]"));
            address = address_element.getText();
        } else {
            System.err.println("Neexistuje adresa");
            throw new Error ("Neexistuje adresa");
        }
        return address;
    }

    public void editAddressSetting(String type){
        String xpath;
        switch (type) {
            case "other":
                xpath = "//button[@value='OTHER'][@title='Upravit adresu']";
                break;
            case "mailing":
                xpath = "//button[@value='MAILING'][@title='Upravit adresu']";
                break;
            case "domicile":
                xpath = "//button[@name='btn_add_domicile'][@title='Upravit adresu']";
                break;
            default:
                System.err.println("Neplatná volba pro kliknutí na editaci adresy.");
                throw new Error ("Neplatná volba pro kliknutí na editaci adresy.");
        }
        System.out.println("Kliknuto na editaci..");
        clickOnElement(By.xpath(xpath));
    }

    /**
     * Funkce na zápis distribuční sítě, ověření, zda funguje API
     * @param dist_network
     * @param text
     */
    public void writeDistNetwork (By dist_network, String text){
        waitVisibility(dist_network);
        writeText(dist_network, text);
        timeDelay(500);
        waitVisibility(By.xpath("//li[@value='" + text + "']"));
        String xpath = "//li[@value='" + text + "']";
        if (!driver.findElements(By.xpath(xpath)).isEmpty()) {
            System.out.println("API 'DeliveryPoint - Vyhledání distribuční sítě' v pořádku. Zadaná hodnota '" + text + "' a API vrací: '" + driver.findElement(By.xpath(xpath)).getText() + "'");
        } else {
            System.err.println("API 'DeliveryPoint - Vyhledání distribuční sítě' nic nevrátilo pro zadanou hodnotu '" + text + "'.");
            throw new Error ("API 'DeliveryPoint - Vyhledání distribuční sítě' nic nevrátilo.");
        }
        clickOnElement(By.xpath(xpath));
    }

    /**
     *
     * @param driver
     * @param selected_value
     * @param town
     * @param psc
     * @param mistni_cast
     * @param street
     * @param number_cp
     * @param number_co
     */
    public void ModalAddressFillAddress(WebDriver driver, String selected_value, String town, String psc, String mistni_cast,
                                   String street, String number_cp, String number_co){
        actual("MODAL ADD ADDRESS", "modal_open");
        //controlAllElement();
        waitDropdownLoadValue(ModalAddNewAddress.country_code);
        writeText(ModalAddNewAddress.town_name, town + " ");
        //timeDelay(2000);
        clickOnElement(By.xpath("//li[contains(text(), '" + psc + "')]"));
        if (!mistni_cast.isEmpty()) {
            writeText(ModalAddNewAddress.local_place_name, mistni_cast);
            clickOnElement(By.xpath("//li[contains(text(), '" + mistni_cast + "')]"));
        }
        if (!street.isEmpty()) {
            writeText(ModalAddNewAddress.street_name, street);
            clickOnElement(By.xpath("//li[contains(text(), '" + street + "')]"));
        }
        writeText(ModalAddNewAddress.house_nr, number_cp);
        if (!number_co.isEmpty()) {
            //writeText(ModalAddNewAddress.orientation_nr, number_co);
        }
        timeDelay(100);
        clickOnElement(ModalAddNewAddress.buttonSave);
        waitInvisibility(ModalAddNewAddress.buttonCloseModal);
        waitHideLoader();
        actual("MODAL ADD ADDRESS", "modal_close");
    }

    /**
     * Elementy které nejsou ničím ovlivněny, zobrazeny vždy
     * @return
     */
    public static LinkedList<By> ModalAddressGetStaticElements() {
        LinkedList<By> list = new LinkedList<By>();
        list.addAll(Arrays.asList(ModalAddNewAddress.buttonCloseModal, ModalAddNewAddress.buttonSave, ModalAddNewAddress.buttonDisplayAddress));
        list.addAll(Arrays.asList(ModalAddNewAddress.country_code, ModalAddNewAddress.town_name, ModalAddNewAddress.post_code,
                ModalAddNewAddress.local_place_name, ModalAddNewAddress.street_name, ModalAddNewAddress.number_type));
        return list;
    }

    /**
     * Elementy u kterých záleží na hodnotě dropdown
     */
    public static LinkedList<By> ModalAddressGetVariablesElements(String selected_value) {
        System.out.println("Kontrola proměnlivých elementů");
        LinkedList<By> listVariables = new LinkedList<By>();
        switch (selected_value)
        {
            case "HOUSE":
                listVariables.add(ModalAddNewAddress.house_nr);
                listVariables.add(ModalAddNewAddress.orientation_nr);
                break;
            case "REGISTRATION":
                listVariables.add(ModalAddNewAddress.registration_nr);
                break;
            case "PLOT":
                listVariables.add(ModalAddNewAddress.plot_number);
                break;
            case "POBOX":
                listVariables.add(ModalAddNewAddress.pobox);
                break;
        }
        return listVariables;
    }

    public void ModalAddressCheckAddressDropDown (String selected_value) {
        System.out.println("Kontrola WebElementu pro dropdown s hodnotou: " + selected_value);
        selectSetByValue(ModalAddNewAddress.number_type, selected_value);
        WebActions.elementsExistance(ModalAddressGetVariablesElements(selected_value));
        WebActions.timeDelay(500);
    }

    /**
     * Kontrola všech elementů
     */
    public void ModalAddressControlAllElement() {
        // kontrola statických elementů
        WebActions.elementsExistance(ModalAddressGetStaticElements());
        // number_type má tyto hodnoty: REGISTRATION, PLOT, HOUSE, POBOX
        /*
        ModalAddressCheckAddressDropDown("REGISTRATION");
        ModalAddressCheckAddressDropDown("PLOT");
        ModalAddressCheckAddressDropDown("POBOX");
        ModalAddressCheckAddressDropDown("HOUSE");
        */
    }

    /**
     * Vyplnění modálního okna - stejnými hodnotami
     */
    public void ModalAddressFillForm(WebDriver driver, String selected_value, String number){
        WebActions.actual("MODAL ADD ADDRESS", "modal_open");
        ModalAddressControlAllElement();
        driver.findElement(ModalAddNewAddress.town_name).sendKeys("Znojmo");
        driver.findElement(By.xpath("//li[@value=2320]")).click();
        driver.findElement(ModalAddNewAddress.street_name).sendKeys("Jarošova");
        driver.findElement(By.xpath("//li[@value=6429]")).click();
        ModalAddressCheckAddressDropDown(selected_value);
        switch (selected_value)
        {
            case "HOUSE":
                driver.findElement(ModalAddNewAddress.house_nr).sendKeys("1");
                timeDelay(200);
                driver.findElement(ModalAddNewAddress.orientation_nr).sendKeys("1244");
                break;
            case "REGISTRATION":
                driver.findElement(ModalAddNewAddress.registration_nr).sendKeys(number);
                break;
            case "PLOT":
                driver.findElement(ModalAddNewAddress.plot_number).sendKeys(number);
                break;
            case "POBOX":
                driver.findElement(ModalAddNewAddress.pobox).sendKeys(number);
                break;
        }

        clickOnElement(ModalAddNewAddress.buttonSave);
        waitInvisibility(ModalAddNewAddress.buttonCloseModal);
        waitHideLoader();
        WebActions.actual("MODAL ADD ADDRESS", "modal_close");
    }

    public void ModalPaymentContactAdd(WebDriver driver, String payment_type, String unique, String nazev, String kod_banky){
        WebActions.actual("MODAL ADD PAYMENT CONTACT", "modal_open");
        switch (payment_type) {
            case "bank":
                selectSetByValue(ModalAddNewPaymentContact.type, "A");
                writeText(ModalAddNewPaymentContact.account_prefix, "1" + unique.substring(8));
                writeText(ModalAddNewPaymentContact.account_number, unique);
                clickOnElement(ModalAddNewPaymentContact.bank_code);
                waitDropdownLoadValue(ModalAddNewPaymentContact.bank_code);
                selectSetByValue(ModalAddNewPaymentContact.bank_code, kod_banky);
                break;
            case "sipo":
                selectSetByValue(ModalAddNewPaymentContact.type, "S");
                driver.findElement(ModalAddNewPaymentContact.sipo_code).sendKeys(unique);
                break;
            default:
                System.err.println("Neplatná volba, můžete pouze 'SIPO' nebo 'Bankovní účet'.");
                break;
        }
        writeText(ModalAddNewPaymentContact.name, nazev);
        writeText(ModalAddNewPaymentContact.memo, "memo" + unique);

        clickOnElement(ModalAddNewPaymentContact.buttonSave);
        waitInvisibility(ModalAddNewPaymentContact.buttonCloseModal);
        waitHideLoader();
        WebActions.actual("CLOSE MODAL ADD PAYMENT CONTACT", "modal_close");
    }

    /**
     *
     * @param table
     * @param payment_type
     * @param unique
     * @param nazev
     * @param kod_banky
     */
    public void ModalPaymentContactControl(WebElement table, String payment_type, String unique, String nazev, String kod_banky) {
        actual("Kontrola přidání platebního kontaktu do tabulky", "section");
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            String aaa="memo"+unique;
            if (aaa.equals(cells.get(3).getText())) {
                if (payment_type == "bank") {
                    tableCellControlValue(cells.get(0).getText(), "BÚ", "Typ");
                    tableCellControlValue(cells.get(1).getText(), "1" + unique.substring(8) + "-" + unique + "/" + kod_banky, "Číslo");
                    tableCellControlValue(cells.get(2).getText(), nazev, "Název");
                    tableCellControlValue(cells.get(3).getText(), "memo" + unique, "Poznámka");
                } else {
                    tableCellControlValue(cells.get(0).getText(), "SIPO", "Typ");
                    tableCellControlValue(cells.get(1).getText(), "SIPO " + unique, "Číslo");
                    tableCellControlValue(cells.get(2).getText(), nazev, "Název");
                    tableCellControlValue(cells.get(3).getText(), "memo" + unique, "Poznámka");
                }
                return;
            }
        }
        System.err.println("Platební kontakt nenalezen! Hledáno pomocí poznámky 'memo" + unique + "'.");
        throw new IllegalStateException("Platební kontakt nenalezen! Hledáno pomocí poznámky 'memo" + unique + "'.");
    }

    /**
     *
     * @param table
     * @param unique
     * @throws Exception
     */
    public void ModalPaymentContactEdit(WebElement table, String unique){
        actual("Editace platebního kontaktu", "section");
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            System.out.println("Před editací: '"+row.getText()+"'.");
            Actions action = new Actions(driver);
            action.moveToElement(row).contextClick().build().perform();
            clickOnElement(By.xpath("//a[@class='Editovat']"));
            writeText(By.name("memoEdit"), "Update " + unique);
            clickOnElement(ModalAddNewPaymentContact.buttonSave);
            waitInvisibility(ModalAddNewPaymentContact.buttonCloseModal);
            waitHideLoader();
            return;
        }
        System.err.println("Při editaci platebního kontaktu došlo k chybě.");
        throw new IllegalStateException("Při editaci platebního kontaktu došlo k chybě.");
    }

    /**
     *
     * @param table
     * @param unique
     * @throws Exception
     */
    public void ModalPaymentContactEditControl (WebElement table, String unique){
        actual("Kontrola editace platebního kontaktu:", "section");
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        for (WebElement row: rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            String memo="Update " + unique;
            if (memo.equals(cells.get(3).getText())) {
                System.out.println("Po editaci: '"+row.getText()+"'.");
                System.out.println("Update poznámky u platebního kontaktu proběhlo v pořádku");
                return;
            }
        }
        System.err.println("Editovaný platební kontakt nenalezen! Hledáno pomocí poznámky 'Update memo " + unique + "'.");
        throw new IllegalStateException("Editovaný platební kontakt nenalezen! Hledáno pomocí poznámky 'Update memo " + unique + "'.");
    }

    /**
     *
     * @param table
     * @param unique
     * @throws Exception
     */
    public void ModalPaymentContactDel (WebElement table, String unique){
        actual("Smazání platebního kontaktu", "section");
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        System.out.println("Počet platebních kontaktů před odstranění: " + table.findElements(By.tagName("tr")).size());
        for (WebElement row: rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            if (("memo" + unique).equals(cells.get(3).getText())) {
                System.out.println("Platební kontakt ke smazání: '"+ row.getText() + "'");
                Actions action = new Actions(driver);
                action.moveToElement(row).contextClick().build().perform();
                timeDelay(1000);
                clickAndControlToast(By.xpath("//a[@class='Smazat']"), "Kontakt smazán", "Není zobrazen toaster, nedošlo ke smazání.");
                System.out.println("Počet platebních kontaktů po odstranění: " + table.findElements(By.tagName("tr")).size());
                return;
            }
        }
        System.err.println("Platební kontakt nenalezen! Hledáno pomocí poznámky 'memo" + unique + "'.");
        throw new IllegalStateException("Platební kontakt nenalezen! Hledáno pomocí poznámky 'memo" + unique + "'.");
    }

    /**
     *
     * @param jmeno
     * @param prijmeni
     * @param funkce
     * @param phone
     * @throws Exception
     */
    public void ModalGeneralFillFormFO(String jmeno, String prijmeni, String funkce, String email, String phone){
        WebActions.actual("OPEN MODAL ADD GENERAL CONTACT", "modal_open");
        ModalAddGeneralContact.controlAllElement();
        writeText(ModalAddGeneralContact.firstname, jmeno);
        writeText(ModalAddGeneralContact.lastname, prijmeni);
        writeText(ModalAddGeneralContact.salutation_first_name, jmeno);
        writeText(ModalAddGeneralContact.salutation_last_name, prijmeni);
        selectSetByValue(ModalAddGeneralContact.sex, "M");
        writeText(ModalAddGeneralContact.position, funkce);

        writeText(ModalAddGeneralContact.emails, email);
        clickOnElement(ModalAddGeneralContact.btn_addMail);
        writeText(ModalAddGeneralContact.phone_numbers, phone);
        clickOnElement(ModalAddGeneralContact.btn_addPhone);
        writeText(ModalAddGeneralContact.memo, "Memo " + jmeno + " " + prijmeni);

        clickOnElement(ModalAddGeneralContact.buttonSave);
        waitInvisibility(ModalAddGeneralContact.buttonCloseModal);
        waitHideLoader();
        WebActions.actual("CLOSE MODAL ADD GENERAL CONTACT", "modal_close");
    }

    public void ModalGeneralFillFormPO(String jmeno, String prijmeni, String pozice, String unique_value){
        WebActions.actual("MODAL ADD GENERAL CONTACT", "modal_open");
        ModalAddGeneralContact.controlAllElement();
        writeText(ModalAddGeneralContact.firstname, jmeno);
        writeText(ModalAddGeneralContact.lastname, prijmeni);
        writeText(ModalAddGeneralContact.salutation_first_name, jmeno);
        writeText(ModalAddGeneralContact.salutation_last_name, prijmeni);
        selectSetByValue(ModalAddGeneralContact.sex, "M");
        writeText(ModalAddGeneralContact.position, pozice);

        writeText(ModalAddGeneralContact.emails, jmeno + prijmeni + unique_value + "@test.com");
        clickOnElement(ModalAddGeneralContact.btn_addMail);
        writeText(ModalAddGeneralContact.phone_numbers, "721" + unique_value);
        clickOnElement(ModalAddGeneralContact.btn_addPhone);

        clickOnElement(ModalAddGeneralContact.buttonSave);
        waitInvisibility(ModalAddGeneralContact.buttonCloseModal);
        waitHideLoader();
        WebActions.actual("CLOSE MODAL ADD GENERAL CONTACT", "modal_close");
    }

    /**
     * Vyplnění modálního okna - stejnými hodnotami
     */
    public void ModalGeneralFillForm(WebDriver driver){
        WebActions.actual("OPEN MODAL ADD GENERAL CONTACT", "modal_open");
        ModalAddGeneralContact.controlAllElement();
        //driver.findElement(firstname).sendKeys("Petr");
        //driver.findElement(lastname).sendKeys("Test");
        writeText(ModalAddGeneralContact.salutation_first_name, "Jméno");
        writeText(ModalAddGeneralContact.salutation_last_name, "Příjmení");
        selectSetByValue(ModalAddGeneralContact.sex, "M");
        writeText(ModalAddGeneralContact.position, "Ředitel");

        String unique_value = uniqueX(6);
        writeText(ModalAddGeneralContact.emails, "email" + unique_value + "@gmail.com");
        clickOnElement(ModalAddGeneralContact.btn_addMail);
        writeText(ModalAddGeneralContact.phone_numbers, "+420721" + unique_value);
        clickOnElement(ModalAddGeneralContact.btn_addPhone);
        writeText(ModalAddGeneralContact.memo, "Poznámka general contact.");

        clickOnElement(ModalAddGeneralContact.buttonSave);
        /**
         */
        //waitInvisibility(ModalAddGeneralContact.buttonCloseModal);
        waitHideLoader();
        WebActions.actual("CLOSE MODAL ADD GENERAL CONTACT", "modal_close");
    }
}
