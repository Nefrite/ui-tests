package cz.nanos.uitests.service;

import cz.nanos.uitests.pages.Header;
import cz.nanos.uitests.pages.HomePage;
import cz.nanos.uitests.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

public abstract class AuthorizedScenario {
	protected WebDriver driver;
	protected WebActions actions;

	public String loginUser = "jirina.brezinova";
	public String loginPassword = "Nano123456.";
	public String basicUrl="http://localhost:3000";

	public abstract String getScenarioDescription();

	//protected abstract String getScenarioMsg();

	@PostConstruct
	private void init() {
		driver = initDriver(basicUrl);
		actions = new WebActions(driver);
		printDescription();
	}

	public static WebDriver initDriver(String url) {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.get(url);
		return driver;
	}

	public void printDescription() {
		System.out.println("------------------------------------------------------");
		System.out.println(this.getClass().getSimpleName());
        System.out.println("Scenario: " + getScenarioDescription() );
        System.out.println("Logged user: " + loginUser + "/" + loginPassword);
        System.out.println("------------------------------------------------------");
        System.out.println("Starting url: " + driver.getCurrentUrl());
		System.out.println("Začátek scénáře: " + java.time.LocalDateTime.now());
	}

	public void userLogin(String username, String password){
		WebActions.actual("LOGIN PAGE", "page");
		LoginPage.controlAllElement();
		actions.writeText(LoginPage.username, username);
		actions.writeText(LoginPage.password, password);
        actions.clickAndControlToast(LoginPage.buttonLogin, "Byl jste přihlášen", "Přihlášení se nezdařilo.");
		System.out.println("Přihlášení uživatele '" + username + "' proběhlo v pořádku.");
		WebActions.actual("HOME PAGE", "page");
		actions.checkUrl(basicUrl + HomePage.url);
		//HomePage.controlAllElement();
    }

	public void userLogout(){
		actions.timeDelay(2000);
		actions.clickOnElement(Header.logout);
		actions.checkUrl(WebActions.basicUrl + '/');
		//System.out.println("Odhlášení uživatele proběhlo v pořádku.");
		if (null != driver) {
			driver.close();
			driver.quit();
		}
		System.out.println("Konec scénáře " + java.time.LocalDateTime.now());
	}
}
